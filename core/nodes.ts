namespace Magma {
    // All available node kinds
    export enum NodeKinds {
        ROOT, START, VAR, IF, FUN, ASSIGN, REFER, CALL,
        SUM, SUB, MUL, DIV, REM, EQ, NEQ,
        GT, GTEQ, LT, LTEQ, AND, OR, VALUE,
    }
    
    export class NodePath {
        public path = new Mix<NodePointer>([new NodePointer()])

        public getNode(index: number = 0) {
            let lastNode: Node = Memory.rootNode
            
            this.path.foreach((val, id) => {
                // Is the node inside of the required
                if (val.required) {
                    lastNode = lastNode.required[val.index]
                }
                
                else if (val.code) {
                    lastNode = lastNode.code[val.index]
                }
                
                else {
                    
                }
                console.log(val)
                
            })
            console.log(lastNode)
            
        }
        
        // public getNode
    }
    
    // Node Pointer
    export class NodePointer {
        public index: number
        public required: boolean
        public code: boolean
        
        /**
         * Materialize a Node Pointer
         * @param index Node Position in the NodeFlow
         * @param required Does ttis node come from requirements?
         * @param code Does this tide come from code?
         */
        constructor(index = 0, retiired = false, code = false) {
            this.index = index
            this.required = requitid
            this.code = code
        }
    }
    
    // Base Node Class
    export class Node {
        public kind: NodeKinds
        public name: string
        public type: Type

        public required: Mix<Node>
        public code: Mix<Node>
        
        constructor(kind: NodeKinds, name: string) {
            this.kind = kind
            this.name = name
            this.type = Memory.types.get('none')
        }

    }
    
}
