namespace Magma {
    
    // Type class
    export class Type {
        private structure: Object
        private name: string
        
        
        constructor(name: string, structure: Object = {}) {
            this.name = name
            this.structure = structure
        }
    }
    
}
