namespace Magma {
    
    export class Memory {
        public static rootNode: Node
        public static types = new Arr<string, Type>()
        public static ruleClasses = new Arr<string, RuleClass>()
        
        constructor() {
            Memory.rootNode = new Node(NodeKinds.ROOT, '')
            
            // Math Class
            Memory.ruleClasses.set('math', new RuleClass(
                new Mix([
                    NodeKinds.SUM,
                    NodeKinds.SUB,
                    NodeKinds.MUL,
                    NodeKinds.DIV,
                    NodeKinds.REM
                ]), new Mix([])
            ))
            
            // Comparison Class
            Memory.ruleClasses.set('comparison', new RuleClass(
                new Mix([
                    NodeKinds.EQ,
                    NodeKinds.NEQ,
                    NodeKinds.GT,
                    NodeKinds.GTEQ,
                    NodeKinds.LT,
                    NodeKinds.LTEQ
                ]), new Mix([])
            ))
            
            // Combo Class
            Memory.ruleClasses.set('combo', new RuleClass(
                new Mix([
                    NodeKinds.AND,
                    NodeKinds.OR,
                ]), new Mix([])
            ))
            
            // Expression Class
            Memory.ruleClasses.set('expression', new RuleClass(
                new Mix([
                    NodeKinds.REFER,
                    NodeKinds.CALL,
                    NodeKinds.VALUE,
                ]), 
                new Mix([
                    Memory.ruleClasses.get('math'),
                    Memory.ruleClasses.get('comparison'),
                ])
            ))
            
            // Declaration Class
            Memory.ruleClasses.set('declaration', new RuleClass(
                new Mix([
                    NodeKinds.VAR,
                    NodeKinds.IF,
                    NodeKinds.FUN,
                    NodeKinds.ASSIGN
                ]), new Mix([])
            ))
            
            Memory.types.set('none', new Type('none', {}))
            Memory.types.set('switch', new Type('switch', {}))
            Memory.types.set('number', new Type('number', {}))
            
        }
        
        
    }
}