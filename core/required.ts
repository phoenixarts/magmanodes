namespace Magma {
    
    export class RuleClass {
        private kinds: Mix<NodeKinds>
        
        /**
         * RuleClass is an abstraction containing multiple
         * node kinds under one name label
         * 
         * @param kindList List of primitive node kinds
         * @param classList List of complex "classes" of node kinds
         */
        constructor(kindList = new Mix<NodeKinds>(), classList = new Mix<RuleClass>()) {
            this.kinds = kindList
            
            // Incorporate class list into kind list
            classList.foreach(val => {
                const kinds = val.get()
                kinds.foreach(val => {
                    this.kinds.add(val)
                })
            })
        }
        
        public get(): Mix<NodeKinds> {
            return this.kinds
        }
        
        
    }
    
    // Object of Required list
    export class RequiredObject {
        public kind = new Mix<NodeKinds>()
        public type = new Mix<Type>()
        
        constructor(kind: RuleClass, type: Mix<Type>) {
            this.kind = kind.get()
            this.type = type
        }
    }
    
    export class Required {
        public list = new Mix<RequiredObject>()
        public infinite = false
        
        // Required ruling happens here
        constructor(node: Node) {
            // Variable
            if ([NodeKinds.VAR].includes(node.kind)) {
                this.list.add(new RequiredObject(
                    Memory.ruleClasses.get('expression'), 
                    new Mix([node.type])
                ))
                return
            }
            
            // If condition
            if ([NodeKinds.IF].includes(node.kind)) {
                this.list.add(new RequiredObject(
                    Memory.ruleClasses.get('expression'),
                    new Mix([Memory.types.get('switch')])
                ))
                return
            }
            
            // ...
            
        }
    }
}