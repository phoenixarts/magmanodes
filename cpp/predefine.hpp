#pragma once

#include <iostream>

// Vectors
#include <vector>
template <class T>
using vec = std::vector<T>

// Maps
#include <map>
template <class T, class U>
using map = std::map<T,U>

// Log
#define log std::cout