const { app, BrowserWindow, Menu } = require('electron')

app.on('ready', () => {
    process.env['ELECTRON_DISABLE_SECURITY_WARNINGS'] = true

    Menu.setApplicationMenu(null)
    
    let win = new BrowserWindow({
        width: 1280,
        height: 720,
        resizable: false,
        darkTheme: true,
        webPreferences: {
            nodeIntegration: true,
            allowEval: false
        }
    })
    
    win.loadFile('index.html')
})

app.on('window-all-closed', app.quit)