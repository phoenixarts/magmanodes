'use strict';

class Ground<O = any> {
    public properties: O;
    
    random(min: number, max: number) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }
}
