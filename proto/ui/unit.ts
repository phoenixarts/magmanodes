let _alphabetIndex = 0.1;
const AlphabedIndexes = {
    a: _alphabetIndex *= 2,
    b: _alphabetIndex *= 2,
    c: _alphabetIndex *= 2,
    d: _alphabetIndex *= 2,
    e: _alphabetIndex *= 2,
    f: _alphabetIndex *= 2,
    g: _alphabetIndex *= 2,
    h: _alphabetIndex *= 2,
    i: _alphabetIndex *= 2,
    j: _alphabetIndex *= 2,
    k: _alphabetIndex *= 2,
    l: _alphabetIndex *= 2,
    m: _alphabetIndex *= 2,
    n: _alphabetIndex *= 2,
    o: _alphabetIndex *= 2,
    p: _alphabetIndex *= 2,
    q: _alphabetIndex *= 2,
    r: _alphabetIndex *= 2,
    s: _alphabetIndex *= 2,
    t: _alphabetIndex *= 2,
    u: _alphabetIndex *= 2,
    v: _alphabetIndex *= 2,
    w: _alphabetIndex *= 2,
    x: _alphabetIndex *= 2,
    y: _alphabetIndex *= 2,
    z: _alphabetIndex *= 2
};

class Unit extends Ground {
    public timeouts = new Map<string, NodeJS.Timeout>();
    public intervals = new Map<string, NodeJS.Timeout>();
    
    setTimeout(func: VoidFunction, delay: number, name: string = "") {
        if (name.length === 0) {
            name = "timeout_" + performance.now() + "_" + this.random(1, 99999);
        }
        
        if (this.timeouts.has(name)) {
            this.clearTimeout(name);
        }
        
        const id = setTimeout(func, delay);
        
        this.timeouts.set(name, id);
        
        return id;
    }
    
    clearTimeout(name: string = "") {
        if (this.timeouts.has(name) === false) {
            return;
        }
        
        clearTimeout(this.timeouts.get(name));
        
        this.timeouts.delete(name);
    }
    
    setInterval(func: VoidFunction, delay: number, name: string = "") {
        if (name.length === 0) {
            name = "interval_" + performance.now() + "_" + this.random(1, 99999);
        }
        
        if (this.intervals.has(name)) {
            this.clearInterval(name);
        }
        
        const id = setInterval(func, delay);
        
        this.intervals.set(name, id);
        
        return id;
    }
    
    clearInterval(name = "") {
        if (this.intervals.has(name) === false) {
            return;
        }
        
        clearInterval(this.intervals.get(name));
        
        this.intervals.delete(name);
    }
    
    public numberify(name: string): number {
        let result = 1;
        
        for (let i = 0; i < name.length; i++) {
            if (AlphabedIndexes[name[i]]) {
                result /= AlphabedIndexes[name[i]] + (i * i * i * i * i);
            }
        }
        
        return result;
    }
    
    public uniqueNumber(): number {
        return parseInt(Date.now() + "" + this.random(1, 99999) + "" + this.random(1, 99999));
    }
    
    public uniqueID(): string {
        return Date.now() + "_" + this.random(1, 99999) + "_" + this.random(1, 99999);
    }
    
    public construct(key: string, val: any) {
        if (typeof this[key] !== "undefined") {
            this[key] = val;
        }
    }
}
