'use strict';

class Struct {
    
}

const Data = {
    lists: new AssocList<string, List>(),
    create: (id = "") => {
        if (Data.lists.has(id)) {
            // console.warn("Trying to add list with already existing listID.");
            
            return;
        }
        
        const list = new List(id);
        
        Data.lists.set(id, list);
        
        list.mount();
    },
    remove: (id = "") => {
        if (Data.lists.has(id) === false) {
            // console.warn("Trying to remove nonexisting list.");
            
            return;
        }
        
        Data.lists.get(id).unmount();
        
        Data.lists.delete(id);
    },
    data: <T = any>(listID: string, itemID: string): T => Data.lists.get(listID).items.get(itemID).data,
    listPointersWaiting: new Map<string, ListPointer[]>()
};
