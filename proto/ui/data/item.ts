'use strict';

class Item<DataType> extends Ground {
    public id: string;
    public data: DataType;
    public pointers = new AssocArray<string, Pointer<any>[]>();
    
    constructor(id: string = "", data: DataType) {
        super();
        
        this.id = id;
        this.data = data;
    }
    
    update(prop: string, newValue: any) {
        const oldValue = this.data[prop];
        
        this.data[prop] = newValue;
        
        this.broadcast(prop, newValue, oldValue);
    }
    
    broadcast(prop: string, newValue: any, oldValue: any) {
        if (this.pointers.has(prop) === false) {
            return;
        }
        
        const pointers = this.pointers.get(prop);
        
        for (let i = 0; i < pointers.length; i++) {
            pointers[i].onUpdate(newValue, oldValue);
        }
    }
}
