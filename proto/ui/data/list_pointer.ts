'use strict';

class ListPointer<DataType = any> extends Ground {
    public eventID: string;
    public listID: string;
    public onAdd: null | ((id: string) => void);
    public onRemove: null | ((id: string) => void);
    public onSort: null | ((elementID1: string, elementID2: string, type: number) => void);
    public onUnmount: null | VoidFunction;
    public mounted: boolean;
    public list: List<DataType> | null;
    public onWaitedMount: () => {};
    
    constructor(
        eventID: string, 
        listID: string, 
        onAdd: null | ((id: string) => void) = null, 
        onRemove: null | ((id: string) => void) = null, 
        onSort: null | VoidFunction = null,
        onUnmount: null | VoidFunction = null, 
        autoMount: boolean = true
    ) {
        super();
        
        this.eventID = eventID.length !== 0 ? eventID : "listEvent_" + performance.now() + "_" + this.random(1, 99999);
        this.listID = listID;
        this.onAdd = onAdd;
        this.onRemove = onRemove;
        this.onSort = onSort;
        this.onUnmount = onUnmount;
        this.mounted = false;
        this.list = null;
        
        if (autoMount) {
            this.mount();
        }
    }
    
    size() {
        return this.list.items.size;
    }
    
    clean() {
        this.list.clean();
    }
    
    item(id = "") {
        if (this.list.items.has(id) === false) {
            return null;
        }
        
        return this.list.items.get(id);
    }
    
    add(structure: DataType, id = "") {
        this.list.add(structure, id);
    }
    
    remove(id = "") {
        if (this.list) {
            this.list.remove(id);
        }
    }
    
    sort(elementID1 = "", elementID2 = "", type = 0) {
        this.list.sort(elementID1, elementID2, type);
    }
    
    protected registerWaitingList(): void {
        if (Data.listPointersWaiting.has(this.listID) === false) {
            Data.listPointersWaiting.set(this.listID, []);
        }
        
        Data.listPointersWaiting.get(this.listID).push(this);
    }
    
    public notifyMountUI(): void {
        if (this.onWaitedMount) {
            this.onWaitedMount();
        }
    }
    
    mount() {
        if (Data.lists.has(this.listID) === false) {
            this.mounted = false;
            
            this.registerWaitingList();
            
            return;
        }
        
        const list = Data.lists.get(this.listID);
        
        if (list.pointers.has(this.eventID)) {
            this.mounted = false;
            
            return;
        }
        
        list.pointers.set(this.eventID, this);
        
        this.list = list;
        
        this.mounted = true;
    }
    
    unmount() {
        if (Data.lists.has(this.listID) === false) {
            this.mounted = false;
            
            return;
        }
        
        const list = Data.lists.get(this.listID);
        
        if (list.pointers.has(this.eventID) === false) {
            this.mounted = false;
            
            return;
        }
        
        list.pointers.delete(this.eventID);
        
        this.list = null;
        
        this.mounted = false;
    }
}
