'use strict';

declare type PointerUpdateFunction = (newValue: any, oldValue: any) => void;

// Add generic type for prop type
class Pointer<DataType = any> extends Ground {
    public listID: string;
    public itemID: string;
    public prop: string;
    public onUnmount: null | VoidFunction;
    public onUpdate: null | PointerUpdateFunction;
    public mounted: boolean;
    public uniqueID: string;
    
    constructor(
        listID: string = "", 
        itemID: string = "", 
        prop: string = "", 
        onUpdate: null | PointerUpdateFunction = null, 
        onUnmount: null | VoidFunction = null, 
        autoMount: boolean = true
    ) {
        super();
        
        this.listID = listID;
        this.itemID = itemID;
        this.prop = prop;
        this.onUnmount = onUnmount;
        this.onUpdate = onUpdate;
        this.mounted = false;
        this.uniqueID = performance.now() + "_" + this.random(1, 99999);
        
        if (autoMount) {
            this.mount();
        }
    }
    
    get item(): Item<any> {
        if (Data.lists.has(this.listID) === false) {
            return null;
        }
        
        return Data.lists.get(this.listID).items.get(this.itemID);
    }
    
    get value(): DataType {
        const item = this.item;
        
        if (item) {
            return this.item.data[this.prop];
        }
        
        return null;
    }
    
    set value(newValue: DataType) {
        if (this.mounted === false) {
            return;
        }
        
        const item = this.item;
        
        if (item) {
            this.item.update(this.prop, newValue);
        }
    }
    
    update() {
        this.value = this.value;
    }
    
    mount() {
        if (Data.lists.has(this.listID) === false) {
            this.mounted = false;
            
            // if (this.listID.length !== 0 && this.itemID.length !== 0) {
            //     setTimeout(() => {
            //         console.log("remounting pointer on list not found", this.listID, this.itemID, this.prop);
                    
            //         // this.mount();
            //     }, 0);
            // }
            
            return;
        }
        
        const list = Data.lists.get(this.listID);
        
        if (list.items.has(this.itemID) === false) {
            this.mounted = false;
            
            return;
        }
        
        if (this.onUpdate !== null) {
            if (this.item.pointers.has(this.prop) === false) {
                this.item.pointers.set(this.prop, []);
            }
            
            this.item.pointers.get(this.prop).push(this);
        }
        
        this.mounted = true;
    }
    
    unmount() {
        if (Data.lists.has(this.listID) === false) {
            this.mounted = false;
            
            return;
        }
        
        const list = Data.lists.get(this.listID);
        
        if (list.items.has(this.itemID) === false) {
            this.mounted = false;
            
            return;
        }
        
        if (this.onUpdate !== null) {
            if (this.item.pointers.has(this.prop) === false) {
                this.mounted = false;
                
                return;
            }
            
            const siblings = this.item.pointers.get(this.prop);
            
            for (let i = 0; i < siblings.length; i++) {
                if (siblings[i].uniqueID === this.uniqueID) {
                    siblings.splice(i, 1);
                    
                    break;
                }
            }
        }
        
        this.mounted = false;
    }
}
