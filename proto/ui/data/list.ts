'use strict';

class List<DataType = any> extends Ground {
    public id: string;
    public items = new AssocList<string, Item<DataType>>();
    public pointers = new AssocList<string, ListPointer<DataType>>();
    public appliedEventIDs = new Map<string, boolean>();
    
    constructor(id: string) {
        super();
        
        this.id = id;
    }
    
    protected catchWaitingList(): void {
        if (Data.listPointersWaiting.has(this.id)) {
            const waitingList = Data.listPointersWaiting.get(this.id);
            
            for (let i = 0; i < waitingList.length; i++) {
                waitingList[i].mount();
                waitingList[i].notifyMountUI();
            }
            
            Data.listPointersWaiting.delete(this.id);
        }
    }
    
    public mount(): void {
        this.catchWaitingList();
    }
    
    clean() {
        this.items.clean((item, id) => {
            this.remove(id);
        });
    }
    
    add(structure: DataType, id = "") {
        if (id.length === 0) {
            id = performance.now() + "_" + this.random(1, 99999);
        }
        
        this.items.set(id, new Item(id, structure));
        
        this.broadcastAdd(id);
    }
    
    remove(id = "") {
        this.items.delete(id);
        
        this.broadcastRemove(id);
    }
    
    sort(elementID1 = "", elementID2 = "", type = 0) {
        this.broadcastSort(elementID1, elementID2, type);
    }
    
    unmount() {
        this.broadcastUnmount();
    }
    
    broadcastAdd(id: string) {
        this.pointers.foreach(pointer => {
            if (pointer.onAdd) {
                pointer.onAdd(id);
            }
        });
    }
    
    broadcastRemove(id: string) {
        this.pointers.foreach(pointer => {
            if (pointer.onRemove) {
                pointer.onRemove(id);
            }
        });
    }
    
    broadcastSort(elementID1 = "", elementID2 = "", type = 0) {
        if (type === 0) { // beforeID - afterID
            this.items.sort(elementID2, elementID1);
        } else { // afterID - beforeID
            if (elementID1 === this.items.last.id) {
                this.items.sort(elementID2, null);
            } else {
                this.items.sort(elementID1, elementID2);
            }
        }
        
        this.pointers.foreach(pointer => {
            if (pointer.onSort) {
                pointer.onSort(elementID1, elementID2, type);
            }
        });
    }
    
    broadcastUnmount() {
        this.pointers.foreach(pointer => {
            if (pointer.onUnmount) {
                pointer.onUnmount();
            }
        });
    }
}
