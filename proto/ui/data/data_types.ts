class Vec3D {
    public x: number = 0;
    public y: number = 0;
    public z: number = 0;
    
    public percentX: boolean = false;
    public percentY: boolean = false;
    public percentZ: boolean = false;
    
    public offsetX: number = 0;
    public offsetY: number = 0;
    public offsetZ: number = 0;
    
    constructor(data?: Partial<Vec3D>) {
        if (data) {
            this.x = typeof data.x === "undefined" ? 0 : data.x;
            this.y = typeof data.y === "undefined" ? 0 : data.y;
            this.z = typeof data.z === "undefined" ? 0 : data.z;
            this.percentX = typeof data.percentX === "undefined" ? false : data.percentX;
            this.percentY = typeof data.percentY === "undefined" ? false : data.percentY;
            this.percentZ = typeof data.percentZ === "undefined" ? false : data.percentZ;
            this.offsetX = typeof data.offsetX === "undefined" ? 0 : data.offsetX;
            this.offsetY = typeof data.offsetY === "undefined" ? 0 : data.offsetY;
            this.offsetZ = typeof data.offsetZ === "undefined" ? 0 : data.offsetZ;
        }
    }
}

class Pos extends Vec3D {
    
}

class Size extends Vec3D {
    
}
