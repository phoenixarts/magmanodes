'use strict';

interface AssocArrayType<T, I> {
    item: T;
    id: I;
}

class AssocArray<I = string, T = any> {
    protected items: AssocArrayType<T, I>[] = [];
    protected ids: Map<I, () => AssocArrayType<T, I>> = new Map();
    public size: number = 0;
    
    public get(id: I): T {
        return this.ids.get(id)().item;
    }
    
    public foreach(callback: (item: T, id?: I) => boolean | unknown): void {
        for (let i = 0; i < this.items.length; i++) {
            const result: unknown | boolean = callback(this.items[i].item, this.items[i].id);
            
            if (typeof result === "boolean") {
                if (result === false) {
                    break;
                }
            }
        }
    }
    
    public forEach(callback: (item: T, id?: I) => boolean | unknown): void {
        this.foreach(callback);
    }
    
    public has(id: I): boolean {
        return this.ids.has(id);
    }
    
    public sort(callback: (a: T, b: T, aID?: I, bID?: I) => number): void {
        this.items.sort((a, b) => callback(a.item, b.item, a.id, b.id));
    }
    
    public set(id: I, item: T): void {
        if (this.has(id)) {
            this.delete(id);
        }
        
        const obj: AssocArrayType<T, I> = {
            item,
            id
        };
        const func = ((obj: T): T => obj).bind(this, obj);
        
        this.items.push(obj);
        this.ids.set(id, func);
        
        this.size++;
    }
    
    public clean(callback?: (item: T, id?: I) => boolean | unknown) {
        this.foreach((item, id) => {
            callback(item, id);
        });
        
        this.ids.clear();
        this.items = [];
        this.size = 0;
    }
    
    public delete(id: I): void {
        if (this.ids.has(id) === false) {
            return;
        }
        
        this.ids.delete(id);
        
        for (let i = 0; i < this.items.length; i++) {
            if (this.items[i].id === id) {
                this.items.splice(i, 1);
                
                break;
            }
        }
        
        this.size--;
    }
}

interface AssocNode<T, I> {
    data: T;
    id: I;
    prev: AssocNode<T, I> | null;
    next: AssocNode<T, I> | null;
}

class AssocList<I, T> extends Unit {
    protected ids: Map<I, () => AssocNode<T, I>> = new Map();
    protected indexes: Map<number, () => AssocNode<T, I>> = new Map();
    public firstNode: AssocNode<T, I> | null = null;
    public lastNode: AssocNode<T, I> | null = null;
    public size: number = 0;
    
    public constructor(items?: [I, T][] | T[]) {
        super();
        
        if (items) {
            for (let i = 0; i < items.length; i++) {
                if (items[i] instanceof Array) {
                    this.set(items[i][0], items[i][1]);
                } else {
                    this.add(items[i] as T);
                }
            }
        }
    }

    /**
     *          --- Pawel ---
     * This method returns id of certain index
     * @param id - index in the array
     */
    public readId(id: number): I {
        return [...this.ids.keys()][id]
    }
    
    /**
     *          --- Pawel ---
     * This method returns all the ids
     */
    public readIds(): Array<I> {
        return Array.from(this.ids.keys())
    }

    public export(): [I, T][] {
        const arr: [I, T][] = [];
        
        this.foreach((item, id) => {
            arr.push([id, item]);
        });
        
        return arr;
    }
    
    public get first(): AssocNode<T, I> | null {
        return this.firstNode;
    }
    
    public get last(): AssocNode<T, I> | null {
        return this.lastNode;
    }
    
    public add(item: T): string {
        if (this instanceof Mix && typeof item === "string") {
            this.set(item as any, item);
            
            return item;
        }
        
        const id = this.uniqueID();
        
        this.set(id as any, item);
        
        return id;
    }
    
    public set(id: I, item: T): void {
        const node: AssocNode<T, I> = {
            data: item,
            id,
            prev: this.lastNode,
            next: null
        }
        
        if (this.lastNode !== null) {
            this.lastNode.next = node;
        }
        
        if (this.firstNode === null) {
            this.firstNode = node;
        }
        
        this.lastNode = node;
        
        this.ids.set(id, () => node);
        
        this.size++;
    }
    
    public index(n: number): T {
        let i = 0;
        let result:T = null;
        
        this.foreach(item => {
            if (i === n) {
                result = item;
                
                return false;
            }
            
            i++
        });
        
        return result;
    }
    
    public iteratedSort(callback: (a: T, b: T) => number): void {
        // ...
    }
    
    public sort(beforeID: I, afterID: I): void {
        if (beforeID === afterID) {
            return;
        }
        
        if (this.ids.has(beforeID) === false) {
            return;
        }
        
        const before = this.getNode(beforeID);
        
        if (before.prev && before.next) {
            before.next.prev = before.prev;
            before.prev.next = before.next;
        } else if (before.prev) {
            before.prev.next = null;
            
            this.lastNode = before.prev;
        } else if (before.next) {
            before.next.prev = null;
            
            this.firstNode = before.next;
        }
        
        if (afterID === null) {
            this.lastNode.next = before;
            before.prev = this.lastNode;
            before.next = null;
            this.lastNode = before;
            
            return;
        }
        
        const after = this.getNode(afterID);
        
        if (this.ids.has(afterID) === false) {
            return;
        }
        
        if (after.prev) {
            after.prev.next = before;
            before.prev = after.prev;
        } else {
            before.prev = null;
            
            this.firstNode = before;
        }
        
        before.next = after;
        after.prev = before;
        
        if (after.id === this.first.id) {
            before.prev = null;
            this.firstNode = before;
        }
    }
    
    public has(id: I): boolean {
        return this.ids.has(id);
    }

    /**
     *          --- Pawel ---
     * Get multiple data based on multiple ids
     * @param ids - IDs of an element
     */
    public getMore(...ids: I[]): T[] {
        const result = []
        for (const id of ids) {
            
            if (this.has(id) === false) {
                return null
            }
            result.push(this.ids.get(id)().data)
        }
        return result
    }
    
    public get(id: I): T {
        if (this.has(id) === false) {
            return null;
        }
        
        return this.ids.get(id)().data;
    }
    
    public getNode(id: I): AssocNode<T, I> {
        return this.ids.get(id)();
    }
    
    public clean(callback?: (item: T, id?: I) => boolean | unknown): void {
        if (callback) {
            this.foreach(callback);
        }
        
        this.ids = new Map<I, () => AssocNode<T, I>>();
        this.firstNode = null;
        this.lastNode = null;
        this.size = 0;
    }
    
    public delete(id: I): void {
        if (this.has(id) === false) {
            return;
        }
        
        let firstLastDelete = false;
        
        if (this.firstNode.id === id) {
            if (this.firstNode.next === null) {
                this.firstNode = null;
            } else {
                if (this.firstNode.next.id === id) {
                    this.firstNode = null;
                    this.lastNode = null;
                } else {
                    this.firstNode.next.prev = null;
                    this.firstNode = this.firstNode.next;
                }
            }
            
            firstLastDelete = true;
        }
        
        if (this.lastNode.id === id) {
            if (this.lastNode.prev === null) {
                this.lastNode = null;
            } else {
                if (this.lastNode.prev.id === id) {
                    this.firstNode = null;
                    this.lastNode = null;
                } else {
                    this.lastNode.prev.next = null;
                    this.lastNode = this.lastNode.prev;
                }
            }
            
            firstLastDelete = true;
        }
        
        if (firstLastDelete === false) {
            const node = this.ids.get(id)();
            
            if (node.prev !== null && node.next !== null) {
                node.prev.next = node.next;
                node.next.prev = node.prev;
            }
        }
        
        this.ids.delete(id);
        
        this.size--;
    }
    
    public foreach(callback: (item: T, id?: I) => boolean | unknown, reversed: boolean = false): void {
        let next: AssocNode<T, I> | null = null;
        
        for (let i = 0; i < 1; i) {
            if (reversed === false) {
                next = this.doNext(next, callback);
            } else {
                next = this.doPrev(next, callback);
            }
            
            if (next === null) {
                break;
            }
        }
    }
    
    public forEach(callback: (item: T, id?: I) => boolean | unknown): void {
        this.foreach(callback);
    }
    
    protected doNext(node: AssocNode<T, I> | null, callback: (item: T, id?: I) => boolean | unknown): AssocNode<T, I> | null {
        if (node === null) {
            node = this.firstNode;
        }
        
        if (node === null) {
            return null;
        }
        
        const result: unknown | boolean = callback(node.data, node.id);
        
        if (typeof result === "boolean") {
            if (result === false) {
                return null;
            }
        }
        
        return node.next;
    }
    
    protected doPrev(node: AssocNode<T, I> | null, callback: (item: T, id?: I) => boolean | unknown): AssocNode<T, I> | null {
        if (node === null) {
            node = this.lastNode;
        }
        
        if (node === null) {
            return null;
        }
        
        const result: unknown | boolean = callback(node.data, node.id);
        
        if (typeof result === "boolean") {
            if (result === false) {
                return null;
            }
        }
        
        return node.prev;
    }
    
    public copy(): Arr<I, T> {
        const copy = new Arr<I, T>();
        
        this.foreach((item, id) => {
            copy.set(id, item);
        });
        
        return copy;
    }
    
    public get array(): T[] {
        const arr = [];
        
        this.foreach(item => {
            arr.push(item);
        });
        
        return arr;
    }
    
    public get map(): Map<I, T> {
        const map = new Map<I, T>();;
        
        this.foreach((item, id) => {
            map.set(id, item);
        });
        
        return map;
    }
    
    // Solid debug purposes
    public get obj(): any {
        const obj: any = {};
        
        this.foreach((item, id) => {
            obj[id] = item;
        });
        
        return obj;
    }
}

class Arr<I = any, T = any> extends AssocList<I, T> {}
class Mix<T = any> extends Arr<string, T> {}
