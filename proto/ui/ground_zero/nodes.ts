'use strict';

declare type OnStateUpdateFunction = (newValue?: any, oldValue?: any) => void;

class NodeUI extends Ground {
    public node: HTMLElement | HTMLInputElement;
    public options: OptionsUI;
    public transform = new Transform(this);
    public rect: DOMRect | ClientRect | null = null;
    
    constructor(node = new HTMLElement(), options: OptionsUI) {
        super();
        
        this.node = node;
        this.options = options;
    }
}

class NodeState extends Ground {
    public pointers: Pointer<any>[];
    public onUpdate: StateUpdateFunction;
    public rewritable: Boolean;
    
    constructor(pointers: Pointer[], onUpdate: StateUpdateFunction, rewritable: Boolean = false) {
        super();
        
        this.pointers = pointers;
        this.onUpdate = onUpdate;
        this.rewritable = rewritable;
    }
}

class NodeValue extends Ground {
    public key: string;
    public value: any;
    
    constructor(key: string, value: any) {
        super();
        
        this.key = key;
        this.value = value;
    }
}

class NodeComment extends Ground {
    public comment: Comment;
    public pointers: Pointer<any>[];
    public onUpdate: () => boolean;
    public elementReference: () => (new() => UI);
    public data: () => any;
    public name: string;
    
    constructor(
        comment: Comment,
        pointers: Pointer<any>[],
        onUpdate: () => boolean,
        elementReference: () => (new() => UI),
        data: () => any,
        name: string
    ) {
        super();
        
        this.comment = comment;
        this.pointers = pointers;
        this.onUpdate = onUpdate;
        this.elementReference = elementReference;
        this.data = data;
        this.name = name;
    }
}

class NodePosition extends Ground {
    public comment: Comment;
    public pointers: Pointer<any>[];
    public elementName: string;
    public condition: () => boolean;
    public name: string;
    
    constructor(
        comment: Comment,
        pointers: Pointer<any>[],
        elementName = "",
        condition: () => boolean,
        name: string
    ) {
        super();
        
        this.comment = comment;
        this.pointers = pointers;
        this.elementName = elementName;
        this.condition = condition;
        this.name = name.length === 0 ? "position_" + performance.now() + "_" + this.random(1, 99999) : name;
    }
}

class NodeSwitch extends Ground {
    public comment: Comment;
    public pointers: Pointer<any>[];
    public onUpdate: () => any;
    public elementReference: () => (new() => UI);
    public data: () => any;
    public name: string;
    public refreshable: boolean;
    
    constructor(
        comment: Comment,
        pointers: Pointer<any>[],
        onUpdate: () => boolean,
        elementReference: () => (new() => UI),
        data: () => any,
        name: string,
        refreshable: boolean
    ) {
        super();
        
        this.comment = comment;
        this.pointers = pointers;
        this.onUpdate = onUpdate;
        this.elementReference = elementReference;
        this.data = data;
        this.name = name;
        this.refreshable = refreshable;
    }
}

class NodeElement extends Ground {
    public ref: new() => UI;
    public data: any;
    public name: string;
    
    constructor(
        ref = UI,
        data = {},
        name = ""
    ) {
        super();
        
        this.ref = ref;
        this.data = data;
        this.name = name;
    }
}
