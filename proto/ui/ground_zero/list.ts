'use strict';

interface ListUIData {
    listNode: NodeUI,
    listID: string,
    itemClassReference: new() => UI
}

class ListUI<DataType> extends UI<ListUIData> {
    public listPointer: ListPointer<DataType> | null = null;
    public sorting: boolean = false;
    protected itemClassReference: new() => UI;
    
    mounted() {
        this.listPointer = new ListPointer<DataType>((performance.now() + "_" + this.random(1, 99999)), this.options.listID);
        this.itemClassReference = this.options.itemClassReference;
        
        if (this.listPointer.mounted === false) {
            this.listPointer.mount();
        }
        
        if (this.listPointer.mounted === false) {
            // console.warn("Failed to mount listPointer[\"" + this.listPointer.listID + "\"]");
             
            // return;
        }
        
        this.listPointer.onAdd = (id = "") => {
            const element = this.element<DataType>(this.itemClassReference, this.listPointer.item(id).data, id);
            
            element.mount();
            
            this.node.node.appendChild(element.node.node);
        };
        
        this.listPointer.onRemove = (id = "") => {
            if (this.elements.has(id) === false) {
                console.warn("Trying to remove a nonexisting element.");
                
                return;
            }
            
            const element = this.elements.get(id);
            
            element.unmount();
        };
        
        this.listPointer.onSort = (elementID1 = "", elementID2 = "", type = 0) => {
            if (this.sorting) {
                return;
            }
            
            if (type === 0) {
                this.elements.get(elementID1).node.node.before(this.elements.get(elementID2).node.node);
            } else {
                this.elements.get(elementID1).node.node.after(this.elements.get(elementID2).node.node);
            }
        };
        
        if (this.listPointer.mounted) {
            this.mountList();
        } else {
            this.listPointer.onWaitedMount = this.mountList.bind(this);
        } 
    }
    
    protected mountList(): void {
        this.listPointer.list.items.forEach((item, id: string) => {
            const element = this.element(this.itemClassReference, item.data, id);
            
            element.mount();
            
            this.node.node.appendChild(element.node.node);
        });
    }
    
    onUnmount(unmount = () => {}) {
        this.listPointer.unmount();
        
        unmount();
    }
    
    display() {
        return this.options.listNode;
    }
}

// var variable1 = false;
// var variable2 = false;

// function MyAction(param1, param2, param3) {
//     variable1 = param3;
    
//     if (variable1 == true) {
//         anotherFunction1(param2);
        
//         variable2 = param2;
        
//         if (variable2 == true) {
//             anotherFunction2(param1);
//         }
//     }
// }



// function anotherFunction1(param) {
//     /* Logic */
// }

// function anotherFunction2(param) {
//     /* Logic */
// }

// MathematicalPatternSet [
//     "addition",
//     "subtraction",
//     "multiplication",
//     "division",
//     "delta"
// ]

// ConditionTypes [
//     "compareEqual",
//     "compareNotEqual",
//     "compareGreaterThan",
//     "compareLesserThan",
//     "compareGreaterOrEqual",
//     "compareLesserOrEqual"
// ]

// ArchitecturalBasis [
//     "make1FunctionOutofTwoFunction",
//     "make1FunctionOutofTwoFunction",
//     "make1FunctionOutofTwoFunction",
//     "make1FunctionOutofTwoFunction",
//     "make1FunctionOutofTwoFunction",
//     "make1FunctionOutofTwoFunction",
// ]


// [
//     memorized outcome result 1 = something
//     memorized outcome result 1 = something
//     memorized outcome result 1 = something
//     memorized outcome result 1 = something
//     memorized outcome result 1 = something
// ]

// function calculator(num1, num2, operator) {
//     if (operator === "+") {
//         return num1 + num2;
//     }
    
//     if (operator === "-") {
//         return num1 - num2;
//     }
    
//     if (operator === "*") {
//         return num1 * num2;
//     }
    
//     if (operator === "/") {
//         return num1 / num2;
//     }
    
//     if (operator === "%") {
//         return num1 % num2;
//     }
    
//     if (operator === "&") {
//         return num1 % num2;
//     }
// }


// calculator(5, 10, "+"); // 15

// context1
// context2
// context3

// test [
//     before this state change happens
//         to these contexts
//     this state change needs to happen
//         to these contexts
// ]

// State change [
//     context1+
//     context2-
//     context3+
// ]

// Creation [
//     State [
//         context4-
//         context5+
//         context6-
//     ]
// ]

// const arr = []; // basket

// function makeAOrderedBasket(numbers) {
//     for (let i = 0; i < numbers; i++) {
//         arr.push(i);
//     }
// }

// [0 /* signed number on it with pencil */, 1, 2, 3, 4, 5, 6, 7, 8, 9 /* eggs numbered, ordered ASC */] = makeAOrderedBasket(10);

function node(options?: any) {
    
}

function style(options?: any) {
    
}

function background(options?: any) {
    
}

/*

node[
    node[
        style[
            background = #fff
        ]
        node[
            style[
                fontSize: 20px
                height: pointer(p)
            ]
            node
            node
            MyComponent[
                node
                AnotherComponent
                node
                MyOtherComponent[
                    node
                    node
                ]
            ]
        ]
        node
    ]
    node[
        
    ]
]


*/



// YOU CAN DO IT™
// action running() {
//     a = 5;
//     a = b;
//     a = change

//     a + b
//     a + change
//     change + b
//     change + change

//     a = (a / (b % y)) + ((b - u) * c)
// }


// 1. The AI itself
// 2. The creation of the AI


// act running(); // JUST DO IT™

// we are in total control [
//     1
//     2
//     3
//     4
//     5
//     6
//     6
//     7
//     8
//     9
//     10
//     ...
// ]

// Terminator
// Regulations

// ACTION CREATION CoolButtonClick

// if (context is a button) {
//     if (context is clicked) {
//         context does that
        
//         this and that happens
        
//         if (this and that happens) {
//             this needs to do that
            
//             this happens combine self + another context
//         }
//     }
// }

// [Soft Language] -> [Hard Language]
// me: hey AI do this do that
//     // clear explanation monitoring, e.g. this is what you actually mean
//     if (context is a button) {
//         if (context is clicked) {
//             context does that
            
//             this and that happens
            
//             if (this and that happens) {
//                 this needs to do that
                
//                 this happens combine self + another context
//             }
//         }
//     }
// AI: ok I have a question

// [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
