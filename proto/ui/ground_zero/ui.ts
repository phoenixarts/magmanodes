'use strict';

declare type MaybeUI = UI | null;
declare type StateUpdateFunction = (update: (data: NodeValue[] | string | boolean | number) => NodeValue[] | void, newVal?: any, oldVal?: any) => void;

Data.create("ui");

interface UIData {
    cursor: Pos,
    windowResponderActive: boolean,
    windowKeyDownEvents: any[],
    windowKeyUpEvents: any[],
    dragging: boolean,
    dragElement: UI<any> | null,
    dragPlaceholder: HTMLDivElement | null,
    dragPos: Pos,
    dragLock: boolean,
}

const uiListPointer = new ListPointer<UIData>("ui", "ui");

uiListPointer.add({
    cursor: new Pos(),
    windowResponderActive: false,
    windowKeyDownEvents: [],
    windowKeyUpEvents: [],
    dragging: false,
    dragElement: null,
    dragPlaceholder: null,
    dragPos: new Pos(),
    dragLock: false,
}, "main");

const View = {
    cursor: new Pointer<Pos>("ui", "main", "cursor"),
    windowResponderActive: new Pointer<boolean>("ui", "main", "windowResponderActive"),
    windowKeyDownEvents: new Pointer<any[]>("ui", "main", "windowKeyDownEvents"),
    windowKeyUpEvents: new Pointer<any[]>("ui", "main", "windowKeyUpEvents"),
    dragging: new Pointer<boolean>("ui", "main", "dragging"),
    dragElement: new Pointer<UI<any> | null>("ui", "main", "dragElement"),
    dragPlaceholder: new Pointer<HTMLDivElement | null>("ui", "main", "dragPlaceholder"),
    dragPos: new Pointer<Pos>("ui", "main", "dragPos"),
    dragLock: new Pointer<boolean>("ui", "main", "dragLock"),
};

class UI<OptionsType = {}> extends ShortcutsUI {
    public parent: UI | null;
    public elements = new Map<string, UI>();
    public nodes = new Map<string, NodeUI>();
    public node: NodeUI = null;
    public name: string = "";
    public options: OptionsType = null;
    public propPointers = new Map<string, Pointer<any>[]>();
    public interactive: InteractiveUI = null;
    public isMounted: boolean = false;
    
    private _windowMouseUp = null;
    private _windowMouseDown = null;
    private _windowMouseMove = null;
    
    setup(parent: UI | null, options: OptionsType) {
        this.parent = parent;
        this.options = options;
        this.interactive = new InteractiveUI(this);
    }
    
    initialise(query = "") {
        const domNode = document.querySelector(query);
        
        if (domNode === null) {
            throw "UI Initialization failed, query[\"" + query + "\"] cannot find node.";
        }
        
        this.name = "root";
        
        this.mount();
        
        domNode.appendChild(this.node.node);
        
        this.prepareWindow();
    }
    
    _windowMouseUpEvent(ev) {
        window.removeEventListener("mousemove", this._windowMouseMove);
        
        View.windowResponderActive.value = false;
    }
    
    _windowMouseMoveEvent(ev) {
        View.cursor.value.x = ev.pageX;
        View.cursor.value.y = ev.pageY;
        View.cursor.update();
    }
    
    _windowMouseDownEvent(ev) {
        View.cursor.value.x = ev.pageX;
        View.cursor.value.y = ev.pageY;
        View.cursor.update();
        View.windowResponderActive.value = true;
        
        window.addEventListener("mousemove", this._windowMouseMove, false);
    }
    
    prepareWindow() {
        this._windowMouseUp = this._windowMouseUpEvent.bind(this);
        this._windowMouseMove = this._windowMouseMoveEvent.bind(this);
        this._windowMouseDown = this._windowMouseDownEvent.bind(this);
        
        window.addEventListener("mouseup", this._windowMouseUp, false);
        window.addEventListener("mousedown", this._windowMouseDown, false);
    }
    
    releaseWindow() {
        window.removeEventListener("mouseup", this._windowMouseUp);
        window.removeEventListener("mousedown", this._windowMouseDown);
    }
    
    mount(external: boolean = false) {
        this.beforeMount();
        
        const node = this.display();
        
        this.nodes.delete(node.options.name);
        
        let baseObj = Object.getPrototypeOf(this);
        let searchingForParents = true;
        
        while (searchingForParents) {
            if (baseObj.__proto__ === null) {
                searchingForParents = false;
                
                break;
            }
            
            node.node.classList.add(baseObj.__proto__.constructor.name);
            
            baseObj = baseObj.__proto__;
        }
        
        baseObj = null;
        
        node.options.name = "main";
        
        node.node.classList.add(this.constructor.name);
        node.node.classList.add("main");
        
        if (this.name.length !== 0) {
            node.node.classList.add(this.name);
        }
        
        this.nodes.set(node.options.name, node);
        this.node = node;
        
        this.isMounted = true;
        
        if (external === false) {
            this.mounted();
        }
    }
    
    unmount(done?: VoidFunction) {
        this.onUnmount(() => {
            this.onUnmountSync();
            this.doUnmount();
            
            if (done) {
                done();
            }
        });
    }
    
    protected onUnmountSync() {
        
    }
    
    doUnmount() {
        this.interactive.move.unmount();
        this.interactive.resize.unmount();
        this.interactive.drag.unmount();
        this.interactive.drop.unmount();
        this.interactive.sort.unmount();
        this.interactive.padding.unmount();
        
        this.nodes.forEach(node => {
            // Problem ID[1]: this can cause sub element async unmount to lose the parent node of it's main node
            // Solution?
            this.cleanNodeEvents(node);
        });
        
        // Problem ID[1]
        if (this.node.node.parentNode !== null) {
            this.node.node.parentNode.removeChild(this.node.node);
        }
        
        if (this.parent !== null) {
            this.parent.elements.delete(this.name);
        }
        
        this.propPointers.forEach(pointers => {
            for (let i = 0; i < pointers.length; i++) {
                pointers[i].unmount();
            }
        });
        
        this.propPointers.clear();
        
        this.node = null;
        
        this.nodes.clear();
        
        this.elements.forEach(element => {
            element.unmount();
        });
    }
    
    element<O = {}>(classReference: new() => UI, options?: O, name: string = "") {
        if (name.length === 0) {
            name = classReference.name;
        }
        
        const element = new classReference();
        
        element.setup(this, options);
        
        element.name = name;
        
        this.elements.set(name, element);
        
        return element;
    }
    
    addElement<O = {}>(nodeName: string, classReference: new() => UI, options: O, name: string = "") {
        const element = this.element(classReference, options, name);
        const node = this.nodes.get(nodeName);
        
        this.applyElement(node, element);
    }
    
    create(type: string, options: Partial<OptionsUI>, children: UIChildren) {
        const node = new NodeUI(document.createElement(type), new OptionsUI(options));
        
        this.setupNodeClass(node);
        this.setupNodeSize(node);
        this.mountNode(node);
        this.setupFormProbability(node, type, children);
        this.setupNodeAttributes(node);
        this.setupNodeType(node);
        this.setupNodeText(node);
        this.setupNodeValue(node);
        this.setupNodeStyle(node);
        this.setupNodeEvents(node);
        this.applyChildren(node, children);
        
        return node;
    }
    
    mountNode(node: NodeUI) {
        if (node.options.name.length === 0) {
            node.options.name = performance.now() + "_" + this.random(1, 99999);
        } else {
            node.node.classList.add(node.options.name);
        }
        
        this.nodes.set(node.options.name, node);
    }
    
    applyChildren(node: NodeUI, children: Array<NodeUI|NodeComment|NodePosition|NodeElement|UI|null>) {
        if (children.length !== 0) {
            for (let i = 0; i < children.length; i++) {
                const applicableNode = children[i];
                
                if (applicableNode !== null) {
                    if (applicableNode instanceof NodeUI) {
                        this.applyNode(node, applicableNode);
                    } else if (applicableNode instanceof NodeComment) {
                        this.applyStateMount(node, applicableNode);
                    } else if (applicableNode instanceof NodeSwitch) {
                        this.applyStateSwitch(node, applicableNode);
                    } else if (applicableNode instanceof NodePosition) {
                        this.applyStatePosition(node, applicableNode);
                    } else if (applicableNode instanceof UI) {
                        this.applyElement(node, applicableNode);
                    }
                }
            }
        }
    }
    
    applyNode(node: NodeUI, child: NodeUI) {
        node.node.appendChild(child.node);
    }
    
    applyStateMount(node: NodeUI, child: NodeComment): void {
        if (child.name.length === 0) {
            if (child.elementReference()) {
                child.name = child.elementReference().name;
            }
        }
        
        node.node.appendChild(child.comment);
        
        // Memory leak?
        const pointers = [];
        
        for (let i = 0; i < child.pointers.length; i++) {
            pointers.push(new Pointer(
                child.pointers[i].listID,
                child.pointers[i].itemID,
                child.pointers[i].prop,
                () => {
                    const result = child.onUpdate();
                    
                    if (result) {
                        if (this.elements.has(child.name) === false) {
                            const element = this.element(child.elementReference(), child.data(), child.name);
                            
                            element.mount();
                            
                            child.comment.after(element.node.node);
                        }
                    } else {
                        if (this.elements.has(child.name)) {
                            this.elements.get(child.name).unmount();
                            this.elements.delete(child.name);
                        }
                    }
                }
            ));
            
            this.propPointers.set(child.name + "_mounts", pointers);
            
            pointers[pointers.length - 1].onUpdate();
        }
    }
    
    applyStateSwitch(node: NodeUI, child: NodeSwitch): void {
        if (child.name.length === 0) {
            if (child.elementReference()) {
                child.name = child.elementReference().name;
            }
        }
        
        node.node.appendChild(child.comment);
        
        // Memory leak?
        const pointers = [];
        let lastResult: any = "";
        
        for (let i = 0; i < child.pointers.length; i++) {
            const p = new Pointer(
                child.pointers[i].listID,
                child.pointers[i].itemID,
                child.pointers[i].prop,
                () => {
                    const result = child.onUpdate();
                    
                    if ((result !== lastResult || child.refreshable) && p.value !== null) {
                        lastResult = result;
                        
                        if (this.elements.has(child.name)) {
                            this.elements.get(child.name).unmount();
                            this.elements.delete(child.name);
                        }
                        
                        const element = this.element(result, child.data(), child.name);
                        
                        element.mount();
                        
                        child.comment.after(element.node.node);
                    }
                }
            );
            
            pointers.push(p);
            
            this.propPointers.set(child.name + "_switches", pointers);
            
            pointers[pointers.length - 1].onUpdate();
        }
    }
    
    applyStatePosition(node: NodeUI, child: NodePosition): void {
        node.node.appendChild(child.comment);
        
        const pointers = [];
        
        for (let i = 0; i < child.pointers.length; i++) {
            pointers.push(new Pointer(
                child.pointers[i].listID,
                child.pointers[i].itemID,
                child.pointers[i].prop,
                () => {
                    const result = child.condition();
                    
                    if (result) {
                        if (this.elements.has(child.elementName)) {
                            child.comment.after(this.elements.get(child.elementName).node.node);
                        }
                    } else {
                        
                    }
                }
            ));
            
            this.propPointers.set(child.name + "_positions", pointers);
            
            setTimeout(() => {
                pointers[pointers.length - 1].onUpdate();
            }, 0);
        }
    }
    
    applyElement(node: NodeUI, child: UI) {
        child.mount();
        node.node.appendChild(child.node.node);
    }
    
    /** @todo Implement statefullness to attributes */
    public setupNodeAttributes(node: NodeUI): void {
        for (let i = 0; i < node.options.attributes.length; i++) {
            node.node.setAttribute(node.options.attributes[i].name, node.options.attributes[i].value);
        }
    }
    
    setupFormProbability(node: NodeUI, type: string, children: UIChildren) {
        if (type === "form") {
            children.push(this.input({
                type: "submit",
                style: "display: none;"
            }));
            
            if (node.options.onSubmit === null) {
                node.options.onSubmit = () => {};
            }
        }
    }
    
    setupNodeType(node: NodeUI) {
        if (node.options.type.length !== 0) {
            node.node.setAttribute("type", node.options.type);
        }
    }
    
    setupNodeText(node: NodeUI) {
        if (node.options.text.length !== 0) {
            node.node.innerText = node.options.text;
        }
        
        if (node.options.html.length !== 0) {
            node.node.innerHTML = node.options.html;
        }
        
        if (node.options.stateText !== null) {
            const pointers: Pointer<any>[] = [];
            const func = (newVal?: any, oldVal?: any) => {
                if (
                    node.options.stateText.rewritable === false && 
                    newVal === oldVal && 
                    typeof newVal !== "undefined" && 
                    typeof oldVal !== "undefined"
                ) {
                    return;
                }
                
                node.options.stateText.onUpdate(updateFunc, newVal, oldVal);
            };
            const updateFunc = (value: string) => {
                node.node.innerText = value;
            }
            
            for (let i = 0; i < node.options.stateText.pointers.length; i++) {
                pointers.push(new Pointer(
                    node.options.stateText.pointers[i].listID,
                    node.options.stateText.pointers[i].itemID,
                    node.options.stateText.pointers[i].prop,
                    func
                ));
            }
            
            this.propPointers.set(node.options.name + "_text", pointers);
            
            func();
        }
    }
    
    setupNodeValue(node: NodeUI) {
        if (node.options.placeholder.length !== 0) {
            if (
                node.node instanceof HTMLInputElement ||
                node.node instanceof HTMLTextAreaElement
            ) {
                node.node.placeholder = node.options.placeholder;
            }
        }
        
        if (node.options.value.length !== 0) {
            if (
                node.node instanceof HTMLInputElement ||
                node.node instanceof HTMLTextAreaElement ||
                node.node instanceof HTMLButtonElement
            ) {
                node.node.value = node.options.value;
            }
        }
        
        if (node.options.stateValue !== null) {
            const pointers: Pointer<any>[] = [];
            const func = (newVal?: any, oldVal?: any) => {
                if (
                    node.options.stateValue.rewritable === false && 
                    newVal === oldVal && 
                    typeof newVal !== "undefined" && 
                    typeof oldVal !== "undefined"
                ) {
                    return;
                }
                
                node.options.stateValue.onUpdate(updateFunc, newVal, oldVal);
            };
            const updateFunc = (value: string) => {
                if (
                    node.node instanceof HTMLInputElement ||
                    node.node instanceof HTMLTextAreaElement ||
                    node.node instanceof HTMLButtonElement
                ) {
                    node.node.value = value;
                }
            }
            
            for (let i = 0; i < node.options.stateValue.pointers.length; i++) {
                pointers.push(new Pointer(
                    node.options.stateValue.pointers[i].listID,
                    node.options.stateValue.pointers[i].itemID,
                    node.options.stateValue.pointers[i].prop,
                    func
                ));
            }
            
            this.propPointers.set(node.options.name + "_value", pointers);
            
            func();
        }
    }
    
    setupNodeStyle(node: any) {
        if (node.options.style.length !== 0) {
            node.node.style = node.options.style;
        }
        
        if (node.options.stateStyle !== null) {
            const pointers = [];
            const func = (newVal?: any, oldVal?: any) => {
                if (
                    node.options.stateStyle.rewritable === false && 
                    newVal === oldVal && 
                    typeof newVal !== "undefined" && 
                    typeof oldVal !== "undefined"
                ) {
                    return;
                }
                
                node.options.stateStyle.onUpdate(updateFunc, newVal, oldVal);
            };
            const updateFunc = (styles: NodeValue[]) => {
                for (let i = 0; i < styles.length; i++) {
                    node.node.style[styles[i].key] = styles[i].value;
                }
            }
            
            for (let i = 0; i < node.options.stateStyle.pointers.length; i++) {
                pointers.push(new Pointer(
                    node.options.stateStyle.pointers[i].listID,
                    node.options.stateStyle.pointers[i].itemID,
                    node.options.stateStyle.pointers[i].prop,
                    func
                ));
            }
            
            this.propPointers.set(node.options.name + "_style", pointers);
            
            func();
        }
    }
    
    setupNodeClass(node: NodeUI) {
        if (node.options.className.length !== 0) {
            node.node.className = node.options.className;
        }
        
        if (node.options.stateClass !== null) {
            const pointers = [];
            const func = (newVal?: any, oldVal?: any) => {
                if (
                    node.options.stateClass.rewritable === false && 
                    newVal === oldVal && 
                    typeof newVal !== "undefined" && 
                    typeof oldVal !== "undefined"
                ) {
                    return;
                }
                
                node.options.stateClass.onUpdate(updateFunc, newVal, oldVal);
            };
            const updateFunc = (classes: NodeValue[]) => {
                for (let i = 0; i < classes.length; i++) {
                    if (classes[i].value) {
                        node.node.classList.add(classes[i].key);
                    } else {
                        node.node.classList.remove(classes[i].key);
                    }
                }
            }
            
            for (let i = 0; i < node.options.stateClass.pointers.length; i++) {
                pointers.push(new Pointer(
                    node.options.stateClass.pointers[i].listID,
                    node.options.stateClass.pointers[i].itemID,
                    node.options.stateClass.pointers[i].prop,
                    func
                ));
            }
            
            this.propPointers.set(node.options.name + "_class", pointers);
            
            func();
        }
    }
    
    // Add statefullnes to this?
    // Or add stateful attributes as a whole?
    setupNodeSize(node: NodeUI) {
        if (node.options.width.length) {
            node.node.setAttribute("width", node.options.width);
        }
        
        if (node.options.height.length) {
            node.node.setAttribute("height", node.options.height)
        }
    }
    
    setupNodeEvents(node: NodeUI) {
        if (node.options.onClick !== null) {
            node.node.addEventListener("click", node.options.onClick, false);
        }
        
        if (node.options.onChange !== null) {
            node.node.addEventListener("change", node.options.onChange, false);
        }
        
        if (node.options.onFocus !== null) {
            node.node.addEventListener("focus", node.options.onFocus, false);
        }
        
        if (node.options.onBlur !== null) {
            node.node.addEventListener("blur", node.options.onBlur, false);
        }
        
        if (node.options.onDoubleClick !== null) {
            node.node.addEventListener("dblclick", node.options.onDoubleClick, false);
        }
        
        if (node.options.onMouseDown !== null) {
            node.node.addEventListener("mousedown", node.options.onMouseDown, false);
        }
        
        if (node.options.onMouseUp !== null) {
            node.node.addEventListener("mouseup", node.options.onMouseUp, false);
        }
        
        if (node.options.onMouseOver !== null) {
            node.node.addEventListener("mouseover", node.options.onMouseOver, false);
        }
        
        if (node.options.onMouseOut !== null) {
            node.node.addEventListener("mouseout", node.options.onMouseOut, false);
        }
        
        if (node.options.onMouseEnter !== null) {
            node.node.addEventListener("mouseenter", node.options.onMouseEnter, false);
        }
        
        if (node.options.onMouseLeave !== null) {
            node.node.addEventListener("mouseleave", node.options.onMouseLeave, false);
        }
        
        if (node.options.onMouseMove !== null) {
            node.node.addEventListener("mousemove", node.options.onMouseMove, false);
        }
        
        if (node.options.onRightClick !== null) {
            node.node.addEventListener("contextmenu", node.options.onRightClick, false);
        }
        
        if (node.options.onKeyUp !== null) {
            node.node.addEventListener("keyup", node.options.onKeyUp, false);
        }
        
        if (node.options.onKeyDown !== null) {
            node.node.addEventListener("keydown", node.options.onKeyDown, false);
        }
        
        if (node.options.onScroll !== null) {
            node.node.addEventListener("scroll", node.options.onScroll, false);
        }
        
        if (node.options.onMouseWheel !== null) {
            node.node.addEventListener("mousewheel", node.options.onMouseWheel, false);
        }
        
        if (node.options.onResize !== null) {
            node.node.addEventListener("resize", node.options.onResize, false);
        }
        
        if (node.options.onSubmit !== null) {
            node.options._onSubmit = node.options.onSubmit;
            node.options.onSubmit = ev => {
                ev.preventDefault();
                
                node.options._onSubmit(ev)
            };
            
            node.node.addEventListener("submit", node.options.onSubmit, false);
        }
    }
    
    cleanNodeEvents(node: NodeUI) {
        if (node.options.onClick !== null) {
            node.node.removeEventListener("click", node.options.onClick);
            
            node.options.onClick = null;
        }
        
        if (node.options.onChange !== null) {
            node.node.removeEventListener("change", node.options.onChange);
            
            node.options.onChange = null;
        }
        
        if (node.options.onFocus !== null) {
            node.node.removeEventListener("focus", node.options.onFocus);
            
            node.options.onFocus = null;
        }
        
        if (node.options.onBlur !== null) {
            node.node.removeEventListener("blur", node.options.onBlur);
            
            node.options.onBlur = null;
        }
        
        if (node.options.onDoubleClick !== null) {
            node.node.removeEventListener("dblclick", node.options.onDoubleClick);
            
            node.options.onDoubleClick = null;
        }
        
        if (node.options.onMouseDown !== null) {
            node.node.removeEventListener("mousedown", node.options.onMouseDown);
            
            node.options.onMouseDown = null;
        }
        
        if (node.options.onMouseUp !== null) {
            node.node.removeEventListener("mouseup", node.options.onMouseUp);
            
            node.options.onMouseUp = null;
        }
        
        if (node.options.onMouseOver !== null) {
            node.node.removeEventListener("mouseover", node.options.onMouseOver);
            
            node.options.onMouseOver = null;
        }
        
        if (node.options.onMouseOut !== null) {
            node.node.removeEventListener("mouseout", node.options.onMouseOut);
            
            node.options.onMouseOut = null;
        }
        
        if (node.options.onMouseEnter !== null) {
            node.node.removeEventListener("mouseenter", node.options.onMouseEnter);
            
            node.options.onMouseEnter = null;
        }
        
        if (node.options.onMouseLeave !== null) {
            node.node.removeEventListener("mouseleave", node.options.onMouseLeave);
            
            node.options.onMouseLeave = null;
        }
        
        if (node.options.onMouseMove !== null) {
            node.node.removeEventListener("mousemove", node.options.onMouseMove);
            
            node.options.onMouseMove = null;
        }
        
        if (node.options.onRightClick !== null) {
            node.node.removeEventListener("contextmenu", node.options.onRightClick);
            
            node.options.onRightClick = null;
        }
        
        if (node.options.onKeyUp !== null) {
            node.node.removeEventListener("keyup", node.options.onKeyUp);
            
            node.options.onKeyUp = null;
        }
        
        if (node.options.onKeyDown !== null) {
            node.node.removeEventListener("keydown", node.options.onKeyDown);
            
            node.options.onKeyDown = null;
        }
        
        if (node.options.onScroll !== null) {
            node.node.removeEventListener("scroll", node.options.onScroll);
            
            node.options.onScroll = null;
        }
        
        if (node.options.onMouseWheel !== null) {
            node.node.removeEventListener("mousewheel", node.options.onMouseWheel);
            
            node.options.onMouseWheel = null;
        }
        
        if (node.options.onResize !== null) {
            node.node.removeEventListener("resize", node.options.onResize);
            
            node.options.onResize = null;
        }
        
        if (node.options.onSubmit !== null) {
            node.node.removeEventListener("submit", node.options.onSubmit);
            
            node.options.onSubmit = null;
            node.options._onSubmit = null;
        }
    }
    
    // Batching of updates?
    state(pointers: Pointer<any>[] | Pointer<any>, onUpdate: StateUpdateFunction, rewritable: boolean = false): NodeState {
        if (pointers instanceof Pointer) {
            return new NodeState([pointers], onUpdate, rewritable);
        }
        
        return new NodeState(pointers, onUpdate, rewritable);
    }
    
    value(key: string, value: any): NodeValue {
        return new NodeValue(key, value);
    }
    
    stateValue(pointer: Pointer<any> | Pointer[]): NodeState {
        if (pointer instanceof Pointer) {
            pointer = [pointer];
        }
        
        return this.state(pointer, update => update(pointer[0].value));
    }
    
    stateMount<DataType = any>(
        pointers: Pointer[] | Pointer, 
        onUpdate: () => boolean, 
        elementReference: () => (new() => UI), 
        data: VoidFunction, 
        name: string = ""
    ): NodeComment {
        return new NodeComment(
            document.createComment(""),
            pointers instanceof Pointer ? [pointers] : pointers,
            onUpdate,
            elementReference,
            data,
            name
        );
    }
    
    stateSwitch<DataType>(
        pointers: Pointer<DataType>[] | Pointer, 
        onUpdate: () => any, 
        data: VoidFunction = () => null, 
        name: string = "",
        refreshable: boolean = false
    ): NodeSwitch {
        return new NodeSwitch(
            document.createComment(""),
            pointers instanceof Pointer ? [pointers] : pointers,
            onUpdate,
            onUpdate,
            data,
            name,
            refreshable
        );
    }
    
    statePosition<DataType>(pointers: Pointer<DataType>[], elementName: string, condition: () => boolean, name: string = ""): NodePosition {
        return new NodePosition(
            document.createComment(""),
            pointers,
            elementName,
            condition,
            name
        );
    }
    
    list(name: string, listNode: NodeUI, listID: string, itemClassReference: new() => UI<any>): UI {
        return this.element<ListUIData>(ListUI, {
            listNode: listNode,
            listID: listID,
            itemClassReference: itemClassReference
        }, name);
    }
    
    public removeItem(listID: string, itemID: string): void {
        const list = new ListPointer("remove", listID);
        list.remove(itemID);
        list.unmount();
    }
    
    public keyDownUpdate(update = () => {}): void {
        setTimeout(update, 0);
    }
    
    // Some TypeScript comforting
    public getNode<T extends HTMLElement>(name: string): T {
        return this.nodes.get(name).node as T;
    }
    
    // Some TypeScript comforting
    public getInput(name: string): HTMLInputElement {
        return this.getNode<HTMLInputElement>(name);
    }
    
    public getDisplay(): NodeUI {
        return this.display();
    }
    
    public externalMounted(): void {
        this.mounted();
    }
    
    public externalBeforeMount(): void {
        this.beforeMount();
    }
    
    public externalUnmount(unmount: VoidFunction): void {
        this.onUnmount(unmount);
    }
    
    public externalUnmountSync(): void {
        this.onUnmountSync();
    }
    
    /**
     * @abstract Method is abstract, implemented in extensions of UI class
     * @returns {NodeUI}
     * */
    protected display(): NodeUI {
        return this.div({}, []);
    }
    
    /**
     * @abstract Method is abstract, implemented in extensions of UI class
     * */
    protected mounted() {}
    
    /**
     * @abstract Method is abstract, implemented in extensions of UI class
     * */
    protected beforeMount() {}
    
    /**
     * @abstract Method is abstract, implemented in extensions of UI class
     * */
    protected onUnmount(unmount = () => {}) {
        unmount();
    }
}
