'use strict';

class InteractiveLibrary extends Ground {
    public interactive: InteractiveUI;
    public enabled = false;
    
    constructor(interactive: InteractiveUI) {
        super();
        
        this.interactive = interactive;
    }
    
    // /** @abstract Abstracted in direct inheritance */
    // enable() {}
    // /** @abstract Abstracted in direct inheritance */
    // disable() {}
    // /** @abstract Abstracted in direct inheritance */
    // unmount() {}
}

class InteractiveUI extends Ground {
    public ui: UI;
    public move: Move;
    public drag: Drag;
    public drop: Drop;
    public resize: Resize;
    public rotate: Rotate;
    public skew: Skew;
    public sort: Sort;
    public padding: Padding;
    
    constructor(ui: UI) {
        super();
        
        this.ui = ui;
        this.move = new Move(this);
        this.drag = new Drag(this);
        this.drop = new Drop(this);
        this.resize = new Resize(this);
        this.rotate = new Rotate(this);
        this.skew = new Skew(this);
        this.sort = new Sort(this);
        this.padding = new Padding(this);
    }
}
