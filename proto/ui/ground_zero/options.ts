'use strict';

declare type MouseEventFunction = (ev?: MouseEvent) => void;

class OptionsUI {
    public name?: string;
    public text?: string;
    public html?: string;
    public style?: string;
    public width?: string;
    public height?: string;
    public className?: string;
    public stateText?: NodeState | null;
    public stateStyle?: NodeState | null;
    public stateClass?: NodeState | null;
    public type?: string;
    public value?: string;
    public placeholder?: string;
    public stateValue?: NodeState;
    public onClick?: MouseEventFunction | null;
    public onChange?: MouseEventFunction | null;
    public onFocus?: MouseEventFunction | null;
    public onBlur?: MouseEventFunction | null;
    public onDoubleClick?: MouseEventFunction | null;
    public onMouseDown?: MouseEventFunction | null;
    public onMouseUp?: MouseEventFunction | null;
    public onMouseMove?: MouseEventFunction | null;
    public onMouseEnter?: MouseEventFunction | null;
    public onMouseLeave?: MouseEventFunction | null;
    public onMouseOver?: MouseEventFunction | null;
    public onMouseOut?: MouseEventFunction | null;
    public onRightClick?: MouseEventFunction | null;
    public onKeyUp?: MouseEventFunction | null;
    public onKeyDown?: MouseEventFunction | null;
    public onScroll?: MouseEventFunction | null;
    public onMouseWheel?: MouseEventFunction | null;
    public onSubmit?: MouseEventFunction | null;
    public onResize?: MouseEventFunction | null;
    public _onSubmit?: MouseEventFunction | null;
    public attributes?: ({name: string, value: string})[];
    
    constructor(data: Partial<OptionsUI>) {
        this.name = typeof data.name === "undefined" ? "" : data.name;
        this.text = typeof data.text === "undefined" ? "" : data.text;
        this.html = typeof data.html === "undefined" ? "" : data.html;
        this.style = typeof data.style === "undefined" ? "" : data.style;
        this.width = typeof data.width === "undefined" ? "" : data.width;
        this.height = typeof data.height === "undefined" ? "" : data.height;
        this.className = typeof data.className === "undefined" ? "" : data.className;
        this.stateText = typeof data.stateText === "undefined" ? null : data.stateText;
        this.stateStyle = typeof data.stateStyle === "undefined" ? null : data.stateStyle;
        this.stateClass = typeof data.stateClass === "undefined" ? null : data.stateClass;
        this.type = typeof data.type === "undefined" ? "" : data.type;
        this.value = typeof data.value === "undefined" ? "" : data.value;
        this.placeholder = typeof data.placeholder === "undefined" ? "" : data.placeholder;
        this.stateValue = typeof data.stateValue === "undefined" ? null : data.stateValue;
        this.onClick = typeof data.onClick === "undefined" ? null : data.onClick;
        this.onChange = typeof data.onChange === "undefined" ? null : data.onChange;
        this.onFocus = typeof data.onFocus === "undefined" ? null : data.onFocus;
        this.onBlur = typeof data.onBlur === "undefined" ? null : data.onBlur;
        this.onDoubleClick = typeof data.onDoubleClick === "undefined" ? null : data.onDoubleClick;
        this.onMouseDown = typeof data.onMouseDown === "undefined" ? null : data.onMouseDown;
        this.onMouseUp = typeof data.onMouseUp === "undefined" ? null : data.onMouseUp;
        this.onMouseMove = typeof data.onMouseMove === "undefined" ? null : data.onMouseMove;
        this.onMouseEnter = typeof data.onMouseEnter === "undefined" ? null : data.onMouseEnter;
        this.onMouseLeave = typeof data.onMouseLeave === "undefined" ? null : data.onMouseLeave;
        this.onMouseOver = typeof data.onMouseOver === "undefined" ? null : data.onMouseOver;
        this.onMouseOut = typeof data.onMouseOut === "undefined" ? null : data.onMouseOut;
        this.onRightClick = typeof data.onRightClick === "undefined" ? null : data.onRightClick;
        this.onKeyUp = typeof data.onKeyUp === "undefined" ? null : data.onKeyUp;
        this.onKeyDown = typeof data.onKeyDown === "undefined" ? null : data.onKeyDown;
        this.onScroll = typeof data.onScroll === "undefined" ? null : data.onScroll;
        this.onMouseWheel = typeof data.onMouseWheel === "undefined" ? null : data.onMouseWheel;
        this.onSubmit = typeof data.onSubmit === "undefined" ? null : data.onSubmit;
        this.onResize = typeof data.onResize === "undefined" ? null : data.onResize;
        this._onSubmit = typeof data._onSubmit === "undefined" ? null : data._onSubmit;
        this.attributes = typeof data.attributes === "undefined" ? [] : data.attributes;
    }
}
