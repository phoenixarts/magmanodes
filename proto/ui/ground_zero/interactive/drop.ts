/**
 * Features to be applied
 * ~1. Can drop item's data
 * 2. Can be combined with sort to automatically adjust where to appear the new item
 * 3. When combined with sort to have oninsert method which handles the data insertion and it's processing
 * 
*/

class Drop extends InteractiveLibrary {
    public _accept: Array<typeof Struct> = [];
    public _onDropBinded: VoidFunction;
    public _onDragEnterBinded: VoidFunction;
    public _onDragLeaveBinded: VoidFunction;
    public sortElementName: string;
    public combineWithSort: boolean = false;
    public sortedInsert: boolean = false;
    
    public node: NodeUI;
    public onDrop: (data?: any, ev?: MouseEvent) => void;
    public onDragEnter: (data?: any) => void;
    public onDragLeave: (data?: any) => void;
    public onInsert: (data?: any) => [Struct, string?] | boolean;
    
    accept(/** @type {Array<<Struct>>} */ structSet = []) {
        this._accept = structSet;
    }
    
    _onDragEnter() {
        if (View.dragging.value === false) {
            return;
        }
        
        if (this.validDrag() === false) {
            return;
        }
        
        if (this.sortedInsert) {
            // Sorting gets started here
        }
        
        if (this.onDragEnter) {
            this.onDragEnter(View.dragElement.value.options);
        }
    }
    
    _onDragLeave() {
        if (View.dragging.value === false) {
            return;
        }
        
        if (this.validDrag() === false) {
            return;
        }
        
        if (this.sortedInsert) {
            // Sorting gets ended without insert here
        }
        
        if (this.onDragLeave) {
            this.onDragLeave(View.dragElement.value.options);
        }
    }
    
    _onDrop(ev: MouseEvent) {
        if (View.dragging.value === false) {
            return;
        }
        
        if (this.validDrag() === false) {
            return;
        }
        
        if (this.sortedInsert) {
            // Sorting gets ended with insert here
            const newItem = this.onInsert(View.dragElement.value.options.data);
            const ui = this.interactive.ui.elements.get(this.sortElementName);
            
            if (ui instanceof ListUI) {
                const pointer = ui.listPointer;
                
                if (newItem !== false && newItem !== true) {
                    if (newItem.length === 1) {
                        newItem[1] = "id_" + performance.now();
                    }
                    
                    pointer.add(newItem[0], newItem[1]);
                }
            }
        }
        
        if (this.onDrop) {
            this.onDrop(View.dragElement.value.options, ev);
        }
    }
    
    validDrag() {
        let found = false;
        const className = View.dragElement.value.options.constructor.name;
        
        for (let i = 0; i < this._accept.length; i++) {
            if (this._accept[i].name === className) {
                found = true;
                
                break;
            }
        }
        
        if (found === false) {
            return false;
        }
        
        return true;
    }
    
    enable(nodeName = "", structSet: Array<typeof Struct>, onDrop: (data?: any) => void, sortElementName = "", sortedInsert = false) {
        if (this.enabled) {
            return;
        }
        
        this.enabled = true;
        
        this.node = this.interactive.ui.nodes.get(nodeName);
        this.onDrop = onDrop;
        this.sortElementName = sortElementName;
        this.combineWithSort = this.sortElementName.length === 0 ? false : true;
        this.sortedInsert = sortedInsert;
        
        this.accept(structSet);
        
        this._onDropBinded = this._onDrop.bind(this);
        this._onDragEnterBinded = this._onDragEnter.bind(this);
        this._onDragLeaveBinded = this._onDragLeave.bind(this);
        
        this.node.node.addEventListener("mouseup", this._onDropBinded, false);
        this.node.node.addEventListener("mouseenter", this._onDragEnterBinded, false);
        this.node.node.addEventListener("mouseleave", this._onDragLeaveBinded, false);
    }
    
    disable() {
        if (this.enabled === false) {
            return;
        }
        
        this.node.node.removeEventListener("mouseenter", this._onDragEnterBinded);
        this.node.node.removeEventListener("mouseleave", this._onDragLeaveBinded);
        this.node.node.removeEventListener("mouseup", this._onDropBinded);
        
        this.node = null;
        
        this.enabled = false;
    }
    
    unmount() {
        this.disable();
    }
}
