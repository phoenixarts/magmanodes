class Drag extends InteractiveLibrary {
    public placeholder = null;
    public placeholderColor = "transparent";
    public floating = "none";
    
    public onBeforeStart: VoidFunction;
    public onStart: VoidFunction;
    public onEnd: VoidFunction;
    public delay: number = 1;
    public startAttempted: boolean = false;
    public startPos: Pos = new Pos();
    public nodeName: string;
    
    private _onDragStartBinded = null;
    
    public windowDelayMove: ((ev2: MouseEvent) => void) | null;
    public windowDelayUp = () => {
        this.startPos.x = 0;
        this.startPos.y = 0;
        this.startAttempted = false;
        
        window.removeEventListener("mousemove", this.windowDelayMove);
        window.removeEventListener("mouseup", this.windowDelayUp);
        
        this.windowDelayMove = null;
    };
    
    _onDragStart(nodeName: string, ev: MouseEvent) {
        if (ev.button !== 0) {
            return;
        }
        
        if (this.startAttempted === false) {
            this.startPos.x = ev.pageX;
            this.startPos.y = ev.pageY;
            this.startAttempted = true;
            
            this.windowDelayMove = (ev2: MouseEvent) => {
                if (
                    Math.abs(this.startPos.x - ev2.pageX) > 1 ||
                    Math.abs(this.startPos.y - ev2.pageY) > 1
                ) {
                    this.windowDelayUp();
                    
                    this.interactive.move.paused = false;
                    
                    this.interactive.move.onStart = this._onStart.bind(this);
                    this.interactive.move.onEnd = this._onEnd.bind(this);
                    this.interactive.move.enable(View.dragPos, nodeName);
                    
                    this.interactive.move._down(this.interactive.ui.node, View.dragPos, nodeName, ev2);
                }
            };
            
            window.addEventListener("mousemove", this.windowDelayMove, false);
            window.addEventListener("mouseup", this.windowDelayUp, false);
        }
        
        ev.preventDefault();
        ev.stopPropagation();
        ev.stopImmediatePropagation();
    }
    
    _onDragMove() {
        
    }
    
    _onStart() {
        if (this.onBeforeStart) {
            this.onBeforeStart();
        }
        
        this.mountPlaceholder();
    }
    
    protected mountPlaceholder(): void {
        this.interactive.ui.node.node.style.pointerEvents = "none";
        
        const rect = this.interactive.ui.node.node.getBoundingClientRect();
        
        this.placeholder = document.createElement("div");
        
        let margin = this.interactive.ui.node.node.style.margin;
        
        if (margin === "") {
            margin = window.getComputedStyle(this.interactive.ui.node.node).margin;
        }
        
        if (margin === "") {
            margin = "0px";
        }
        
        const marginInt = margin === "" ? 0 : parseInt(margin.replace("px", "").replace("%", ""));
        
        if (this.floating === "left") {
            this.placeholder.style.float = "left";
        }
        
        if (this.floating === "right") {
            this.placeholder.style.float = "right";
        }
        
        if (this.floating === "inline-block") {
            this.placeholder.style.display = "inline-block";
        }
        
        this.placeholder.style.margin = margin;
        this.placeholder.style.backgroundColor = this.placeholderColor;
        this.placeholder.style.borderRadius = this.interactive.ui.node.node.style.borderRadius;
        this.placeholder.style.width = rect.width + "px";
        this.placeholder.style.height = rect.height + "px";
        
        if (this.placeholder.style.borderRadius === null || this.placeholder.style.borderRadius === "") {
            this.placeholder.style.borderRadius = window.getComputedStyle(this.interactive.ui.node.node).borderRadius;
        }
        
        this.interactive.ui.node.node.style.position = "absolute";
        this.interactive.ui.node.node.style.left = (rect.left - marginInt) + "px";
        this.interactive.ui.node.node.style.top = (rect.top - marginInt) + "px";
        
        this.interactive.ui.node.node.after(this.placeholder);
        document.body.appendChild(this.interactive.ui.node.node);
        
        View.dragging.value = true;
        View.dragElement.value = this.interactive.ui;
        View.dragPlaceholder.value = this.placeholder;
        
        if (this.onStart) {
            this.onStart();
        }
    }
    
    _onEnd() {
        this.startAttempted = false;
        this.interactive.move.paused = true;
        
        View.dragging.value = false;
        View.dragElement.value = null;
        View.dragPlaceholder.value = null;
        
        this.interactive.ui.node.node.style.position = null;
        this.interactive.ui.node.node.style.left = null;
        this.interactive.ui.node.node.style.top = null;
        
        this.placeholder.after(this.interactive.ui.node.node);
        this.placeholder.parentNode.removeChild(this.placeholder);
        this.placeholder = null;
        
        this.interactive.move.mount.pointer.value.x = 0;
        this.interactive.move.mount.pointer.value.y = 0;
        this.interactive.move.mount.pointer.update();
        
        this.interactive.move.unmount();
        
        if (this.onEnd) {
            this.onEnd();
        }
        
        this.interactive.ui.node.node.style.pointerEvents = null;
    }
    
    enable(nodeName = "main", placeholderColor = "transparent", floating = "none") {
        if (this.enabled) {
            return;
        }
        
        this.nodeName = nodeName;
        
        this.enabled = true;
        
        this.placeholderColor = placeholderColor;
        this.floating = floating;
        
        this._onDragStartBinded = this._onDragStart.bind(this, this.nodeName);
        
        this.interactive.ui.nodes.get(this.nodeName).node.addEventListener("mousedown", this._onDragStartBinded, false);
    }
    
    disable() {
        this.interactive.ui.nodes.get(this.nodeName).node.removeEventListener("mousedown", this._onDragStartBinded);
        
        this.interactive.move.unmount();
        
        this.enabled = false;
    }
    
    unmount() {
        if (this.enabled === false) {
            return;
        }
        
        this.disable();
    }
}
