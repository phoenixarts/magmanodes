'use strict';

declare type InteractiveMoveEventFunction = (node: NodeUI, pointer: Pointer<Pos>, name: string) => void;

class MoveMount {
    public id: string;
    public node: NodeUI;
    public pointer: Pointer<Pos>;
    public down: (ev?: MouseEvent) => void;
    public move: (ev?: MouseEvent) => void;
    public up: (ev?: MouseEvent) => void;
    
    constructor(
        id: string,
        node: NodeUI,
        pointer: Pointer<Pos>,
        down?: (ev?: MouseEvent) => void,
        move?: (ev?: MouseEvent) => void,
        up?: (ev?: MouseEvent) => void
    ) {
        this.id = id;
        this.node = node;
        this.pointer = pointer;
        
        if (down) {
            this.down = down;
        }
        
        if (move) {
            this.move = move;
        }
        
        if (up) {
            this.up = up;
        }
        
        this.mount();
    }
    
    mount() {
        this.node.node.addEventListener("mousedown", this.down, false);
    }
    
    unmount() {
        this.node.node.removeEventListener("mousedown", this.down);
    }
}

declare type MoveCustomEvent = (node?: NodeUI, pointer?: Pointer<Pos>, id?: string, ev?: MouseEvent) => void;

let moving: boolean = false;

class Move extends InteractiveLibrary {
    public nodeName: string;
    public mount: MoveMount;
    public startPos = new Pos();
    public startTransform = new Pos();
    public muted = false;
    public paused = false;
    public onStart: MoveCustomEvent;
    public onMove: MoveCustomEvent;
    public onEnd: MoveCustomEvent;
    
    _down(node: NodeUI, pointer: Pointer<Pos>, id: string, ev: MouseEvent) {
        if (ev.button !== 0) {
            return;
        }
        
        if (this.paused === true) {
            return;
        }
        
        moving = true;
        
        document.body.style.userSelect = "none";
        
        this.startPos.x = ev.pageX;
        this.startPos.y = ev.pageY;
        this.startTransform.x = node.transform.matrix[0][0];
        this.startTransform.y = node.transform.matrix[0][1];
        
        window.addEventListener("mousemove", this.mount.move, false);
        window.addEventListener("mouseup", this.mount.up, false);
        
        if (this.onStart) {
            this.onStart(node, pointer, id, ev);
        }
    }
    
    _move(node: NodeUI, pointer: Pointer<Pos>, id: string, ev: MouseEvent) {
        if (this.muted === false) {
            pointer.value.x = Math.round(ev.pageX - (this.startPos.x - this.startTransform.x));
            pointer.value.y = Math.round(ev.pageY - (this.startPos.y - this.startTransform.y));
            
            pointer.value = pointer.value;
        }
        
        if (this.onMove) {
            this.onMove(node, pointer, id, ev);
        }
    }
    
    _up(node: NodeUI, pointer: Pointer<Pos>, id: string, ev: MouseEvent) {
        if (this.mount === null) {
            // Cancelled beforehand
            return;
        }
        
        window.removeEventListener("mousemove", this.mount.move);
        window.removeEventListener("mouseup", this.mount.up);
        
        moving = false;
        
        document.body.style.userSelect = null;
        
        if (this.onEnd) {
            this.onEnd(node, pointer, id, ev);
        }
    }
    
    _pointerUpdate(pointer: Pointer<Pos>, node: NodeUI) {
        node.transform.translate(pointer.value.x, pointer.value.y);
    }
    
    enable(pointer: Pointer<Pos>, nodeName: string = "main") {
        if (this.enabled) {
            return;
        }
        
        this.nodeName = nodeName;
        
        const node = this.interactive.ui.nodes.get(nodeName);
        
        if (pointer.listID.length !== 0) {
            this.interactive.ui.node.transform.translate(pointer.value.x, pointer.value.y, pointer.value.z);
        }
        
        this.mount = new MoveMount(
            nodeName, 
            node,
            pointer, 
            (ev?: MouseEvent) => this._down(this.interactive.ui.node, pointer, nodeName, ev),
            (ev?: MouseEvent) => this._move(this.interactive.ui.node, pointer, nodeName, ev),
            (ev?: MouseEvent) => this._up(this.interactive.ui.node, pointer, nodeName, ev)
        );
        
        this.interactive.ui.propPointers.set(
            nodeName + "_move", [
                new Pointer(
                    pointer.listID, 
                    pointer.itemID, 
                    pointer.prop, 
                    this._pointerUpdate.bind(
                        this, 
                        pointer, 
                        this.interactive.ui.node
                    )
                )
            ]
        );
        
        this.enabled = true;
    }
    
    disable() {
        this.interactive.ui.propPointers.get(this.nodeName + "_move")[0].unmount();
        this.interactive.ui.propPointers.delete(this.nodeName + "_move");
        
        this.enabled = false;
        
        window.removeEventListener("mousemove", this.mount.move);
        window.removeEventListener("mouseup", this.mount.up);
        
        this.mount.unmount();
        this.mount = null;
    }
    
    unmount() {
        if (this.enabled === false) {
            return;
        }
        
        this.disable();
    }
}
