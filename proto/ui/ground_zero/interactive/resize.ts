'use strict';

class Resize extends InteractiveLibrary {
    public left: boolean = true;
    public top: boolean = true;
    public right: boolean = true;
    public bottom: boolean = true;
    public topLeft: boolean = true;
    public topRight: boolean = true;
    public bottomLeft: boolean = true;
    public bottomRight: boolean = true;
    public movement: boolean = true;
    public noMovement: boolean = false;
    public percentOffsetCorrection: boolean = false;
    public onResizeStart: VoidFunction = () => {};
    public onResizeMove: VoidFunction = () => {};
    public onResizeEnd: VoidFunction = () => {};
    
    _pointerUpdate(sizePointer: Pointer<Size>, positionPointer: Pointer<Pos>, node: NodeUI, manual: boolean = false) {
        if ((this.interactive.ui.interactive.move.enabled === false || manual === true) && this.noMovement === false) {
            node.transform.translate(positionPointer.value.x, positionPointer.value.y);
        }
        
        if (sizePointer.value.percentX) {
            node.transform.sizeX(sizePointer.value.offsetX, 1, sizePointer.value.x, false);
        } else {
            node.transform.sizeX(sizePointer.value.x, 0, 0, false);
        }
        
        if (sizePointer.value.percentY) {
            node.transform.sizeY(sizePointer.value.offsetY, 1, sizePointer.value.y, false);
        } else {
            node.transform.sizeY(sizePointer.value.y, 0, 0, false);
        }
        
        node.transform.applySize();
    }
    
    enable(sizePointer: Pointer<Size>, posPointer: Pointer<Pos>) {
        const sizeFunc = this._pointerUpdate.bind(
            this, 
            sizePointer, 
            posPointer, 
            this.interactive.ui.node
        )
        
        const posFunc = this._pointerUpdate.bind(
            this, 
            sizePointer, 
            posPointer, 
            this.interactive.ui.node
        )
        
        this.interactive.ui.propPointers.set(
            "resize_size", [
                new Pointer(
                    sizePointer.listID, 
                    sizePointer.itemID, 
                    sizePointer.prop, 
                    sizeFunc
                )
            ]
        );
        
        this.interactive.ui.propPointers.set(
            "resize_pos", [
                new Pointer(
                    posPointer.listID, 
                    posPointer.itemID, 
                    posPointer.prop, 
                    posFunc
                )
            ]
        );
        
        this.interactive.ui.addElement("main", ResizerLeft, {
            sizePointer: sizePointer,
            posPointer: posPointer,
            active: this.left,
            movement: this.movement,
            start: this.onResizeStart,
            move: this.onResizeMove,
            end: this.onResizeEnd
        });
        this.interactive.ui.addElement("main", ResizerTop, {
            sizePointer: sizePointer,
            posPointer: posPointer,
            active: this.top,
            movement: this.movement,
            start: this.onResizeStart,
            move: this.onResizeMove,
            end: this.onResizeEnd
        });
        this.interactive.ui.addElement("main", ResizerRight, {
            sizePointer: sizePointer,
            posPointer: posPointer,
            active: this.right,
            movement: this.movement,
            start: this.onResizeStart,
            move: this.onResizeMove,
            end: this.onResizeEnd
        });
        this.interactive.ui.addElement("main", ResizerBottom, {
            sizePointer: sizePointer,
            posPointer: posPointer,
            active: this.bottom,
            movement: this.movement,
            start: this.onResizeStart,
            move: this.onResizeMove,
            end: this.onResizeEnd
        });
        this.interactive.ui.addElement("main", ResizerTopLeft, {
            sizePointer: sizePointer,
            posPointer: posPointer,
            active: this.topLeft,
            movement: this.movement,
            start: this.onResizeStart,
            move: this.onResizeMove,
            end: this.onResizeEnd
        });
        this.interactive.ui.addElement("main", ResizerTopRight, {
            sizePointer: sizePointer,
            posPointer: posPointer,
            active: this.topRight,
            movement: this.movement,
            start: this.onResizeStart,
            move: this.onResizeMove,
            end: this.onResizeEnd
        });
        this.interactive.ui.addElement("main", ResizerBottomRight, {
            sizePointer: sizePointer,
            posPointer: posPointer,
            active: this.bottomRight,
            movement: this.movement,
            start: this.onResizeStart,
            move: this.onResizeMove,
            end: this.onResizeEnd
        });
        this.interactive.ui.addElement("main", ResizerBottomLeft, {
            sizePointer: sizePointer,
            posPointer: posPointer,
            active: this.bottomLeft,
            movement: this.movement,
            start: this.onResizeStart,
            move: this.onResizeMove,
            end: this.onResizeEnd
        });
        
        sizeFunc(true);
        posFunc(true);
        
        this.enabled = true;
    }
    
    disable() {
        this.enabled = false;
        
        this.interactive.ui.propPointers.get("resize_size")[0].unmount();
        this.interactive.ui.propPointers.delete("resize_size");
        
        this.interactive.ui.propPointers.get("resize_pos")[0].unmount();
        this.interactive.ui.propPointers.delete("resize_pos");
        
        this.interactive.ui.elements.get("ResizerLeft").unmount();
        this.interactive.ui.elements.get("ResizerTop").unmount();
        this.interactive.ui.elements.get("ResizerRight").unmount();
        this.interactive.ui.elements.get("ResizerBottom").unmount();
        this.interactive.ui.elements.get("ResizerTopLeft").unmount();
        this.interactive.ui.elements.get("ResizerTopRight").unmount();
        this.interactive.ui.elements.get("ResizerBottomRight").unmount();
        this.interactive.ui.elements.get("ResizerBottomLeft").unmount();
    }
    
    unmount() {
        if (this.enabled === false) {
            return;
        }
        
        this.disable();
    }
}

interface ResizerOptions {
    sizePointer: Pointer<Size>;
    posPointer: Pointer<Pos>;
    active: boolean;
    movement: boolean;
    start: VoidFunction;
    move: VoidFunction;
    end: VoidFunction;
}

class Resizer extends UI<ResizerOptions> {
    public sizePointer: Pointer<Size>;
    public posPointer: Pointer<Pos>;
    
    public startPos = new Pos();
    public startStatePos = new Pos();
    public startSize = new Size();
    public startStateSize = new Size();
    
    beforeMount() {
        this.sizePointer = this.options.sizePointer;
        this.posPointer = this.options.posPointer;
    }
    
    mounted() {
        this.interactive.move.muted = true;
    }
    
    display() {
        if (this.options.active === false) {
            return this.div({
                style: "display: none;"
            });
        }
        
        return this.div();
    }
}

class ResizerLeft extends Resizer {
    mounted() {
        super.mounted();
        
        if (this.options.movement === false) {
            return;
        }
        
        this.interactive.move.enable(new Pointer<Pos>(), "main");
        
        this.interactive.move.onStart = this.onStart.bind(this);
        this.interactive.move.onMove = this.onResize.bind(this);
        this.interactive.move.onEnd = this.onEnd.bind(this);
    }
    
    onStart(node: NodeUI, pointer: Pointer<Pos>, id: string, ev: MouseEvent, applyResizer: boolean = true) {
        if (applyResizer) {
            document.body.style.cursor = "ew-resize !important";
        }
        
        this.startPos.x = ev.pageX;
        this.startPos.y = ev.pageY;
        
        this.startStatePos.x = this.posPointer.value.x;
        this.startStateSize.x = this.sizePointer.value.x;
        this.options.start();
    }
    
    onResize(node: NodeUI, pointer: Pointer<Pos>, id: string, ev: MouseEvent) {
        this.posPointer.value.x = this.startStatePos.x + (ev.pageX - this.startPos.x);
        this.sizePointer.value.x = this.startStateSize.x - (ev.pageX - this.startPos.x);
        
        this.sizePointer.update();
        this.posPointer.update();
        this.options.move();
    }
    
    onEnd(node: NodeUI, pointer: Pointer<Pos>, id: string, ev: MouseEvent) {
        document.body.style.cursor = null;
        this.options.end();
    }
}

class ResizerTop extends Resizer {
    mounted() {
        super.mounted();
        
        if (this.options.movement === false) {
            return;
        }
        
        this.interactive.move.enable(new Pointer<Pos>(), "main");
        
        this.interactive.move.onStart = this.onStart.bind(this);
        this.interactive.move.onMove = this.onResize.bind(this);
        this.interactive.move.onEnd = this.onEnd.bind(this);
    }
    
    onStart(node: NodeUI, pointer: Pointer<Pos>, id: string, ev: MouseEvent, applyResizer: boolean = true) {
        if (applyResizer) {
            document.body.style.cursor = "ns-resize !important";
        }
        
        this.startPos.x = ev.pageX;
        this.startPos.y = ev.pageY;
        
        this.startStatePos.y = this.posPointer.value.y;
        
        this.startStateSize.y = this.sizePointer.value.y;
        this.options.start();
    }
    
    onResize(node: NodeUI, pointer: Pointer<Pos>, id: string, ev: MouseEvent) {
        this.posPointer.value.y = this.startStatePos.y + (ev.pageY - this.startPos.y);
        this.sizePointer.value.y = this.startStateSize.y - (ev.pageY - this.startPos.y);
        
        this.sizePointer.update();
        this.posPointer.update();
        this.options.move();
    }
    
    onEnd(node: NodeUI, pointer: Pointer<Pos>, id: string, ev: MouseEvent) {
        document.body.style.cursor = null;
        this.options.end();
    }
}

class ResizerRight extends Resizer {
    mounted() {
        super.mounted();
        
        if (this.options.movement === false) {
            return;
        }
        
        this.interactive.move.enable(new Pointer<Pos>(), "main");
        
        this.interactive.move.onStart = this.onStart.bind(this);
        this.interactive.move.onMove = this.onResize.bind(this);
        this.interactive.move.onEnd = this.onEnd.bind(this);
    }
    
    onStart(node: NodeUI, pointer: Pointer<Pos>, id: string, ev: MouseEvent, applyResizer: boolean = true) {
        if (applyResizer) {
            document.body.style.cursor = "ew-resize !important";
        }
        
        this.startPos.x = ev.pageX;
        this.startPos.y = ev.pageY;
        
        this.startStateSize.x = this.sizePointer.value.x;
        this.options.start();
    }
    
    onResize(node: NodeUI, pointer: Pointer<Pos>, id: string, ev: MouseEvent) {
        this.sizePointer.value.x = this.startStateSize.x + (ev.pageX - this.startPos.x);
        
        this.sizePointer.update();
        this.options.move();
    }
    
    onEnd(node: NodeUI, pointer: Pointer<Pos>, id: string, ev: MouseEvent) {
        document.body.style.cursor = null;
        this.options.end();
    }
}

class ResizerBottom extends Resizer {
    mounted() {
        super.mounted();
        
        if (this.options.movement === false) {
            return;
        }
        
        this.interactive.move.enable(new Pointer<Pos>(), "main");
        
        this.interactive.move.onStart = this.onStart.bind(this);
        this.interactive.move.onMove = this.onResize.bind(this);
        this.interactive.move.onEnd = this.onEnd.bind(this);
    }
    
    onStart(node: NodeUI, pointer: Pointer<Pos>, id: string, ev: MouseEvent, applyResizer: boolean = true) {
        if (applyResizer) {
            document.body.style.cursor = "ns-resize !important";
        }
        
        this.startPos.x = ev.pageX;
        this.startPos.y = ev.pageY;
        
        this.startStateSize.y = this.sizePointer.value.y;
        this.options.start();
    }
    
    onResize(node: NodeUI, pointer: Pointer<Pos>, id: string, ev: MouseEvent) {
        this.sizePointer.value.y = this.startStateSize.y + (ev.pageY - this.startPos.y);
        
        this.sizePointer.update();
        this.options.move();
    }
    
    onEnd(node: NodeUI, pointer: Pointer<Pos>, id: string, ev: MouseEvent) {
        document.body.style.cursor = null;
        this.options.end();
    }
}

class ResizerTopLeft extends Resizer {
    mounted() {
        super.mounted();
        
        if (this.options.movement === false) {
            return;
        }
        
        this.interactive.move.enable(new Pointer<Pos>(), "main");
        
        this.interactive.move.onStart = this.onStart.bind(this);
        this.interactive.move.onMove = this.onResize.bind(this);
        this.interactive.move.onEnd = this.onEnd.bind(this);
    }
    
    onStart(node: NodeUI, pointer: Pointer<Pos>, id: string, ev: MouseEvent) {
        document.body.style.cursor = "nwse-resize !important";
        
        const resizerLeft = this.parent.elements.get("ResizerLeft");
        const resizerTop = this.parent.elements.get("ResizerTop");
        
        if (resizerLeft instanceof ResizerLeft) {
            resizerLeft.onStart(node, pointer, id, ev, false);
        }
        
        if (resizerTop instanceof ResizerTop) {
            resizerTop.onStart(node, pointer, id, ev, false);
        }
    }
    
    onResize(node: NodeUI, pointer: Pointer<Pos>, id: string, ev: MouseEvent) {
        const resizerLeft = this.parent.elements.get("ResizerLeft");
        const resizerTop = this.parent.elements.get("ResizerTop");
        
        if (resizerLeft instanceof ResizerLeft) {
            resizerLeft.onResize(node, pointer, id, ev);
        }
        
        if (resizerTop instanceof ResizerTop) {
            resizerTop.onResize(node, pointer, id, ev);
        }
    }
    
    onEnd(node: NodeUI, pointer: Pointer<Pos>, id: string, ev: MouseEvent) {
        document.body.style.cursor = null;
    }
}

class ResizerTopRight extends Resizer {
    mounted() {
        super.mounted();
        
        if (this.options.movement === false) {
            return;
        }
        
        this.interactive.move.enable(new Pointer<Pos>(), "main");
        
        this.interactive.move.onStart = this.onStart.bind(this);
        this.interactive.move.onMove = this.onResize.bind(this);
        this.interactive.move.onEnd = this.onEnd.bind(this);
    }
    
    onStart(node: NodeUI, pointer: Pointer<Pos>, id: string, ev: MouseEvent) {
        document.body.style.cursor = "nesw-resize !important";
        
        const resizerRight = this.parent.elements.get("ResizerRight");
        const resizerTop = this.parent.elements.get("ResizerTop");
        
        if (resizerRight instanceof ResizerRight) {
            resizerRight.onStart(node, pointer, id, ev, false);
        }
        
        if (resizerTop instanceof ResizerTop) {
            resizerTop.onStart(node, pointer, id, ev, false);
        }
    }
    
    onResize(node: NodeUI, pointer: Pointer<Pos>, id: string, ev: MouseEvent) {
        const resizerRight = this.parent.elements.get("ResizerRight");
        const resizerTop = this.parent.elements.get("ResizerTop");
        
        if (resizerRight instanceof ResizerRight) {
            resizerRight.onResize(node, pointer, id, ev);
        }
        
        if (resizerTop instanceof ResizerTop) {
            resizerTop.onResize(node, pointer, id, ev);
        }
    }
    
    onEnd(node: NodeUI, pointer: Pointer<Pos>, id: string, ev: MouseEvent) {
        document.body.style.cursor = null;
    }
}

class ResizerBottomRight extends Resizer {
    mounted() {
        super.mounted();
        
        if (this.options.movement === false) {
            return;
        }
        
        this.interactive.move.enable(new Pointer<Pos>(), "main");
        
        this.interactive.move.onStart = this.onStart.bind(this);
        this.interactive.move.onMove = this.onResize.bind(this);
        this.interactive.move.onEnd = this.onEnd.bind(this);
    }
    
    onStart(node: NodeUI, pointer: Pointer<Pos>, id: string, ev: MouseEvent) {
        document.body.style.cursor = "nesw-resize !important";
        
        const resizerRight = this.parent.elements.get("ResizerRight");
        const resizerBottom = this.parent.elements.get("ResizerBottom");
        
        if (resizerRight instanceof ResizerRight) {
            resizerRight.onStart(node, pointer, id, ev, false);
        }
        
        if (resizerBottom instanceof ResizerBottom) {
            resizerBottom.onStart(node, pointer, id, ev, false);
        }
    }
    
    onResize(node: NodeUI, pointer: Pointer<Pos>, id: string, ev: MouseEvent) {
        const resizerRight = this.parent.elements.get("ResizerRight");
        const resizerBottom = this.parent.elements.get("ResizerBottom");
        
        if (resizerRight instanceof ResizerRight) {
            resizerRight.onResize(node, pointer, id, ev);
        }
        
        if (resizerBottom instanceof ResizerBottom) {
            resizerBottom.onResize(node, pointer, id, ev);
        }
    }
    
    onEnd(node: NodeUI, pointer: Pointer<Pos>, id: string, ev: MouseEvent) {
        document.body.style.cursor = null;
    }
}

class ResizerBottomLeft extends Resizer {
    mounted() {
        super.mounted();
        
        if (this.options.movement === false) {
            return;
        }
        
        this.interactive.move.enable(new Pointer<Pos>(), "main");
        
        this.interactive.move.onStart = this.onStart.bind(this);
        this.interactive.move.onMove = this.onResize.bind(this);
        this.interactive.move.onEnd = this.onEnd.bind(this);
    }
    
    onStart(node: NodeUI, pointer: Pointer<Pos>, id: string, ev: MouseEvent) {
        document.body.style.cursor = "nwse-resize !important";
        
        const resizerLeft = this.parent.elements.get("ResizerLeft");
        const resizerBottom = this.parent.elements.get("ResizerBottom");
        
        if (resizerLeft instanceof ResizerLeft) {
            resizerLeft.onStart(node, pointer, id, ev, false);
        }
        
        if (resizerBottom instanceof ResizerBottom) {
            resizerBottom.onStart(node, pointer, id, ev, false);
        }
    }
    
    onResize(node: NodeUI, pointer: Pointer<Pos>, id: string, ev: MouseEvent) {
        const resizerLeft = this.parent.elements.get("ResizerLeft");
        const resizerBottom = this.parent.elements.get("ResizerBottom");
        
        if (resizerLeft instanceof ResizerLeft) {
            resizerLeft.onResize(node, pointer, id, ev);
        }
        
        if (resizerBottom instanceof ResizerBottom) {
            resizerBottom.onResize(node, pointer, id, ev);
        }
    }
    
    onEnd(node: NodeUI, pointer: Pointer<Pos>, id: string, ev: MouseEvent) {
        document.body.style.cursor = null;
    }
}
