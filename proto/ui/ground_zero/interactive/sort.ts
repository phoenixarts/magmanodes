'use strict';

class Sort extends InteractiveLibrary {
    public onBeforeStart = () => {};
    public onStart = () => {};
    public onEnd = () => {};
    public onSort = () => {};
    public ui: ListUI<any>;
    public listPointer: ListPointer<any>;
    public onMouseEnterBinded: VoidFunction;
    
    constructor(interactive: InteractiveUI) {
        super(interactive);
        
        if (this.interactive.ui.parent instanceof ListUI) {
            this.ui = this.interactive.ui.parent;
        }
    }
    
    _onBeforeStart() {
        if (this.onBeforeStart) {
            this.onBeforeStart();
        }
    }
    
    _onStart() {
        this.ui.sorting = true;
        this.onStart();
    }
    
    _onEnd() {
        this.ui.sorting = false;
        
        this.onEnd();
    }
    
    onEnter() {
        if (View.dragging.value === false) {
            return;
        }
        
        if (this.ui.sorting === false) {
            return;
        }
        
        const placeholderNode = View.dragPlaceholder.value;
        const targetNode = this.interactive.ui.node.node;
        
        if (targetNode.compareDocumentPosition(placeholderNode) & 0x04) { // Target is after hovered
            targetNode.before(placeholderNode);
            
            this.listPointer.sort(this.interactive.ui.name, View.dragElement.value.name, 0);
        } else {
            targetNode.after(placeholderNode);
            
            this.listPointer.sort(this.interactive.ui.name, View.dragElement.value.name, 1);
        }
        
        this.onSort();
    }
    
    enable(listPointer: ListPointer<any>, nodeName: string = "main", placeholderColor: string = "#0001", floating: string = "none") {
        if (this.enabled) {
            return;
        }
        
        this.enabled = true;
        
        this.interactive.drag.enable(nodeName, placeholderColor, floating);
        this.interactive.drag.onBeforeStart = this._onBeforeStart.bind(this);
        this.interactive.drag.onStart = this._onStart.bind(this);
        this.interactive.drag.onEnd = this._onEnd.bind(this);
        this.onMouseEnterBinded = this.onEnter.bind(this);
        
        this.interactive.ui.node.node.addEventListener("mouseenter", this.onMouseEnterBinded, false);
        
        this.listPointer = new ListPointer("id_" + performance.now() + "_" + this.random(1, 99999), listPointer.listID);
    }
    
    disable() {
        if (this.enabled === false) {
            return;
        }
        
        this.listPointer.unmount();
        this.listPointer = null;
        
        this.interactive.ui.node.node.removeEventListener("mouseenter", this.onMouseEnterBinded);
        this.interactive.drag.unmount();
        
        this.enabled = false;
    }
    
    unmount() {
        this.disable();
    }
}
