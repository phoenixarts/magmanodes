'use strict';

declare type UIChildren = Array<NodeUI | UI | NodeComment | null>;

abstract class ShortcutsUI extends Unit {
    constructor() {
        super();
    }
    
    /** @abstract Method is abstract, implemented in UI class */
    abstract create(type: string, options: Partial<OptionsUI>, children: UIChildren): NodeUI;
    
    div(options: Partial<OptionsUI> = {}, children: UIChildren = []) { return this.create("div", new OptionsUI(options ? options : {}), children) }
    span(options: Partial<OptionsUI> = {}, children: UIChildren = []) { return this.create("span", new OptionsUI(options ? options : {}), children) }
    a(options: Partial<OptionsUI> = {}, children: UIChildren = []) { return this.create("a", new OptionsUI(options ? options : {}), children) }
    li(options: Partial<OptionsUI> = {}, children: UIChildren = []) { return this.create("li", new OptionsUI(options ? options : {}), children) }
    ul(options: Partial<OptionsUI> = {}, children: UIChildren = []) { return this.create("ul", new OptionsUI(options ? options : {}), children) }
    p(options: Partial<OptionsUI> = {}, children: UIChildren = []) { return this.create("p", new OptionsUI(options ? options : {}), children) }
    form(options: Partial<OptionsUI> = {}, children: UIChildren = []) { return this.create("form", new OptionsUI(options ? options : {}), children) }
    input(options: Partial<OptionsUI> = {}, children: UIChildren = []) { return this.create("input", new OptionsUI(options ? options : {}), children) }
    textarea(options: Partial<OptionsUI> = {}, children: UIChildren = []) { return this.create("textarea", new OptionsUI(options ? options : {}), children) }
    button(options: Partial<OptionsUI> = {}, children: UIChildren = []) { return this.create("button", new OptionsUI(options ? options : {}), children) }
    video(options: Partial<OptionsUI> = {}, children: UIChildren = []) { return this.create("video", new OptionsUI(options ? options : {}), children) }
    audio(options: Partial<OptionsUI> = {}, children: UIChildren = []) { return this.create("audio", new OptionsUI(options ? options : {}), children) }
    canvas(options: Partial<OptionsUI> = {}, children: UIChildren = []) { return this.create("canvas", new OptionsUI(options ? options : {}), children) }
}
