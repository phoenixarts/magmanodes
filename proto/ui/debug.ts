
const { remote } = require('electron')

window.addEventListener('keydown', e => {
    if (e.key === 'F6') {
        remote.getCurrentWebContents().openDevTools()
    }
    
    else if (e.key === 'F5') {
        remote.getCurrentWindow().reload()
    }
})