'use strict';
class Ground {
    random(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }
}
let _alphabetIndex = 0.1;
const AlphabedIndexes = {
    a: _alphabetIndex *= 2,
    b: _alphabetIndex *= 2,
    c: _alphabetIndex *= 2,
    d: _alphabetIndex *= 2,
    e: _alphabetIndex *= 2,
    f: _alphabetIndex *= 2,
    g: _alphabetIndex *= 2,
    h: _alphabetIndex *= 2,
    i: _alphabetIndex *= 2,
    j: _alphabetIndex *= 2,
    k: _alphabetIndex *= 2,
    l: _alphabetIndex *= 2,
    m: _alphabetIndex *= 2,
    n: _alphabetIndex *= 2,
    o: _alphabetIndex *= 2,
    p: _alphabetIndex *= 2,
    q: _alphabetIndex *= 2,
    r: _alphabetIndex *= 2,
    s: _alphabetIndex *= 2,
    t: _alphabetIndex *= 2,
    u: _alphabetIndex *= 2,
    v: _alphabetIndex *= 2,
    w: _alphabetIndex *= 2,
    x: _alphabetIndex *= 2,
    y: _alphabetIndex *= 2,
    z: _alphabetIndex *= 2
};
class Unit extends Ground {
    constructor() {
        super(...arguments);
        this.timeouts = new Map();
        this.intervals = new Map();
    }
    setTimeout(func, delay, name = "") {
        if (name.length === 0) {
            name = "timeout_" + performance.now() + "_" + this.random(1, 99999);
        }
        if (this.timeouts.has(name)) {
            this.clearTimeout(name);
        }
        const id = setTimeout(func, delay);
        this.timeouts.set(name, id);
        return id;
    }
    clearTimeout(name = "") {
        if (this.timeouts.has(name) === false) {
            return;
        }
        clearTimeout(this.timeouts.get(name));
        this.timeouts.delete(name);
    }
    setInterval(func, delay, name = "") {
        if (name.length === 0) {
            name = "interval_" + performance.now() + "_" + this.random(1, 99999);
        }
        if (this.intervals.has(name)) {
            this.clearInterval(name);
        }
        const id = setInterval(func, delay);
        this.intervals.set(name, id);
        return id;
    }
    clearInterval(name = "") {
        if (this.intervals.has(name) === false) {
            return;
        }
        clearInterval(this.intervals.get(name));
        this.intervals.delete(name);
    }
    numberify(name) {
        let result = 1;
        for (let i = 0; i < name.length; i++) {
            if (AlphabedIndexes[name[i]]) {
                result /= AlphabedIndexes[name[i]] + (i * i * i * i * i);
            }
        }
        return result;
    }
    uniqueNumber() {
        return parseInt(Date.now() + "" + this.random(1, 99999) + "" + this.random(1, 99999));
    }
    uniqueID() {
        return Date.now() + "_" + this.random(1, 99999) + "_" + this.random(1, 99999);
    }
    construct(key, val) {
        if (typeof this[key] !== "undefined") {
            this[key] = val;
        }
    }
}
class AssocArray {
    constructor() {
        this.items = [];
        this.ids = new Map();
        this.size = 0;
    }
    get(id) {
        return this.ids.get(id)().item;
    }
    foreach(callback) {
        for (let i = 0; i < this.items.length; i++) {
            const result = callback(this.items[i].item, this.items[i].id);
            if (typeof result === "boolean") {
                if (result === false) {
                    break;
                }
            }
        }
    }
    forEach(callback) {
        this.foreach(callback);
    }
    has(id) {
        return this.ids.has(id);
    }
    sort(callback) {
        this.items.sort((a, b) => callback(a.item, b.item, a.id, b.id));
    }
    set(id, item) {
        if (this.has(id)) {
            this.delete(id);
        }
        const obj = {
            item,
            id
        };
        const func = ((obj) => obj).bind(this, obj);
        this.items.push(obj);
        this.ids.set(id, func);
        this.size++;
    }
    clean(callback) {
        this.foreach((item, id) => {
            callback(item, id);
        });
        this.ids.clear();
        this.items = [];
        this.size = 0;
    }
    delete(id) {
        if (this.ids.has(id) === false) {
            return;
        }
        this.ids.delete(id);
        for (let i = 0; i < this.items.length; i++) {
            if (this.items[i].id === id) {
                this.items.splice(i, 1);
                break;
            }
        }
        this.size--;
    }
}
class AssocList extends Unit {
    constructor(items) {
        super();
        this.ids = new Map();
        this.indexes = new Map();
        this.firstNode = null;
        this.lastNode = null;
        this.size = 0;
        if (items) {
            for (let i = 0; i < items.length; i++) {
                if (items[i] instanceof Array) {
                    this.set(items[i][0], items[i][1]);
                }
                else {
                    this.add(items[i]);
                }
            }
        }
    }
    /**
     *          --- Pawel ---
     * This method returns id of certain index
     * @param id - index in the array
     */
    readId(id) {
        return [...this.ids.keys()][id];
    }
    /**
     *          --- Pawel ---
     * This method returns all the ids
     */
    readIds() {
        return Array.from(this.ids.keys());
    }
    export() {
        const arr = [];
        this.foreach((item, id) => {
            arr.push([id, item]);
        });
        return arr;
    }
    get first() {
        return this.firstNode;
    }
    get last() {
        return this.lastNode;
    }
    add(item) {
        if (this instanceof Mix && typeof item === "string") {
            this.set(item, item);
            return item;
        }
        const id = this.uniqueID();
        this.set(id, item);
        return id;
    }
    set(id, item) {
        const node = {
            data: item,
            id,
            prev: this.lastNode,
            next: null
        };
        if (this.lastNode !== null) {
            this.lastNode.next = node;
        }
        if (this.firstNode === null) {
            this.firstNode = node;
        }
        this.lastNode = node;
        this.ids.set(id, () => node);
        this.size++;
    }
    index(n) {
        let i = 0;
        let result = null;
        this.foreach(item => {
            if (i === n) {
                result = item;
                return false;
            }
            i++;
        });
        return result;
    }
    iteratedSort(callback) {
        // ...
    }
    sort(beforeID, afterID) {
        if (beforeID === afterID) {
            return;
        }
        if (this.ids.has(beforeID) === false) {
            return;
        }
        const before = this.getNode(beforeID);
        if (before.prev && before.next) {
            before.next.prev = before.prev;
            before.prev.next = before.next;
        }
        else if (before.prev) {
            before.prev.next = null;
            this.lastNode = before.prev;
        }
        else if (before.next) {
            before.next.prev = null;
            this.firstNode = before.next;
        }
        if (afterID === null) {
            this.lastNode.next = before;
            before.prev = this.lastNode;
            before.next = null;
            this.lastNode = before;
            return;
        }
        const after = this.getNode(afterID);
        if (this.ids.has(afterID) === false) {
            return;
        }
        if (after.prev) {
            after.prev.next = before;
            before.prev = after.prev;
        }
        else {
            before.prev = null;
            this.firstNode = before;
        }
        before.next = after;
        after.prev = before;
        if (after.id === this.first.id) {
            before.prev = null;
            this.firstNode = before;
        }
    }
    has(id) {
        return this.ids.has(id);
    }
    /**
     *          --- Pawel ---
     * Get multiple data based on multiple ids
     * @param ids - IDs of an element
     */
    getMore(...ids) {
        const result = [];
        for (const id of ids) {
            if (this.has(id) === false) {
                return null;
            }
            result.push(this.ids.get(id)().data);
        }
        return result;
    }
    get(id) {
        if (this.has(id) === false) {
            return null;
        }
        return this.ids.get(id)().data;
    }
    getNode(id) {
        return this.ids.get(id)();
    }
    clean(callback) {
        if (callback) {
            this.foreach(callback);
        }
        this.ids = new Map();
        this.firstNode = null;
        this.lastNode = null;
        this.size = 0;
    }
    delete(id) {
        if (this.has(id) === false) {
            return;
        }
        let firstLastDelete = false;
        if (this.firstNode.id === id) {
            if (this.firstNode.next === null) {
                this.firstNode = null;
            }
            else {
                if (this.firstNode.next.id === id) {
                    this.firstNode = null;
                    this.lastNode = null;
                }
                else {
                    this.firstNode.next.prev = null;
                    this.firstNode = this.firstNode.next;
                }
            }
            firstLastDelete = true;
        }
        if (this.lastNode.id === id) {
            if (this.lastNode.prev === null) {
                this.lastNode = null;
            }
            else {
                if (this.lastNode.prev.id === id) {
                    this.firstNode = null;
                    this.lastNode = null;
                }
                else {
                    this.lastNode.prev.next = null;
                    this.lastNode = this.lastNode.prev;
                }
            }
            firstLastDelete = true;
        }
        if (firstLastDelete === false) {
            const node = this.ids.get(id)();
            if (node.prev !== null && node.next !== null) {
                node.prev.next = node.next;
                node.next.prev = node.prev;
            }
        }
        this.ids.delete(id);
        this.size--;
    }
    foreach(callback, reversed = false) {
        let next = null;
        for (let i = 0; i < 1; i) {
            if (reversed === false) {
                next = this.doNext(next, callback);
            }
            else {
                next = this.doPrev(next, callback);
            }
            if (next === null) {
                break;
            }
        }
    }
    forEach(callback) {
        this.foreach(callback);
    }
    doNext(node, callback) {
        if (node === null) {
            node = this.firstNode;
        }
        if (node === null) {
            return null;
        }
        const result = callback(node.data, node.id);
        if (typeof result === "boolean") {
            if (result === false) {
                return null;
            }
        }
        return node.next;
    }
    doPrev(node, callback) {
        if (node === null) {
            node = this.lastNode;
        }
        if (node === null) {
            return null;
        }
        const result = callback(node.data, node.id);
        if (typeof result === "boolean") {
            if (result === false) {
                return null;
            }
        }
        return node.prev;
    }
    copy() {
        const copy = new Arr();
        this.foreach((item, id) => {
            copy.set(id, item);
        });
        return copy;
    }
    get array() {
        const arr = [];
        this.foreach(item => {
            arr.push(item);
        });
        return arr;
    }
    get map() {
        const map = new Map();
        ;
        this.foreach((item, id) => {
            map.set(id, item);
        });
        return map;
    }
    // Solid debug purposes
    get obj() {
        const obj = {};
        this.foreach((item, id) => {
            obj[id] = item;
        });
        return obj;
    }
}
class Arr extends AssocList {
}
class Mix extends Arr {
}
class Vec3D {
    constructor(data) {
        this.x = 0;
        this.y = 0;
        this.z = 0;
        this.percentX = false;
        this.percentY = false;
        this.percentZ = false;
        this.offsetX = 0;
        this.offsetY = 0;
        this.offsetZ = 0;
        if (data) {
            this.x = typeof data.x === "undefined" ? 0 : data.x;
            this.y = typeof data.y === "undefined" ? 0 : data.y;
            this.z = typeof data.z === "undefined" ? 0 : data.z;
            this.percentX = typeof data.percentX === "undefined" ? false : data.percentX;
            this.percentY = typeof data.percentY === "undefined" ? false : data.percentY;
            this.percentZ = typeof data.percentZ === "undefined" ? false : data.percentZ;
            this.offsetX = typeof data.offsetX === "undefined" ? 0 : data.offsetX;
            this.offsetY = typeof data.offsetY === "undefined" ? 0 : data.offsetY;
            this.offsetZ = typeof data.offsetZ === "undefined" ? 0 : data.offsetZ;
        }
    }
}
class Pos extends Vec3D {
}
class Size extends Vec3D {
}
class List extends Ground {
    constructor(id) {
        super();
        this.items = new AssocList();
        this.pointers = new AssocList();
        this.appliedEventIDs = new Map();
        this.id = id;
    }
    catchWaitingList() {
        if (Data.listPointersWaiting.has(this.id)) {
            const waitingList = Data.listPointersWaiting.get(this.id);
            for (let i = 0; i < waitingList.length; i++) {
                waitingList[i].mount();
                waitingList[i].notifyMountUI();
            }
            Data.listPointersWaiting.delete(this.id);
        }
    }
    mount() {
        this.catchWaitingList();
    }
    clean() {
        this.items.clean((item, id) => {
            this.remove(id);
        });
    }
    add(structure, id = "") {
        if (id.length === 0) {
            id = performance.now() + "_" + this.random(1, 99999);
        }
        this.items.set(id, new Item(id, structure));
        this.broadcastAdd(id);
    }
    remove(id = "") {
        this.items.delete(id);
        this.broadcastRemove(id);
    }
    sort(elementID1 = "", elementID2 = "", type = 0) {
        this.broadcastSort(elementID1, elementID2, type);
    }
    unmount() {
        this.broadcastUnmount();
    }
    broadcastAdd(id) {
        this.pointers.foreach(pointer => {
            if (pointer.onAdd) {
                pointer.onAdd(id);
            }
        });
    }
    broadcastRemove(id) {
        this.pointers.foreach(pointer => {
            if (pointer.onRemove) {
                pointer.onRemove(id);
            }
        });
    }
    broadcastSort(elementID1 = "", elementID2 = "", type = 0) {
        if (type === 0) { // beforeID - afterID
            this.items.sort(elementID2, elementID1);
        }
        else { // afterID - beforeID
            if (elementID1 === this.items.last.id) {
                this.items.sort(elementID2, null);
            }
            else {
                this.items.sort(elementID1, elementID2);
            }
        }
        this.pointers.foreach(pointer => {
            if (pointer.onSort) {
                pointer.onSort(elementID1, elementID2, type);
            }
        });
    }
    broadcastUnmount() {
        this.pointers.foreach(pointer => {
            if (pointer.onUnmount) {
                pointer.onUnmount();
            }
        });
    }
}
class Item extends Ground {
    constructor(id = "", data) {
        super();
        this.pointers = new AssocArray();
        this.id = id;
        this.data = data;
    }
    update(prop, newValue) {
        const oldValue = this.data[prop];
        this.data[prop] = newValue;
        this.broadcast(prop, newValue, oldValue);
    }
    broadcast(prop, newValue, oldValue) {
        if (this.pointers.has(prop) === false) {
            return;
        }
        const pointers = this.pointers.get(prop);
        for (let i = 0; i < pointers.length; i++) {
            pointers[i].onUpdate(newValue, oldValue);
        }
    }
}
class ListPointer extends Ground {
    constructor(eventID, listID, onAdd = null, onRemove = null, onSort = null, onUnmount = null, autoMount = true) {
        super();
        this.eventID = eventID.length !== 0 ? eventID : "listEvent_" + performance.now() + "_" + this.random(1, 99999);
        this.listID = listID;
        this.onAdd = onAdd;
        this.onRemove = onRemove;
        this.onSort = onSort;
        this.onUnmount = onUnmount;
        this.mounted = false;
        this.list = null;
        if (autoMount) {
            this.mount();
        }
    }
    size() {
        return this.list.items.size;
    }
    clean() {
        this.list.clean();
    }
    item(id = "") {
        if (this.list.items.has(id) === false) {
            return null;
        }
        return this.list.items.get(id);
    }
    add(structure, id = "") {
        this.list.add(structure, id);
    }
    remove(id = "") {
        if (this.list) {
            this.list.remove(id);
        }
    }
    sort(elementID1 = "", elementID2 = "", type = 0) {
        this.list.sort(elementID1, elementID2, type);
    }
    registerWaitingList() {
        if (Data.listPointersWaiting.has(this.listID) === false) {
            Data.listPointersWaiting.set(this.listID, []);
        }
        Data.listPointersWaiting.get(this.listID).push(this);
    }
    notifyMountUI() {
        if (this.onWaitedMount) {
            this.onWaitedMount();
        }
    }
    mount() {
        if (Data.lists.has(this.listID) === false) {
            this.mounted = false;
            this.registerWaitingList();
            return;
        }
        const list = Data.lists.get(this.listID);
        if (list.pointers.has(this.eventID)) {
            this.mounted = false;
            return;
        }
        list.pointers.set(this.eventID, this);
        this.list = list;
        this.mounted = true;
    }
    unmount() {
        if (Data.lists.has(this.listID) === false) {
            this.mounted = false;
            return;
        }
        const list = Data.lists.get(this.listID);
        if (list.pointers.has(this.eventID) === false) {
            this.mounted = false;
            return;
        }
        list.pointers.delete(this.eventID);
        this.list = null;
        this.mounted = false;
    }
}
// Add generic type for prop type
class Pointer extends Ground {
    constructor(listID = "", itemID = "", prop = "", onUpdate = null, onUnmount = null, autoMount = true) {
        super();
        this.listID = listID;
        this.itemID = itemID;
        this.prop = prop;
        this.onUnmount = onUnmount;
        this.onUpdate = onUpdate;
        this.mounted = false;
        this.uniqueID = performance.now() + "_" + this.random(1, 99999);
        if (autoMount) {
            this.mount();
        }
    }
    get item() {
        if (Data.lists.has(this.listID) === false) {
            return null;
        }
        return Data.lists.get(this.listID).items.get(this.itemID);
    }
    get value() {
        const item = this.item;
        if (item) {
            return this.item.data[this.prop];
        }
        return null;
    }
    set value(newValue) {
        if (this.mounted === false) {
            return;
        }
        const item = this.item;
        if (item) {
            this.item.update(this.prop, newValue);
        }
    }
    update() {
        this.value = this.value;
    }
    mount() {
        if (Data.lists.has(this.listID) === false) {
            this.mounted = false;
            // if (this.listID.length !== 0 && this.itemID.length !== 0) {
            //     setTimeout(() => {
            //         console.log("remounting pointer on list not found", this.listID, this.itemID, this.prop);
            //         // this.mount();
            //     }, 0);
            // }
            return;
        }
        const list = Data.lists.get(this.listID);
        if (list.items.has(this.itemID) === false) {
            this.mounted = false;
            return;
        }
        if (this.onUpdate !== null) {
            if (this.item.pointers.has(this.prop) === false) {
                this.item.pointers.set(this.prop, []);
            }
            this.item.pointers.get(this.prop).push(this);
        }
        this.mounted = true;
    }
    unmount() {
        if (Data.lists.has(this.listID) === false) {
            this.mounted = false;
            return;
        }
        const list = Data.lists.get(this.listID);
        if (list.items.has(this.itemID) === false) {
            this.mounted = false;
            return;
        }
        if (this.onUpdate !== null) {
            if (this.item.pointers.has(this.prop) === false) {
                this.mounted = false;
                return;
            }
            const siblings = this.item.pointers.get(this.prop);
            for (let i = 0; i < siblings.length; i++) {
                if (siblings[i].uniqueID === this.uniqueID) {
                    siblings.splice(i, 1);
                    break;
                }
            }
        }
        this.mounted = false;
    }
}
class Struct {
}
const Data = {
    lists: new AssocList(),
    create: (id = "") => {
        if (Data.lists.has(id)) {
            // console.warn("Trying to add list with already existing listID.");
            return;
        }
        const list = new List(id);
        Data.lists.set(id, list);
        list.mount();
    },
    remove: (id = "") => {
        if (Data.lists.has(id) === false) {
            // console.warn("Trying to remove nonexisting list.");
            return;
        }
        Data.lists.get(id).unmount();
        Data.lists.delete(id);
    },
    data: (listID, itemID) => Data.lists.get(listID).items.get(itemID).data,
    listPointersWaiting: new Map()
};
class ShortcutsUI extends Unit {
    constructor() {
        super();
    }
    div(options = {}, children = []) { return this.create("div", new OptionsUI(options ? options : {}), children); }
    span(options = {}, children = []) { return this.create("span", new OptionsUI(options ? options : {}), children); }
    a(options = {}, children = []) { return this.create("a", new OptionsUI(options ? options : {}), children); }
    li(options = {}, children = []) { return this.create("li", new OptionsUI(options ? options : {}), children); }
    ul(options = {}, children = []) { return this.create("ul", new OptionsUI(options ? options : {}), children); }
    p(options = {}, children = []) { return this.create("p", new OptionsUI(options ? options : {}), children); }
    form(options = {}, children = []) { return this.create("form", new OptionsUI(options ? options : {}), children); }
    input(options = {}, children = []) { return this.create("input", new OptionsUI(options ? options : {}), children); }
    textarea(options = {}, children = []) { return this.create("textarea", new OptionsUI(options ? options : {}), children); }
    button(options = {}, children = []) { return this.create("button", new OptionsUI(options ? options : {}), children); }
    video(options = {}, children = []) { return this.create("video", new OptionsUI(options ? options : {}), children); }
    audio(options = {}, children = []) { return this.create("audio", new OptionsUI(options ? options : {}), children); }
    canvas(options = {}, children = []) { return this.create("canvas", new OptionsUI(options ? options : {}), children); }
}
class OptionsUI {
    constructor(data) {
        this.name = typeof data.name === "undefined" ? "" : data.name;
        this.text = typeof data.text === "undefined" ? "" : data.text;
        this.html = typeof data.html === "undefined" ? "" : data.html;
        this.style = typeof data.style === "undefined" ? "" : data.style;
        this.width = typeof data.width === "undefined" ? "" : data.width;
        this.height = typeof data.height === "undefined" ? "" : data.height;
        this.className = typeof data.className === "undefined" ? "" : data.className;
        this.stateText = typeof data.stateText === "undefined" ? null : data.stateText;
        this.stateStyle = typeof data.stateStyle === "undefined" ? null : data.stateStyle;
        this.stateClass = typeof data.stateClass === "undefined" ? null : data.stateClass;
        this.type = typeof data.type === "undefined" ? "" : data.type;
        this.value = typeof data.value === "undefined" ? "" : data.value;
        this.placeholder = typeof data.placeholder === "undefined" ? "" : data.placeholder;
        this.stateValue = typeof data.stateValue === "undefined" ? null : data.stateValue;
        this.onClick = typeof data.onClick === "undefined" ? null : data.onClick;
        this.onChange = typeof data.onChange === "undefined" ? null : data.onChange;
        this.onFocus = typeof data.onFocus === "undefined" ? null : data.onFocus;
        this.onBlur = typeof data.onBlur === "undefined" ? null : data.onBlur;
        this.onDoubleClick = typeof data.onDoubleClick === "undefined" ? null : data.onDoubleClick;
        this.onMouseDown = typeof data.onMouseDown === "undefined" ? null : data.onMouseDown;
        this.onMouseUp = typeof data.onMouseUp === "undefined" ? null : data.onMouseUp;
        this.onMouseMove = typeof data.onMouseMove === "undefined" ? null : data.onMouseMove;
        this.onMouseEnter = typeof data.onMouseEnter === "undefined" ? null : data.onMouseEnter;
        this.onMouseLeave = typeof data.onMouseLeave === "undefined" ? null : data.onMouseLeave;
        this.onMouseOver = typeof data.onMouseOver === "undefined" ? null : data.onMouseOver;
        this.onMouseOut = typeof data.onMouseOut === "undefined" ? null : data.onMouseOut;
        this.onRightClick = typeof data.onRightClick === "undefined" ? null : data.onRightClick;
        this.onKeyUp = typeof data.onKeyUp === "undefined" ? null : data.onKeyUp;
        this.onKeyDown = typeof data.onKeyDown === "undefined" ? null : data.onKeyDown;
        this.onScroll = typeof data.onScroll === "undefined" ? null : data.onScroll;
        this.onMouseWheel = typeof data.onMouseWheel === "undefined" ? null : data.onMouseWheel;
        this.onSubmit = typeof data.onSubmit === "undefined" ? null : data.onSubmit;
        this.onResize = typeof data.onResize === "undefined" ? null : data.onResize;
        this._onSubmit = typeof data._onSubmit === "undefined" ? null : data._onSubmit;
        this.attributes = typeof data.attributes === "undefined" ? [] : data.attributes;
    }
}
Data.create("ui");
const uiListPointer = new ListPointer("ui", "ui");
uiListPointer.add({
    cursor: new Pos(),
    windowResponderActive: false,
    windowKeyDownEvents: [],
    windowKeyUpEvents: [],
    dragging: false,
    dragElement: null,
    dragPlaceholder: null,
    dragPos: new Pos(),
    dragLock: false,
}, "main");
const View = {
    cursor: new Pointer("ui", "main", "cursor"),
    windowResponderActive: new Pointer("ui", "main", "windowResponderActive"),
    windowKeyDownEvents: new Pointer("ui", "main", "windowKeyDownEvents"),
    windowKeyUpEvents: new Pointer("ui", "main", "windowKeyUpEvents"),
    dragging: new Pointer("ui", "main", "dragging"),
    dragElement: new Pointer("ui", "main", "dragElement"),
    dragPlaceholder: new Pointer("ui", "main", "dragPlaceholder"),
    dragPos: new Pointer("ui", "main", "dragPos"),
    dragLock: new Pointer("ui", "main", "dragLock"),
};
class UI extends ShortcutsUI {
    constructor() {
        super(...arguments);
        this.elements = new Map();
        this.nodes = new Map();
        this.node = null;
        this.name = "";
        this.options = null;
        this.propPointers = new Map();
        this.interactive = null;
        this.isMounted = false;
        this._windowMouseUp = null;
        this._windowMouseDown = null;
        this._windowMouseMove = null;
    }
    setup(parent, options) {
        this.parent = parent;
        this.options = options;
        this.interactive = new InteractiveUI(this);
    }
    initialise(query = "") {
        const domNode = document.querySelector(query);
        if (domNode === null) {
            throw "UI Initialization failed, query[\"" + query + "\"] cannot find node.";
        }
        this.name = "root";
        this.mount();
        domNode.appendChild(this.node.node);
        this.prepareWindow();
    }
    _windowMouseUpEvent(ev) {
        window.removeEventListener("mousemove", this._windowMouseMove);
        View.windowResponderActive.value = false;
    }
    _windowMouseMoveEvent(ev) {
        View.cursor.value.x = ev.pageX;
        View.cursor.value.y = ev.pageY;
        View.cursor.update();
    }
    _windowMouseDownEvent(ev) {
        View.cursor.value.x = ev.pageX;
        View.cursor.value.y = ev.pageY;
        View.cursor.update();
        View.windowResponderActive.value = true;
        window.addEventListener("mousemove", this._windowMouseMove, false);
    }
    prepareWindow() {
        this._windowMouseUp = this._windowMouseUpEvent.bind(this);
        this._windowMouseMove = this._windowMouseMoveEvent.bind(this);
        this._windowMouseDown = this._windowMouseDownEvent.bind(this);
        window.addEventListener("mouseup", this._windowMouseUp, false);
        window.addEventListener("mousedown", this._windowMouseDown, false);
    }
    releaseWindow() {
        window.removeEventListener("mouseup", this._windowMouseUp);
        window.removeEventListener("mousedown", this._windowMouseDown);
    }
    mount(external = false) {
        this.beforeMount();
        const node = this.display();
        this.nodes.delete(node.options.name);
        let baseObj = Object.getPrototypeOf(this);
        let searchingForParents = true;
        while (searchingForParents) {
            if (baseObj.__proto__ === null) {
                searchingForParents = false;
                break;
            }
            node.node.classList.add(baseObj.__proto__.constructor.name);
            baseObj = baseObj.__proto__;
        }
        baseObj = null;
        node.options.name = "main";
        node.node.classList.add(this.constructor.name);
        node.node.classList.add("main");
        if (this.name.length !== 0) {
            node.node.classList.add(this.name);
        }
        this.nodes.set(node.options.name, node);
        this.node = node;
        this.isMounted = true;
        if (external === false) {
            this.mounted();
        }
    }
    unmount(done) {
        this.onUnmount(() => {
            this.onUnmountSync();
            this.doUnmount();
            if (done) {
                done();
            }
        });
    }
    onUnmountSync() {
    }
    doUnmount() {
        this.interactive.move.unmount();
        this.interactive.resize.unmount();
        this.interactive.drag.unmount();
        this.interactive.drop.unmount();
        this.interactive.sort.unmount();
        this.interactive.padding.unmount();
        this.nodes.forEach(node => {
            // Problem ID[1]: this can cause sub element async unmount to lose the parent node of it's main node
            // Solution?
            this.cleanNodeEvents(node);
        });
        // Problem ID[1]
        if (this.node.node.parentNode !== null) {
            this.node.node.parentNode.removeChild(this.node.node);
        }
        if (this.parent !== null) {
            this.parent.elements.delete(this.name);
        }
        this.propPointers.forEach(pointers => {
            for (let i = 0; i < pointers.length; i++) {
                pointers[i].unmount();
            }
        });
        this.propPointers.clear();
        this.node = null;
        this.nodes.clear();
        this.elements.forEach(element => {
            element.unmount();
        });
    }
    element(classReference, options, name = "") {
        if (name.length === 0) {
            name = classReference.name;
        }
        const element = new classReference();
        element.setup(this, options);
        element.name = name;
        this.elements.set(name, element);
        return element;
    }
    addElement(nodeName, classReference, options, name = "") {
        const element = this.element(classReference, options, name);
        const node = this.nodes.get(nodeName);
        this.applyElement(node, element);
    }
    create(type, options, children) {
        const node = new NodeUI(document.createElement(type), new OptionsUI(options));
        this.setupNodeClass(node);
        this.setupNodeSize(node);
        this.mountNode(node);
        this.setupFormProbability(node, type, children);
        this.setupNodeAttributes(node);
        this.setupNodeType(node);
        this.setupNodeText(node);
        this.setupNodeValue(node);
        this.setupNodeStyle(node);
        this.setupNodeEvents(node);
        this.applyChildren(node, children);
        return node;
    }
    mountNode(node) {
        if (node.options.name.length === 0) {
            node.options.name = performance.now() + "_" + this.random(1, 99999);
        }
        else {
            node.node.classList.add(node.options.name);
        }
        this.nodes.set(node.options.name, node);
    }
    applyChildren(node, children) {
        if (children.length !== 0) {
            for (let i = 0; i < children.length; i++) {
                const applicableNode = children[i];
                if (applicableNode !== null) {
                    if (applicableNode instanceof NodeUI) {
                        this.applyNode(node, applicableNode);
                    }
                    else if (applicableNode instanceof NodeComment) {
                        this.applyStateMount(node, applicableNode);
                    }
                    else if (applicableNode instanceof NodeSwitch) {
                        this.applyStateSwitch(node, applicableNode);
                    }
                    else if (applicableNode instanceof NodePosition) {
                        this.applyStatePosition(node, applicableNode);
                    }
                    else if (applicableNode instanceof UI) {
                        this.applyElement(node, applicableNode);
                    }
                }
            }
        }
    }
    applyNode(node, child) {
        node.node.appendChild(child.node);
    }
    applyStateMount(node, child) {
        if (child.name.length === 0) {
            if (child.elementReference()) {
                child.name = child.elementReference().name;
            }
        }
        node.node.appendChild(child.comment);
        // Memory leak?
        const pointers = [];
        for (let i = 0; i < child.pointers.length; i++) {
            pointers.push(new Pointer(child.pointers[i].listID, child.pointers[i].itemID, child.pointers[i].prop, () => {
                const result = child.onUpdate();
                if (result) {
                    if (this.elements.has(child.name) === false) {
                        const element = this.element(child.elementReference(), child.data(), child.name);
                        element.mount();
                        child.comment.after(element.node.node);
                    }
                }
                else {
                    if (this.elements.has(child.name)) {
                        this.elements.get(child.name).unmount();
                        this.elements.delete(child.name);
                    }
                }
            }));
            this.propPointers.set(child.name + "_mounts", pointers);
            pointers[pointers.length - 1].onUpdate();
        }
    }
    applyStateSwitch(node, child) {
        if (child.name.length === 0) {
            if (child.elementReference()) {
                child.name = child.elementReference().name;
            }
        }
        node.node.appendChild(child.comment);
        // Memory leak?
        const pointers = [];
        let lastResult = "";
        for (let i = 0; i < child.pointers.length; i++) {
            const p = new Pointer(child.pointers[i].listID, child.pointers[i].itemID, child.pointers[i].prop, () => {
                const result = child.onUpdate();
                if ((result !== lastResult || child.refreshable) && p.value !== null) {
                    lastResult = result;
                    if (this.elements.has(child.name)) {
                        this.elements.get(child.name).unmount();
                        this.elements.delete(child.name);
                    }
                    const element = this.element(result, child.data(), child.name);
                    element.mount();
                    child.comment.after(element.node.node);
                }
            });
            pointers.push(p);
            this.propPointers.set(child.name + "_switches", pointers);
            pointers[pointers.length - 1].onUpdate();
        }
    }
    applyStatePosition(node, child) {
        node.node.appendChild(child.comment);
        const pointers = [];
        for (let i = 0; i < child.pointers.length; i++) {
            pointers.push(new Pointer(child.pointers[i].listID, child.pointers[i].itemID, child.pointers[i].prop, () => {
                const result = child.condition();
                if (result) {
                    if (this.elements.has(child.elementName)) {
                        child.comment.after(this.elements.get(child.elementName).node.node);
                    }
                }
                else {
                }
            }));
            this.propPointers.set(child.name + "_positions", pointers);
            setTimeout(() => {
                pointers[pointers.length - 1].onUpdate();
            }, 0);
        }
    }
    applyElement(node, child) {
        child.mount();
        node.node.appendChild(child.node.node);
    }
    /** @todo Implement statefullness to attributes */
    setupNodeAttributes(node) {
        for (let i = 0; i < node.options.attributes.length; i++) {
            node.node.setAttribute(node.options.attributes[i].name, node.options.attributes[i].value);
        }
    }
    setupFormProbability(node, type, children) {
        if (type === "form") {
            children.push(this.input({
                type: "submit",
                style: "display: none;"
            }));
            if (node.options.onSubmit === null) {
                node.options.onSubmit = () => { };
            }
        }
    }
    setupNodeType(node) {
        if (node.options.type.length !== 0) {
            node.node.setAttribute("type", node.options.type);
        }
    }
    setupNodeText(node) {
        if (node.options.text.length !== 0) {
            node.node.innerText = node.options.text;
        }
        if (node.options.html.length !== 0) {
            node.node.innerHTML = node.options.html;
        }
        if (node.options.stateText !== null) {
            const pointers = [];
            const func = (newVal, oldVal) => {
                if (node.options.stateText.rewritable === false &&
                    newVal === oldVal &&
                    typeof newVal !== "undefined" &&
                    typeof oldVal !== "undefined") {
                    return;
                }
                node.options.stateText.onUpdate(updateFunc, newVal, oldVal);
            };
            const updateFunc = (value) => {
                node.node.innerText = value;
            };
            for (let i = 0; i < node.options.stateText.pointers.length; i++) {
                pointers.push(new Pointer(node.options.stateText.pointers[i].listID, node.options.stateText.pointers[i].itemID, node.options.stateText.pointers[i].prop, func));
            }
            this.propPointers.set(node.options.name + "_text", pointers);
            func();
        }
    }
    setupNodeValue(node) {
        if (node.options.placeholder.length !== 0) {
            if (node.node instanceof HTMLInputElement ||
                node.node instanceof HTMLTextAreaElement) {
                node.node.placeholder = node.options.placeholder;
            }
        }
        if (node.options.value.length !== 0) {
            if (node.node instanceof HTMLInputElement ||
                node.node instanceof HTMLTextAreaElement ||
                node.node instanceof HTMLButtonElement) {
                node.node.value = node.options.value;
            }
        }
        if (node.options.stateValue !== null) {
            const pointers = [];
            const func = (newVal, oldVal) => {
                if (node.options.stateValue.rewritable === false &&
                    newVal === oldVal &&
                    typeof newVal !== "undefined" &&
                    typeof oldVal !== "undefined") {
                    return;
                }
                node.options.stateValue.onUpdate(updateFunc, newVal, oldVal);
            };
            const updateFunc = (value) => {
                if (node.node instanceof HTMLInputElement ||
                    node.node instanceof HTMLTextAreaElement ||
                    node.node instanceof HTMLButtonElement) {
                    node.node.value = value;
                }
            };
            for (let i = 0; i < node.options.stateValue.pointers.length; i++) {
                pointers.push(new Pointer(node.options.stateValue.pointers[i].listID, node.options.stateValue.pointers[i].itemID, node.options.stateValue.pointers[i].prop, func));
            }
            this.propPointers.set(node.options.name + "_value", pointers);
            func();
        }
    }
    setupNodeStyle(node) {
        if (node.options.style.length !== 0) {
            node.node.style = node.options.style;
        }
        if (node.options.stateStyle !== null) {
            const pointers = [];
            const func = (newVal, oldVal) => {
                if (node.options.stateStyle.rewritable === false &&
                    newVal === oldVal &&
                    typeof newVal !== "undefined" &&
                    typeof oldVal !== "undefined") {
                    return;
                }
                node.options.stateStyle.onUpdate(updateFunc, newVal, oldVal);
            };
            const updateFunc = (styles) => {
                for (let i = 0; i < styles.length; i++) {
                    node.node.style[styles[i].key] = styles[i].value;
                }
            };
            for (let i = 0; i < node.options.stateStyle.pointers.length; i++) {
                pointers.push(new Pointer(node.options.stateStyle.pointers[i].listID, node.options.stateStyle.pointers[i].itemID, node.options.stateStyle.pointers[i].prop, func));
            }
            this.propPointers.set(node.options.name + "_style", pointers);
            func();
        }
    }
    setupNodeClass(node) {
        if (node.options.className.length !== 0) {
            node.node.className = node.options.className;
        }
        if (node.options.stateClass !== null) {
            const pointers = [];
            const func = (newVal, oldVal) => {
                if (node.options.stateClass.rewritable === false &&
                    newVal === oldVal &&
                    typeof newVal !== "undefined" &&
                    typeof oldVal !== "undefined") {
                    return;
                }
                node.options.stateClass.onUpdate(updateFunc, newVal, oldVal);
            };
            const updateFunc = (classes) => {
                for (let i = 0; i < classes.length; i++) {
                    if (classes[i].value) {
                        node.node.classList.add(classes[i].key);
                    }
                    else {
                        node.node.classList.remove(classes[i].key);
                    }
                }
            };
            for (let i = 0; i < node.options.stateClass.pointers.length; i++) {
                pointers.push(new Pointer(node.options.stateClass.pointers[i].listID, node.options.stateClass.pointers[i].itemID, node.options.stateClass.pointers[i].prop, func));
            }
            this.propPointers.set(node.options.name + "_class", pointers);
            func();
        }
    }
    // Add statefullnes to this?
    // Or add stateful attributes as a whole?
    setupNodeSize(node) {
        if (node.options.width.length) {
            node.node.setAttribute("width", node.options.width);
        }
        if (node.options.height.length) {
            node.node.setAttribute("height", node.options.height);
        }
    }
    setupNodeEvents(node) {
        if (node.options.onClick !== null) {
            node.node.addEventListener("click", node.options.onClick, false);
        }
        if (node.options.onChange !== null) {
            node.node.addEventListener("change", node.options.onChange, false);
        }
        if (node.options.onFocus !== null) {
            node.node.addEventListener("focus", node.options.onFocus, false);
        }
        if (node.options.onBlur !== null) {
            node.node.addEventListener("blur", node.options.onBlur, false);
        }
        if (node.options.onDoubleClick !== null) {
            node.node.addEventListener("dblclick", node.options.onDoubleClick, false);
        }
        if (node.options.onMouseDown !== null) {
            node.node.addEventListener("mousedown", node.options.onMouseDown, false);
        }
        if (node.options.onMouseUp !== null) {
            node.node.addEventListener("mouseup", node.options.onMouseUp, false);
        }
        if (node.options.onMouseOver !== null) {
            node.node.addEventListener("mouseover", node.options.onMouseOver, false);
        }
        if (node.options.onMouseOut !== null) {
            node.node.addEventListener("mouseout", node.options.onMouseOut, false);
        }
        if (node.options.onMouseEnter !== null) {
            node.node.addEventListener("mouseenter", node.options.onMouseEnter, false);
        }
        if (node.options.onMouseLeave !== null) {
            node.node.addEventListener("mouseleave", node.options.onMouseLeave, false);
        }
        if (node.options.onMouseMove !== null) {
            node.node.addEventListener("mousemove", node.options.onMouseMove, false);
        }
        if (node.options.onRightClick !== null) {
            node.node.addEventListener("contextmenu", node.options.onRightClick, false);
        }
        if (node.options.onKeyUp !== null) {
            node.node.addEventListener("keyup", node.options.onKeyUp, false);
        }
        if (node.options.onKeyDown !== null) {
            node.node.addEventListener("keydown", node.options.onKeyDown, false);
        }
        if (node.options.onScroll !== null) {
            node.node.addEventListener("scroll", node.options.onScroll, false);
        }
        if (node.options.onMouseWheel !== null) {
            node.node.addEventListener("mousewheel", node.options.onMouseWheel, false);
        }
        if (node.options.onResize !== null) {
            node.node.addEventListener("resize", node.options.onResize, false);
        }
        if (node.options.onSubmit !== null) {
            node.options._onSubmit = node.options.onSubmit;
            node.options.onSubmit = ev => {
                ev.preventDefault();
                node.options._onSubmit(ev);
            };
            node.node.addEventListener("submit", node.options.onSubmit, false);
        }
    }
    cleanNodeEvents(node) {
        if (node.options.onClick !== null) {
            node.node.removeEventListener("click", node.options.onClick);
            node.options.onClick = null;
        }
        if (node.options.onChange !== null) {
            node.node.removeEventListener("change", node.options.onChange);
            node.options.onChange = null;
        }
        if (node.options.onFocus !== null) {
            node.node.removeEventListener("focus", node.options.onFocus);
            node.options.onFocus = null;
        }
        if (node.options.onBlur !== null) {
            node.node.removeEventListener("blur", node.options.onBlur);
            node.options.onBlur = null;
        }
        if (node.options.onDoubleClick !== null) {
            node.node.removeEventListener("dblclick", node.options.onDoubleClick);
            node.options.onDoubleClick = null;
        }
        if (node.options.onMouseDown !== null) {
            node.node.removeEventListener("mousedown", node.options.onMouseDown);
            node.options.onMouseDown = null;
        }
        if (node.options.onMouseUp !== null) {
            node.node.removeEventListener("mouseup", node.options.onMouseUp);
            node.options.onMouseUp = null;
        }
        if (node.options.onMouseOver !== null) {
            node.node.removeEventListener("mouseover", node.options.onMouseOver);
            node.options.onMouseOver = null;
        }
        if (node.options.onMouseOut !== null) {
            node.node.removeEventListener("mouseout", node.options.onMouseOut);
            node.options.onMouseOut = null;
        }
        if (node.options.onMouseEnter !== null) {
            node.node.removeEventListener("mouseenter", node.options.onMouseEnter);
            node.options.onMouseEnter = null;
        }
        if (node.options.onMouseLeave !== null) {
            node.node.removeEventListener("mouseleave", node.options.onMouseLeave);
            node.options.onMouseLeave = null;
        }
        if (node.options.onMouseMove !== null) {
            node.node.removeEventListener("mousemove", node.options.onMouseMove);
            node.options.onMouseMove = null;
        }
        if (node.options.onRightClick !== null) {
            node.node.removeEventListener("contextmenu", node.options.onRightClick);
            node.options.onRightClick = null;
        }
        if (node.options.onKeyUp !== null) {
            node.node.removeEventListener("keyup", node.options.onKeyUp);
            node.options.onKeyUp = null;
        }
        if (node.options.onKeyDown !== null) {
            node.node.removeEventListener("keydown", node.options.onKeyDown);
            node.options.onKeyDown = null;
        }
        if (node.options.onScroll !== null) {
            node.node.removeEventListener("scroll", node.options.onScroll);
            node.options.onScroll = null;
        }
        if (node.options.onMouseWheel !== null) {
            node.node.removeEventListener("mousewheel", node.options.onMouseWheel);
            node.options.onMouseWheel = null;
        }
        if (node.options.onResize !== null) {
            node.node.removeEventListener("resize", node.options.onResize);
            node.options.onResize = null;
        }
        if (node.options.onSubmit !== null) {
            node.node.removeEventListener("submit", node.options.onSubmit);
            node.options.onSubmit = null;
            node.options._onSubmit = null;
        }
    }
    // Batching of updates?
    state(pointers, onUpdate, rewritable = false) {
        if (pointers instanceof Pointer) {
            return new NodeState([pointers], onUpdate, rewritable);
        }
        return new NodeState(pointers, onUpdate, rewritable);
    }
    value(key, value) {
        return new NodeValue(key, value);
    }
    stateValue(pointer) {
        if (pointer instanceof Pointer) {
            pointer = [pointer];
        }
        return this.state(pointer, update => update(pointer[0].value));
    }
    stateMount(pointers, onUpdate, elementReference, data, name = "") {
        return new NodeComment(document.createComment(""), pointers instanceof Pointer ? [pointers] : pointers, onUpdate, elementReference, data, name);
    }
    stateSwitch(pointers, onUpdate, data = () => null, name = "", refreshable = false) {
        return new NodeSwitch(document.createComment(""), pointers instanceof Pointer ? [pointers] : pointers, onUpdate, onUpdate, data, name, refreshable);
    }
    statePosition(pointers, elementName, condition, name = "") {
        return new NodePosition(document.createComment(""), pointers, elementName, condition, name);
    }
    list(name, listNode, listID, itemClassReference) {
        return this.element(ListUI, {
            listNode: listNode,
            listID: listID,
            itemClassReference: itemClassReference
        }, name);
    }
    removeItem(listID, itemID) {
        const list = new ListPointer("remove", listID);
        list.remove(itemID);
        list.unmount();
    }
    keyDownUpdate(update = () => { }) {
        setTimeout(update, 0);
    }
    // Some TypeScript comforting
    getNode(name) {
        return this.nodes.get(name).node;
    }
    // Some TypeScript comforting
    getInput(name) {
        return this.getNode(name);
    }
    getDisplay() {
        return this.display();
    }
    externalMounted() {
        this.mounted();
    }
    externalBeforeMount() {
        this.beforeMount();
    }
    externalUnmount(unmount) {
        this.onUnmount(unmount);
    }
    externalUnmountSync() {
        this.onUnmountSync();
    }
    /**
     * @abstract Method is abstract, implemented in extensions of UI class
     * @returns {NodeUI}
     * */
    display() {
        return this.div({}, []);
    }
    /**
     * @abstract Method is abstract, implemented in extensions of UI class
     * */
    mounted() { }
    /**
     * @abstract Method is abstract, implemented in extensions of UI class
     * */
    beforeMount() { }
    /**
     * @abstract Method is abstract, implemented in extensions of UI class
     * */
    onUnmount(unmount = () => { }) {
        unmount();
    }
}
class Transform extends Ground {
    constructor(node) {
        super();
        this.matrix = [
            [0, 0, 0],
            [0, 0, 0],
            [1, 1],
            [0, 0],
            [
                0, 0, 0,
                0, 0, 0,
                0, 0, 0 // 3D Offset Correction
            ],
            [0, 0],
            [0, 0],
            [0, 0],
            [0, 0, 0, 0],
            [0],
        ];
        this.node = node;
    }
    translateX(x = 0, apply = true) {
        this.matrix[0][0] = x;
        if (apply) {
            this.apply();
        }
    }
    translateY(y = 0, apply = true) {
        this.matrix[0][1] = y;
        if (apply) {
            this.apply();
        }
    }
    translateZ(z = 0, apply = true) {
        this.matrix[0][2] = z;
        if (apply) {
            this.apply();
        }
    }
    translateXY(x = 0, y = 0, apply = true) {
        this.matrix[0][0] = x;
        this.matrix[0][1] = y;
        if (apply) {
            this.apply();
        }
    }
    translate(x = 0, y = 0, z = 0, apply = true) {
        this.matrix[0][0] = x;
        this.matrix[0][1] = y;
        this.matrix[0][2] = z;
        if (apply) {
            this.apply();
        }
    }
    rotateX(x = 0, apply = true) {
        this.matrix[1][0] = x;
        if (apply) {
            this.apply();
        }
    }
    rotateY(y = 0, apply = true) {
        this.matrix[1][1] = y;
        if (apply) {
            this.apply();
        }
    }
    rotateZ(z = 0, apply = true) {
        this.matrix[1][2] = z;
        if (apply) {
            this.apply();
        }
    }
    rotateXY(x = 0, y = 0, apply = true) {
        this.matrix[1][0] = x;
        this.matrix[1][1] = y;
        if (apply) {
            this.apply();
        }
    }
    rotate(x = 0, y = 0, z = 0, apply = true) {
        this.matrix[1][0] = x;
        this.matrix[1][1] = y;
        this.matrix[1][2] = z;
        if (apply) {
            this.apply();
        }
    }
    scaleX(x = 0, apply = true) {
        this.matrix[2][0] = x;
        if (apply) {
            this.apply();
        }
    }
    scaleY(y = 0, apply = true) {
        this.matrix[2][1] = y;
        if (apply) {
            this.apply();
        }
    }
    scale(x = 0, y = 0, apply = true) {
        this.matrix[2][0] = x;
        this.matrix[2][1] = y;
        if (apply) {
            this.apply();
        }
    }
    skewX(x = 0, apply = true) {
        this.matrix[3][0] = x;
        if (apply) {
            this.apply();
        }
    }
    skewY(y = 0, apply = true) {
        this.matrix[3][1] = y;
        if (apply) {
            this.apply();
        }
    }
    skew(x = 0, y = 0, apply = true) {
        this.matrix[3][0] = x;
        this.matrix[3][1] = y;
        if (apply) {
            this.apply();
        }
    }
    updateMatrix(trX = 0, trY = 0, trZ = 0, roX = 0, roY = 0, roZ = 0, scX = 0, scY = 0, skX = 0, skY = 0, apply = true) {
        this.translate(trX, trY, trZ, false);
        this.rotate(roX, roY, roZ, false);
        this.scale(scX, scY, false);
        this.skew(skX, skY, false);
        if (apply) {
            this.apply();
        }
    }
    sizeX(x = 0, px = 0, ox = 0, apply = true) {
        this.matrix[4][0] = x;
        this.matrix[4][3] = px;
        this.matrix[4][6] = ox;
        if (apply) {
            this.applySize();
        }
    }
    sizeY(y = 0, py = 0, oy = 0, apply = true) {
        this.matrix[4][1] = y;
        this.matrix[4][4] = py;
        this.matrix[4][7] = oy;
        if (apply) {
            this.applySize();
        }
    }
    sizeZ(z = 0, pz = 0, oz = 0, apply = true) {
        this.matrix[4][2] = z;
        this.matrix[4][5] = pz;
        this.matrix[4][8] = oz;
        if (apply) {
            this.applySize();
        }
    }
    sizeXY(x = 0, y = 0, px = 0, py = 0, ox = 0, oy = 0, apply = true) {
        this.matrix[4][0] = x;
        this.matrix[4][1] = y;
        this.matrix[4][3] = px;
        this.matrix[4][4] = py;
        this.matrix[4][6] = ox;
        this.matrix[4][7] = oy;
        if (apply) {
            this.applySize();
        }
    }
    size(x = 0, y = 0, z = 0, px = 0, py = 0, pz = 0, ox = 0, oy = 0, oz = 0, apply = true) {
        this.matrix[4][0] = x;
        this.matrix[4][1] = y;
        this.matrix[4][2] = z;
        this.matrix[4][3] = px;
        this.matrix[4][4] = py;
        this.matrix[4][5] = pz;
        this.matrix[4][6] = ox;
        this.matrix[4][7] = oy;
        this.matrix[4][8] = oz;
        if (apply) {
            this.applySize();
        }
    }
    apply() {
        this.node.node.style.transform =
            "translate3d(" + this.matrix[0][0] + "px, " + this.matrix[0][1] + "px, " + this.matrix[0][2] + "px) " +
                "rotateX(" + this.matrix[1][0] + "deg) " +
                "rotateY(" + this.matrix[1][1] + "deg) " +
                "rotateZ(" + this.matrix[1][2] + "deg) " +
                "scaleX(" + this.matrix[2][0] + ") " +
                "scaleY(" + this.matrix[2][1] + ") " +
                "skewX(" + this.matrix[3][0] + "deg) " +
                "skewY(" + this.matrix[3][1] + "deg)";
    }
    applySize() {
        if (this.matrix[4][3] === 0) {
            this.node.node.style.width = this.matrix[4][0] + "px";
        }
        else {
            if (this.matrix[4][6] === 0) {
                this.node.node.style.width = this.matrix[4][0] + "%";
            }
            else {
                this.node.node.style.width = "calc(" + this.matrix[4][0] + "%" + " + " + this.matrix[4][6] + "px)";
            }
        }
        if (this.matrix[4][4] === 0) {
            this.node.node.style.height = this.matrix[4][1] + "px";
        }
        else {
            if (this.matrix[4][7] === 0) {
                this.node.node.style.height = this.matrix[4][1] + "%";
            }
            else {
                this.node.node.style.height = "calc(" + this.matrix[4][1] + "%" + " + " + this.matrix[4][7] + "px)";
            }
        }
    }
}
class NodeUI extends Ground {
    constructor(node = new HTMLElement(), options) {
        super();
        this.transform = new Transform(this);
        this.rect = null;
        this.node = node;
        this.options = options;
    }
}
class NodeState extends Ground {
    constructor(pointers, onUpdate, rewritable = false) {
        super();
        this.pointers = pointers;
        this.onUpdate = onUpdate;
        this.rewritable = rewritable;
    }
}
class NodeValue extends Ground {
    constructor(key, value) {
        super();
        this.key = key;
        this.value = value;
    }
}
class NodeComment extends Ground {
    constructor(comment, pointers, onUpdate, elementReference, data, name) {
        super();
        this.comment = comment;
        this.pointers = pointers;
        this.onUpdate = onUpdate;
        this.elementReference = elementReference;
        this.data = data;
        this.name = name;
    }
}
class NodePosition extends Ground {
    constructor(comment, pointers, elementName = "", condition, name) {
        super();
        this.comment = comment;
        this.pointers = pointers;
        this.elementName = elementName;
        this.condition = condition;
        this.name = name.length === 0 ? "position_" + performance.now() + "_" + this.random(1, 99999) : name;
    }
}
class NodeSwitch extends Ground {
    constructor(comment, pointers, onUpdate, elementReference, data, name, refreshable) {
        super();
        this.comment = comment;
        this.pointers = pointers;
        this.onUpdate = onUpdate;
        this.elementReference = elementReference;
        this.data = data;
        this.name = name;
        this.refreshable = refreshable;
    }
}
class NodeElement extends Ground {
    constructor(ref = UI, data = {}, name = "") {
        super();
        this.ref = ref;
        this.data = data;
        this.name = name;
    }
}
class ListUI extends UI {
    constructor() {
        super(...arguments);
        this.listPointer = null;
        this.sorting = false;
    }
    mounted() {
        this.listPointer = new ListPointer((performance.now() + "_" + this.random(1, 99999)), this.options.listID);
        this.itemClassReference = this.options.itemClassReference;
        if (this.listPointer.mounted === false) {
            this.listPointer.mount();
        }
        if (this.listPointer.mounted === false) {
            // console.warn("Failed to mount listPointer[\"" + this.listPointer.listID + "\"]");
            // return;
        }
        this.listPointer.onAdd = (id = "") => {
            const element = this.element(this.itemClassReference, this.listPointer.item(id).data, id);
            element.mount();
            this.node.node.appendChild(element.node.node);
        };
        this.listPointer.onRemove = (id = "") => {
            if (this.elements.has(id) === false) {
                console.warn("Trying to remove a nonexisting element.");
                return;
            }
            const element = this.elements.get(id);
            element.unmount();
        };
        this.listPointer.onSort = (elementID1 = "", elementID2 = "", type = 0) => {
            if (this.sorting) {
                return;
            }
            if (type === 0) {
                this.elements.get(elementID1).node.node.before(this.elements.get(elementID2).node.node);
            }
            else {
                this.elements.get(elementID1).node.node.after(this.elements.get(elementID2).node.node);
            }
        };
        if (this.listPointer.mounted) {
            this.mountList();
        }
        else {
            this.listPointer.onWaitedMount = this.mountList.bind(this);
        }
    }
    mountList() {
        this.listPointer.list.items.forEach((item, id) => {
            const element = this.element(this.itemClassReference, item.data, id);
            element.mount();
            this.node.node.appendChild(element.node.node);
        });
    }
    onUnmount(unmount = () => { }) {
        this.listPointer.unmount();
        unmount();
    }
    display() {
        return this.options.listNode;
    }
}
// var variable1 = false;
// var variable2 = false;
// function MyAction(param1, param2, param3) {
//     variable1 = param3;
//     if (variable1 == true) {
//         anotherFunction1(param2);
//         variable2 = param2;
//         if (variable2 == true) {
//             anotherFunction2(param1);
//         }
//     }
// }
// function anotherFunction1(param) {
//     /* Logic */
// }
// function anotherFunction2(param) {
//     /* Logic */
// }
// MathematicalPatternSet [
//     "addition",
//     "subtraction",
//     "multiplication",
//     "division",
//     "delta"
// ]
// ConditionTypes [
//     "compareEqual",
//     "compareNotEqual",
//     "compareGreaterThan",
//     "compareLesserThan",
//     "compareGreaterOrEqual",
//     "compareLesserOrEqual"
// ]
// ArchitecturalBasis [
//     "make1FunctionOutofTwoFunction",
//     "make1FunctionOutofTwoFunction",
//     "make1FunctionOutofTwoFunction",
//     "make1FunctionOutofTwoFunction",
//     "make1FunctionOutofTwoFunction",
//     "make1FunctionOutofTwoFunction",
// ]
// [
//     memorized outcome result 1 = something
//     memorized outcome result 1 = something
//     memorized outcome result 1 = something
//     memorized outcome result 1 = something
//     memorized outcome result 1 = something
// ]
// function calculator(num1, num2, operator) {
//     if (operator === "+") {
//         return num1 + num2;
//     }
//     if (operator === "-") {
//         return num1 - num2;
//     }
//     if (operator === "*") {
//         return num1 * num2;
//     }
//     if (operator === "/") {
//         return num1 / num2;
//     }
//     if (operator === "%") {
//         return num1 % num2;
//     }
//     if (operator === "&") {
//         return num1 % num2;
//     }
// }
// calculator(5, 10, "+"); // 15
// context1
// context2
// context3
// test [
//     before this state change happens
//         to these contexts
//     this state change needs to happen
//         to these contexts
// ]
// State change [
//     context1+
//     context2-
//     context3+
// ]
// Creation [
//     State [
//         context4-
//         context5+
//         context6-
//     ]
// ]
// const arr = []; // basket
// function makeAOrderedBasket(numbers) {
//     for (let i = 0; i < numbers; i++) {
//         arr.push(i);
//     }
// }
// [0 /* signed number on it with pencil */, 1, 2, 3, 4, 5, 6, 7, 8, 9 /* eggs numbered, ordered ASC */] = makeAOrderedBasket(10);
function node(options) {
}
function style(options) {
}
function background(options) {
}
class InteractiveLibrary extends Ground {
    constructor(interactive) {
        super();
        this.enabled = false;
        this.interactive = interactive;
    }
}
class InteractiveUI extends Ground {
    constructor(ui) {
        super();
        this.ui = ui;
        this.move = new Move(this);
        this.drag = new Drag(this);
        this.drop = new Drop(this);
        this.resize = new Resize(this);
        this.rotate = new Rotate(this);
        this.skew = new Skew(this);
        this.sort = new Sort(this);
        this.padding = new Padding(this);
    }
}
class MoveMount {
    constructor(id, node, pointer, down, move, up) {
        this.id = id;
        this.node = node;
        this.pointer = pointer;
        if (down) {
            this.down = down;
        }
        if (move) {
            this.move = move;
        }
        if (up) {
            this.up = up;
        }
        this.mount();
    }
    mount() {
        this.node.node.addEventListener("mousedown", this.down, false);
    }
    unmount() {
        this.node.node.removeEventListener("mousedown", this.down);
    }
}
let moving = false;
class Move extends InteractiveLibrary {
    constructor() {
        super(...arguments);
        this.startPos = new Pos();
        this.startTransform = new Pos();
        this.muted = false;
        this.paused = false;
    }
    _down(node, pointer, id, ev) {
        if (ev.button !== 0) {
            return;
        }
        if (this.paused === true) {
            return;
        }
        moving = true;
        document.body.style.userSelect = "none";
        this.startPos.x = ev.pageX;
        this.startPos.y = ev.pageY;
        this.startTransform.x = node.transform.matrix[0][0];
        this.startTransform.y = node.transform.matrix[0][1];
        window.addEventListener("mousemove", this.mount.move, false);
        window.addEventListener("mouseup", this.mount.up, false);
        if (this.onStart) {
            this.onStart(node, pointer, id, ev);
        }
    }
    _move(node, pointer, id, ev) {
        if (this.muted === false) {
            pointer.value.x = Math.round(ev.pageX - (this.startPos.x - this.startTransform.x));
            pointer.value.y = Math.round(ev.pageY - (this.startPos.y - this.startTransform.y));
            pointer.value = pointer.value;
        }
        if (this.onMove) {
            this.onMove(node, pointer, id, ev);
        }
    }
    _up(node, pointer, id, ev) {
        if (this.mount === null) {
            // Cancelled beforehand
            return;
        }
        window.removeEventListener("mousemove", this.mount.move);
        window.removeEventListener("mouseup", this.mount.up);
        moving = false;
        document.body.style.userSelect = null;
        if (this.onEnd) {
            this.onEnd(node, pointer, id, ev);
        }
    }
    _pointerUpdate(pointer, node) {
        node.transform.translate(pointer.value.x, pointer.value.y);
    }
    enable(pointer, nodeName = "main") {
        if (this.enabled) {
            return;
        }
        this.nodeName = nodeName;
        const node = this.interactive.ui.nodes.get(nodeName);
        if (pointer.listID.length !== 0) {
            this.interactive.ui.node.transform.translate(pointer.value.x, pointer.value.y, pointer.value.z);
        }
        this.mount = new MoveMount(nodeName, node, pointer, (ev) => this._down(this.interactive.ui.node, pointer, nodeName, ev), (ev) => this._move(this.interactive.ui.node, pointer, nodeName, ev), (ev) => this._up(this.interactive.ui.node, pointer, nodeName, ev));
        this.interactive.ui.propPointers.set(nodeName + "_move", [
            new Pointer(pointer.listID, pointer.itemID, pointer.prop, this._pointerUpdate.bind(this, pointer, this.interactive.ui.node))
        ]);
        this.enabled = true;
    }
    disable() {
        this.interactive.ui.propPointers.get(this.nodeName + "_move")[0].unmount();
        this.interactive.ui.propPointers.delete(this.nodeName + "_move");
        this.enabled = false;
        window.removeEventListener("mousemove", this.mount.move);
        window.removeEventListener("mouseup", this.mount.up);
        this.mount.unmount();
        this.mount = null;
    }
    unmount() {
        if (this.enabled === false) {
            return;
        }
        this.disable();
    }
}
class Drag extends InteractiveLibrary {
    constructor() {
        super(...arguments);
        this.placeholder = null;
        this.placeholderColor = "transparent";
        this.floating = "none";
        this.delay = 1;
        this.startAttempted = false;
        this.startPos = new Pos();
        this._onDragStartBinded = null;
        this.windowDelayUp = () => {
            this.startPos.x = 0;
            this.startPos.y = 0;
            this.startAttempted = false;
            window.removeEventListener("mousemove", this.windowDelayMove);
            window.removeEventListener("mouseup", this.windowDelayUp);
            this.windowDelayMove = null;
        };
    }
    _onDragStart(nodeName, ev) {
        if (ev.button !== 0) {
            return;
        }
        if (this.startAttempted === false) {
            this.startPos.x = ev.pageX;
            this.startPos.y = ev.pageY;
            this.startAttempted = true;
            this.windowDelayMove = (ev2) => {
                if (Math.abs(this.startPos.x - ev2.pageX) > 1 ||
                    Math.abs(this.startPos.y - ev2.pageY) > 1) {
                    this.windowDelayUp();
                    this.interactive.move.paused = false;
                    this.interactive.move.onStart = this._onStart.bind(this);
                    this.interactive.move.onEnd = this._onEnd.bind(this);
                    this.interactive.move.enable(View.dragPos, nodeName);
                    this.interactive.move._down(this.interactive.ui.node, View.dragPos, nodeName, ev2);
                }
            };
            window.addEventListener("mousemove", this.windowDelayMove, false);
            window.addEventListener("mouseup", this.windowDelayUp, false);
        }
        ev.preventDefault();
        ev.stopPropagation();
        ev.stopImmediatePropagation();
    }
    _onDragMove() {
    }
    _onStart() {
        if (this.onBeforeStart) {
            this.onBeforeStart();
        }
        this.mountPlaceholder();
    }
    mountPlaceholder() {
        this.interactive.ui.node.node.style.pointerEvents = "none";
        const rect = this.interactive.ui.node.node.getBoundingClientRect();
        this.placeholder = document.createElement("div");
        let margin = this.interactive.ui.node.node.style.margin;
        if (margin === "") {
            margin = window.getComputedStyle(this.interactive.ui.node.node).margin;
        }
        if (margin === "") {
            margin = "0px";
        }
        const marginInt = margin === "" ? 0 : parseInt(margin.replace("px", "").replace("%", ""));
        if (this.floating === "left") {
            this.placeholder.style.float = "left";
        }
        if (this.floating === "right") {
            this.placeholder.style.float = "right";
        }
        if (this.floating === "inline-block") {
            this.placeholder.style.display = "inline-block";
        }
        this.placeholder.style.margin = margin;
        this.placeholder.style.backgroundColor = this.placeholderColor;
        this.placeholder.style.borderRadius = this.interactive.ui.node.node.style.borderRadius;
        this.placeholder.style.width = rect.width + "px";
        this.placeholder.style.height = rect.height + "px";
        if (this.placeholder.style.borderRadius === null || this.placeholder.style.borderRadius === "") {
            this.placeholder.style.borderRadius = window.getComputedStyle(this.interactive.ui.node.node).borderRadius;
        }
        this.interactive.ui.node.node.style.position = "absolute";
        this.interactive.ui.node.node.style.left = (rect.left - marginInt) + "px";
        this.interactive.ui.node.node.style.top = (rect.top - marginInt) + "px";
        this.interactive.ui.node.node.after(this.placeholder);
        document.body.appendChild(this.interactive.ui.node.node);
        View.dragging.value = true;
        View.dragElement.value = this.interactive.ui;
        View.dragPlaceholder.value = this.placeholder;
        if (this.onStart) {
            this.onStart();
        }
    }
    _onEnd() {
        this.startAttempted = false;
        this.interactive.move.paused = true;
        View.dragging.value = false;
        View.dragElement.value = null;
        View.dragPlaceholder.value = null;
        this.interactive.ui.node.node.style.position = null;
        this.interactive.ui.node.node.style.left = null;
        this.interactive.ui.node.node.style.top = null;
        this.placeholder.after(this.interactive.ui.node.node);
        this.placeholder.parentNode.removeChild(this.placeholder);
        this.placeholder = null;
        this.interactive.move.mount.pointer.value.x = 0;
        this.interactive.move.mount.pointer.value.y = 0;
        this.interactive.move.mount.pointer.update();
        this.interactive.move.unmount();
        if (this.onEnd) {
            this.onEnd();
        }
        this.interactive.ui.node.node.style.pointerEvents = null;
    }
    enable(nodeName = "main", placeholderColor = "transparent", floating = "none") {
        if (this.enabled) {
            return;
        }
        this.nodeName = nodeName;
        this.enabled = true;
        this.placeholderColor = placeholderColor;
        this.floating = floating;
        this._onDragStartBinded = this._onDragStart.bind(this, this.nodeName);
        this.interactive.ui.nodes.get(this.nodeName).node.addEventListener("mousedown", this._onDragStartBinded, false);
    }
    disable() {
        this.interactive.ui.nodes.get(this.nodeName).node.removeEventListener("mousedown", this._onDragStartBinded);
        this.interactive.move.unmount();
        this.enabled = false;
    }
    unmount() {
        if (this.enabled === false) {
            return;
        }
        this.disable();
    }
}
/**
 * Features to be applied
 * ~1. Can drop item's data
 * 2. Can be combined with sort to automatically adjust where to appear the new item
 * 3. When combined with sort to have oninsert method which handles the data insertion and it's processing
 *
*/
class Drop extends InteractiveLibrary {
    constructor() {
        super(...arguments);
        this._accept = [];
        this.combineWithSort = false;
        this.sortedInsert = false;
    }
    accept(/** @type {Array<<Struct>>} */ structSet = []) {
        this._accept = structSet;
    }
    _onDragEnter() {
        if (View.dragging.value === false) {
            return;
        }
        if (this.validDrag() === false) {
            return;
        }
        if (this.sortedInsert) {
            // Sorting gets started here
        }
        if (this.onDragEnter) {
            this.onDragEnter(View.dragElement.value.options);
        }
    }
    _onDragLeave() {
        if (View.dragging.value === false) {
            return;
        }
        if (this.validDrag() === false) {
            return;
        }
        if (this.sortedInsert) {
            // Sorting gets ended without insert here
        }
        if (this.onDragLeave) {
            this.onDragLeave(View.dragElement.value.options);
        }
    }
    _onDrop(ev) {
        if (View.dragging.value === false) {
            return;
        }
        if (this.validDrag() === false) {
            return;
        }
        if (this.sortedInsert) {
            // Sorting gets ended with insert here
            const newItem = this.onInsert(View.dragElement.value.options.data);
            const ui = this.interactive.ui.elements.get(this.sortElementName);
            if (ui instanceof ListUI) {
                const pointer = ui.listPointer;
                if (newItem !== false && newItem !== true) {
                    if (newItem.length === 1) {
                        newItem[1] = "id_" + performance.now();
                    }
                    pointer.add(newItem[0], newItem[1]);
                }
            }
        }
        if (this.onDrop) {
            this.onDrop(View.dragElement.value.options, ev);
        }
    }
    validDrag() {
        let found = false;
        const className = View.dragElement.value.options.constructor.name;
        for (let i = 0; i < this._accept.length; i++) {
            if (this._accept[i].name === className) {
                found = true;
                break;
            }
        }
        if (found === false) {
            return false;
        }
        return true;
    }
    enable(nodeName = "", structSet, onDrop, sortElementName = "", sortedInsert = false) {
        if (this.enabled) {
            return;
        }
        this.enabled = true;
        this.node = this.interactive.ui.nodes.get(nodeName);
        this.onDrop = onDrop;
        this.sortElementName = sortElementName;
        this.combineWithSort = this.sortElementName.length === 0 ? false : true;
        this.sortedInsert = sortedInsert;
        this.accept(structSet);
        this._onDropBinded = this._onDrop.bind(this);
        this._onDragEnterBinded = this._onDragEnter.bind(this);
        this._onDragLeaveBinded = this._onDragLeave.bind(this);
        this.node.node.addEventListener("mouseup", this._onDropBinded, false);
        this.node.node.addEventListener("mouseenter", this._onDragEnterBinded, false);
        this.node.node.addEventListener("mouseleave", this._onDragLeaveBinded, false);
    }
    disable() {
        if (this.enabled === false) {
            return;
        }
        this.node.node.removeEventListener("mouseenter", this._onDragEnterBinded);
        this.node.node.removeEventListener("mouseleave", this._onDragLeaveBinded);
        this.node.node.removeEventListener("mouseup", this._onDropBinded);
        this.node = null;
        this.enabled = false;
    }
    unmount() {
        this.disable();
    }
}
class Resize extends InteractiveLibrary {
    constructor() {
        super(...arguments);
        this.left = true;
        this.top = true;
        this.right = true;
        this.bottom = true;
        this.topLeft = true;
        this.topRight = true;
        this.bottomLeft = true;
        this.bottomRight = true;
        this.movement = true;
        this.noMovement = false;
        this.percentOffsetCorrection = false;
        this.onResizeStart = () => { };
        this.onResizeMove = () => { };
        this.onResizeEnd = () => { };
    }
    _pointerUpdate(sizePointer, positionPointer, node, manual = false) {
        if ((this.interactive.ui.interactive.move.enabled === false || manual === true) && this.noMovement === false) {
            node.transform.translate(positionPointer.value.x, positionPointer.value.y);
        }
        if (sizePointer.value.percentX) {
            node.transform.sizeX(sizePointer.value.offsetX, 1, sizePointer.value.x, false);
        }
        else {
            node.transform.sizeX(sizePointer.value.x, 0, 0, false);
        }
        if (sizePointer.value.percentY) {
            node.transform.sizeY(sizePointer.value.offsetY, 1, sizePointer.value.y, false);
        }
        else {
            node.transform.sizeY(sizePointer.value.y, 0, 0, false);
        }
        node.transform.applySize();
    }
    enable(sizePointer, posPointer) {
        const sizeFunc = this._pointerUpdate.bind(this, sizePointer, posPointer, this.interactive.ui.node);
        const posFunc = this._pointerUpdate.bind(this, sizePointer, posPointer, this.interactive.ui.node);
        this.interactive.ui.propPointers.set("resize_size", [
            new Pointer(sizePointer.listID, sizePointer.itemID, sizePointer.prop, sizeFunc)
        ]);
        this.interactive.ui.propPointers.set("resize_pos", [
            new Pointer(posPointer.listID, posPointer.itemID, posPointer.prop, posFunc)
        ]);
        this.interactive.ui.addElement("main", ResizerLeft, {
            sizePointer: sizePointer,
            posPointer: posPointer,
            active: this.left,
            movement: this.movement,
            start: this.onResizeStart,
            move: this.onResizeMove,
            end: this.onResizeEnd
        });
        this.interactive.ui.addElement("main", ResizerTop, {
            sizePointer: sizePointer,
            posPointer: posPointer,
            active: this.top,
            movement: this.movement,
            start: this.onResizeStart,
            move: this.onResizeMove,
            end: this.onResizeEnd
        });
        this.interactive.ui.addElement("main", ResizerRight, {
            sizePointer: sizePointer,
            posPointer: posPointer,
            active: this.right,
            movement: this.movement,
            start: this.onResizeStart,
            move: this.onResizeMove,
            end: this.onResizeEnd
        });
        this.interactive.ui.addElement("main", ResizerBottom, {
            sizePointer: sizePointer,
            posPointer: posPointer,
            active: this.bottom,
            movement: this.movement,
            start: this.onResizeStart,
            move: this.onResizeMove,
            end: this.onResizeEnd
        });
        this.interactive.ui.addElement("main", ResizerTopLeft, {
            sizePointer: sizePointer,
            posPointer: posPointer,
            active: this.topLeft,
            movement: this.movement,
            start: this.onResizeStart,
            move: this.onResizeMove,
            end: this.onResizeEnd
        });
        this.interactive.ui.addElement("main", ResizerTopRight, {
            sizePointer: sizePointer,
            posPointer: posPointer,
            active: this.topRight,
            movement: this.movement,
            start: this.onResizeStart,
            move: this.onResizeMove,
            end: this.onResizeEnd
        });
        this.interactive.ui.addElement("main", ResizerBottomRight, {
            sizePointer: sizePointer,
            posPointer: posPointer,
            active: this.bottomRight,
            movement: this.movement,
            start: this.onResizeStart,
            move: this.onResizeMove,
            end: this.onResizeEnd
        });
        this.interactive.ui.addElement("main", ResizerBottomLeft, {
            sizePointer: sizePointer,
            posPointer: posPointer,
            active: this.bottomLeft,
            movement: this.movement,
            start: this.onResizeStart,
            move: this.onResizeMove,
            end: this.onResizeEnd
        });
        sizeFunc(true);
        posFunc(true);
        this.enabled = true;
    }
    disable() {
        this.enabled = false;
        this.interactive.ui.propPointers.get("resize_size")[0].unmount();
        this.interactive.ui.propPointers.delete("resize_size");
        this.interactive.ui.propPointers.get("resize_pos")[0].unmount();
        this.interactive.ui.propPointers.delete("resize_pos");
        this.interactive.ui.elements.get("ResizerLeft").unmount();
        this.interactive.ui.elements.get("ResizerTop").unmount();
        this.interactive.ui.elements.get("ResizerRight").unmount();
        this.interactive.ui.elements.get("ResizerBottom").unmount();
        this.interactive.ui.elements.get("ResizerTopLeft").unmount();
        this.interactive.ui.elements.get("ResizerTopRight").unmount();
        this.interactive.ui.elements.get("ResizerBottomRight").unmount();
        this.interactive.ui.elements.get("ResizerBottomLeft").unmount();
    }
    unmount() {
        if (this.enabled === false) {
            return;
        }
        this.disable();
    }
}
class Resizer extends UI {
    constructor() {
        super(...arguments);
        this.startPos = new Pos();
        this.startStatePos = new Pos();
        this.startSize = new Size();
        this.startStateSize = new Size();
    }
    beforeMount() {
        this.sizePointer = this.options.sizePointer;
        this.posPointer = this.options.posPointer;
    }
    mounted() {
        this.interactive.move.muted = true;
    }
    display() {
        if (this.options.active === false) {
            return this.div({
                style: "display: none;"
            });
        }
        return this.div();
    }
}
class ResizerLeft extends Resizer {
    mounted() {
        super.mounted();
        if (this.options.movement === false) {
            return;
        }
        this.interactive.move.enable(new Pointer(), "main");
        this.interactive.move.onStart = this.onStart.bind(this);
        this.interactive.move.onMove = this.onResize.bind(this);
        this.interactive.move.onEnd = this.onEnd.bind(this);
    }
    onStart(node, pointer, id, ev, applyResizer = true) {
        if (applyResizer) {
            document.body.style.cursor = "ew-resize !important";
        }
        this.startPos.x = ev.pageX;
        this.startPos.y = ev.pageY;
        this.startStatePos.x = this.posPointer.value.x;
        this.startStateSize.x = this.sizePointer.value.x;
        this.options.start();
    }
    onResize(node, pointer, id, ev) {
        this.posPointer.value.x = this.startStatePos.x + (ev.pageX - this.startPos.x);
        this.sizePointer.value.x = this.startStateSize.x - (ev.pageX - this.startPos.x);
        this.sizePointer.update();
        this.posPointer.update();
        this.options.move();
    }
    onEnd(node, pointer, id, ev) {
        document.body.style.cursor = null;
        this.options.end();
    }
}
class ResizerTop extends Resizer {
    mounted() {
        super.mounted();
        if (this.options.movement === false) {
            return;
        }
        this.interactive.move.enable(new Pointer(), "main");
        this.interactive.move.onStart = this.onStart.bind(this);
        this.interactive.move.onMove = this.onResize.bind(this);
        this.interactive.move.onEnd = this.onEnd.bind(this);
    }
    onStart(node, pointer, id, ev, applyResizer = true) {
        if (applyResizer) {
            document.body.style.cursor = "ns-resize !important";
        }
        this.startPos.x = ev.pageX;
        this.startPos.y = ev.pageY;
        this.startStatePos.y = this.posPointer.value.y;
        this.startStateSize.y = this.sizePointer.value.y;
        this.options.start();
    }
    onResize(node, pointer, id, ev) {
        this.posPointer.value.y = this.startStatePos.y + (ev.pageY - this.startPos.y);
        this.sizePointer.value.y = this.startStateSize.y - (ev.pageY - this.startPos.y);
        this.sizePointer.update();
        this.posPointer.update();
        this.options.move();
    }
    onEnd(node, pointer, id, ev) {
        document.body.style.cursor = null;
        this.options.end();
    }
}
class ResizerRight extends Resizer {
    mounted() {
        super.mounted();
        if (this.options.movement === false) {
            return;
        }
        this.interactive.move.enable(new Pointer(), "main");
        this.interactive.move.onStart = this.onStart.bind(this);
        this.interactive.move.onMove = this.onResize.bind(this);
        this.interactive.move.onEnd = this.onEnd.bind(this);
    }
    onStart(node, pointer, id, ev, applyResizer = true) {
        if (applyResizer) {
            document.body.style.cursor = "ew-resize !important";
        }
        this.startPos.x = ev.pageX;
        this.startPos.y = ev.pageY;
        this.startStateSize.x = this.sizePointer.value.x;
        this.options.start();
    }
    onResize(node, pointer, id, ev) {
        this.sizePointer.value.x = this.startStateSize.x + (ev.pageX - this.startPos.x);
        this.sizePointer.update();
        this.options.move();
    }
    onEnd(node, pointer, id, ev) {
        document.body.style.cursor = null;
        this.options.end();
    }
}
class ResizerBottom extends Resizer {
    mounted() {
        super.mounted();
        if (this.options.movement === false) {
            return;
        }
        this.interactive.move.enable(new Pointer(), "main");
        this.interactive.move.onStart = this.onStart.bind(this);
        this.interactive.move.onMove = this.onResize.bind(this);
        this.interactive.move.onEnd = this.onEnd.bind(this);
    }
    onStart(node, pointer, id, ev, applyResizer = true) {
        if (applyResizer) {
            document.body.style.cursor = "ns-resize !important";
        }
        this.startPos.x = ev.pageX;
        this.startPos.y = ev.pageY;
        this.startStateSize.y = this.sizePointer.value.y;
        this.options.start();
    }
    onResize(node, pointer, id, ev) {
        this.sizePointer.value.y = this.startStateSize.y + (ev.pageY - this.startPos.y);
        this.sizePointer.update();
        this.options.move();
    }
    onEnd(node, pointer, id, ev) {
        document.body.style.cursor = null;
        this.options.end();
    }
}
class ResizerTopLeft extends Resizer {
    mounted() {
        super.mounted();
        if (this.options.movement === false) {
            return;
        }
        this.interactive.move.enable(new Pointer(), "main");
        this.interactive.move.onStart = this.onStart.bind(this);
        this.interactive.move.onMove = this.onResize.bind(this);
        this.interactive.move.onEnd = this.onEnd.bind(this);
    }
    onStart(node, pointer, id, ev) {
        document.body.style.cursor = "nwse-resize !important";
        const resizerLeft = this.parent.elements.get("ResizerLeft");
        const resizerTop = this.parent.elements.get("ResizerTop");
        if (resizerLeft instanceof ResizerLeft) {
            resizerLeft.onStart(node, pointer, id, ev, false);
        }
        if (resizerTop instanceof ResizerTop) {
            resizerTop.onStart(node, pointer, id, ev, false);
        }
    }
    onResize(node, pointer, id, ev) {
        const resizerLeft = this.parent.elements.get("ResizerLeft");
        const resizerTop = this.parent.elements.get("ResizerTop");
        if (resizerLeft instanceof ResizerLeft) {
            resizerLeft.onResize(node, pointer, id, ev);
        }
        if (resizerTop instanceof ResizerTop) {
            resizerTop.onResize(node, pointer, id, ev);
        }
    }
    onEnd(node, pointer, id, ev) {
        document.body.style.cursor = null;
    }
}
class ResizerTopRight extends Resizer {
    mounted() {
        super.mounted();
        if (this.options.movement === false) {
            return;
        }
        this.interactive.move.enable(new Pointer(), "main");
        this.interactive.move.onStart = this.onStart.bind(this);
        this.interactive.move.onMove = this.onResize.bind(this);
        this.interactive.move.onEnd = this.onEnd.bind(this);
    }
    onStart(node, pointer, id, ev) {
        document.body.style.cursor = "nesw-resize !important";
        const resizerRight = this.parent.elements.get("ResizerRight");
        const resizerTop = this.parent.elements.get("ResizerTop");
        if (resizerRight instanceof ResizerRight) {
            resizerRight.onStart(node, pointer, id, ev, false);
        }
        if (resizerTop instanceof ResizerTop) {
            resizerTop.onStart(node, pointer, id, ev, false);
        }
    }
    onResize(node, pointer, id, ev) {
        const resizerRight = this.parent.elements.get("ResizerRight");
        const resizerTop = this.parent.elements.get("ResizerTop");
        if (resizerRight instanceof ResizerRight) {
            resizerRight.onResize(node, pointer, id, ev);
        }
        if (resizerTop instanceof ResizerTop) {
            resizerTop.onResize(node, pointer, id, ev);
        }
    }
    onEnd(node, pointer, id, ev) {
        document.body.style.cursor = null;
    }
}
class ResizerBottomRight extends Resizer {
    mounted() {
        super.mounted();
        if (this.options.movement === false) {
            return;
        }
        this.interactive.move.enable(new Pointer(), "main");
        this.interactive.move.onStart = this.onStart.bind(this);
        this.interactive.move.onMove = this.onResize.bind(this);
        this.interactive.move.onEnd = this.onEnd.bind(this);
    }
    onStart(node, pointer, id, ev) {
        document.body.style.cursor = "nesw-resize !important";
        const resizerRight = this.parent.elements.get("ResizerRight");
        const resizerBottom = this.parent.elements.get("ResizerBottom");
        if (resizerRight instanceof ResizerRight) {
            resizerRight.onStart(node, pointer, id, ev, false);
        }
        if (resizerBottom instanceof ResizerBottom) {
            resizerBottom.onStart(node, pointer, id, ev, false);
        }
    }
    onResize(node, pointer, id, ev) {
        const resizerRight = this.parent.elements.get("ResizerRight");
        const resizerBottom = this.parent.elements.get("ResizerBottom");
        if (resizerRight instanceof ResizerRight) {
            resizerRight.onResize(node, pointer, id, ev);
        }
        if (resizerBottom instanceof ResizerBottom) {
            resizerBottom.onResize(node, pointer, id, ev);
        }
    }
    onEnd(node, pointer, id, ev) {
        document.body.style.cursor = null;
    }
}
class ResizerBottomLeft extends Resizer {
    mounted() {
        super.mounted();
        if (this.options.movement === false) {
            return;
        }
        this.interactive.move.enable(new Pointer(), "main");
        this.interactive.move.onStart = this.onStart.bind(this);
        this.interactive.move.onMove = this.onResize.bind(this);
        this.interactive.move.onEnd = this.onEnd.bind(this);
    }
    onStart(node, pointer, id, ev) {
        document.body.style.cursor = "nwse-resize !important";
        const resizerLeft = this.parent.elements.get("ResizerLeft");
        const resizerBottom = this.parent.elements.get("ResizerBottom");
        if (resizerLeft instanceof ResizerLeft) {
            resizerLeft.onStart(node, pointer, id, ev, false);
        }
        if (resizerBottom instanceof ResizerBottom) {
            resizerBottom.onStart(node, pointer, id, ev, false);
        }
    }
    onResize(node, pointer, id, ev) {
        const resizerLeft = this.parent.elements.get("ResizerLeft");
        const resizerBottom = this.parent.elements.get("ResizerBottom");
        if (resizerLeft instanceof ResizerLeft) {
            resizerLeft.onResize(node, pointer, id, ev);
        }
        if (resizerBottom instanceof ResizerBottom) {
            resizerBottom.onResize(node, pointer, id, ev);
        }
    }
    onEnd(node, pointer, id, ev) {
        document.body.style.cursor = null;
    }
}
class Rotate extends InteractiveLibrary {
}
class Scale extends InteractiveLibrary {
}
class Skew extends InteractiveLibrary {
}
class Sort extends InteractiveLibrary {
    constructor(interactive) {
        super(interactive);
        this.onBeforeStart = () => { };
        this.onStart = () => { };
        this.onEnd = () => { };
        this.onSort = () => { };
        if (this.interactive.ui.parent instanceof ListUI) {
            this.ui = this.interactive.ui.parent;
        }
    }
    _onBeforeStart() {
        if (this.onBeforeStart) {
            this.onBeforeStart();
        }
    }
    _onStart() {
        this.ui.sorting = true;
        this.onStart();
    }
    _onEnd() {
        this.ui.sorting = false;
        this.onEnd();
    }
    onEnter() {
        if (View.dragging.value === false) {
            return;
        }
        if (this.ui.sorting === false) {
            return;
        }
        const placeholderNode = View.dragPlaceholder.value;
        const targetNode = this.interactive.ui.node.node;
        if (targetNode.compareDocumentPosition(placeholderNode) & 0x04) { // Target is after hovered
            targetNode.before(placeholderNode);
            this.listPointer.sort(this.interactive.ui.name, View.dragElement.value.name, 0);
        }
        else {
            targetNode.after(placeholderNode);
            this.listPointer.sort(this.interactive.ui.name, View.dragElement.value.name, 1);
        }
        this.onSort();
    }
    enable(listPointer, nodeName = "main", placeholderColor = "#0001", floating = "none") {
        if (this.enabled) {
            return;
        }
        this.enabled = true;
        this.interactive.drag.enable(nodeName, placeholderColor, floating);
        this.interactive.drag.onBeforeStart = this._onBeforeStart.bind(this);
        this.interactive.drag.onStart = this._onStart.bind(this);
        this.interactive.drag.onEnd = this._onEnd.bind(this);
        this.onMouseEnterBinded = this.onEnter.bind(this);
        this.interactive.ui.node.node.addEventListener("mouseenter", this.onMouseEnterBinded, false);
        this.listPointer = new ListPointer("id_" + performance.now() + "_" + this.random(1, 99999), listPointer.listID);
    }
    disable() {
        if (this.enabled === false) {
            return;
        }
        this.listPointer.unmount();
        this.listPointer = null;
        this.interactive.ui.node.node.removeEventListener("mouseenter", this.onMouseEnterBinded);
        this.interactive.drag.unmount();
        this.enabled = false;
    }
    unmount() {
        this.disable();
    }
}
/** @todo Has to be reinvented from previous version */
class Padding extends InteractiveLibrary {
    enable() {
    }
    disable() {
    }
    unmount() {
        this.disable();
    }
}
const { remote } = require('electron');
window.addEventListener('keydown', e => {
    if (e.key === 'F6') {
        remote.getCurrentWebContents().openDevTools();
    }
    else if (e.key === 'F5') {
        remote.getCurrentWindow().reload();
    }
});
var Magma;
(function (Magma) {
    /**
     * Kind values
     */
    let Kind;
    (function (Kind) {
        Kind[Kind["ROOT"] = 0] = "ROOT";
        Kind[Kind["START"] = 1] = "START";
        Kind[Kind["VAR"] = 2] = "VAR";
        Kind[Kind["IF"] = 3] = "IF";
        Kind[Kind["FUN"] = 4] = "FUN";
        Kind[Kind["ASSIGN"] = 5] = "ASSIGN";
        Kind[Kind["REFER"] = 6] = "REFER";
        Kind[Kind["CALL"] = 7] = "CALL";
        Kind[Kind["SUM"] = 8] = "SUM";
        Kind[Kind["SUB"] = 9] = "SUB";
        Kind[Kind["MUL"] = 10] = "MUL";
        Kind[Kind["DIV"] = 11] = "DIV";
        Kind[Kind["REM"] = 12] = "REM";
        Kind[Kind["EQ"] = 13] = "EQ";
        Kind[Kind["NEQ"] = 14] = "NEQ";
        Kind[Kind["GT"] = 15] = "GT";
        Kind[Kind["GTEQ"] = 16] = "GTEQ";
        Kind[Kind["LT"] = 17] = "LT";
        Kind[Kind["LTEQ"] = 18] = "LTEQ";
        Kind[Kind["AND"] = 19] = "AND";
        Kind[Kind["OR"] = 20] = "OR";
        Kind[Kind["VALUE"] = 21] = "VALUE";
    })(Kind = Magma.Kind || (Magma.Kind = {}));
    /**
     * Kind name representations
     */
    Magma.KindNames = new Mix([
        'Root', 'Start', 'Variable', 'If',
        'Function', 'Assignment', 'Reference',
        'Function Call', 'Addition', 'Substraction', 'Multiplication',
        'Division', 'Reminder', 'Equasion', 'Negative Equasion',
        'Greater', 'Greater or Equal', 'Less', 'Less or Equal', 'And',
        'Or', 'Value'
    ]);
    /**
     * Class that defines certain kinds
     * under one namespace which makes
     * it easier to identify
     */
    class KindClass {
        /**
         * Merge classes and primitive kinds to
         * create a new kind class
         * @param kindList list of primitive kinds
         * @param classList list of other classes
         */
        constructor(kindList = new Mix(), classList = new Mix()) {
            this.kinds = kindList;
            // Incorporate class list into kind list
            classList.foreach(val => {
                const kinds = val.kinds;
                kinds.foreach(val => {
                    this.kinds.add(val);
                });
            });
        }
        /**
         * Checks if kind class
         * contains given kind
         * @param kind given sample kind
         */
        has(kind) {
            let status = false;
            this.kinds.foreach(v => {
                if (v == kind)
                    status = true;
            });
            return status;
        }
    }
    Magma.KindClass = KindClass;
})(Magma || (Magma = {}));
var Magma;
(function (Magma) {
    class Type {
        constructor(config) {
            this.name = config.name;
            this.structure = {};
            this.all = false;
            if (config.structure) {
                this.structure = config.structure;
            }
            if (config.all) {
                this.all = config.all;
            }
        }
    }
    Magma.Type = Type;
})(Magma || (Magma = {}));
var Magma;
(function (Magma) {
    class RulesData {
        constructor(newData) {
        }
    }
    Magma.RulesData = RulesData;
    class Rules {
        /**
         * Materialize Rule set for
         * node materialization
         * @param kind Kind of the node
         * @param type Type of the node
         */
        constructor(kind, type, trait, data) {
            this.trait = trait;
            this.updateData((data) ? data : new Mix());
            this.updateRules(kind, type, trait);
        }
        /**
         * Get rule regarding a node on certain index
         * @param index index of the element to be inserted
         */
        getRule(index) {
            // If it's an infinite requirement
            if (this.infinite) {
                index = 0;
            }
            // Check whether index is correct
            if (index >= this.kinds.size)
                return null;
            if (index < 0)
                return null;
            return {
                kind: this.kinds.array[index],
                type: this.types.array[index]
            };
        }
        /**
         * Update rules based on node data
         * < This method should be invoked before updating rules! >
         * @param types Nodes of trait that must be updated
         */
        updateData(newData) {
            this.data = newData;
        }
        /**
         * Gets node data on certain index
         * @param index Index of data you want to get access
         */
        getData(index) {
            return this.data.array[index];
        }
        /**
         * Update Rules for
         * "req" and "code"
         */
        updateRules(kind, type, trait) {
            const classes = Magma.Core.memory.nodeKindClasses;
            const types = Magma.Core.memory.nodeTypes;
            const isEmpty = (trait == Magma.Trait.REQ)
                ? classes.get('no-req').has(kind)
                : classes.get('no-code').has(kind);
            if (isEmpty) {
                this.kinds = new Mix();
                this.types = new Mix();
            }
            this.infinite = false;
            this.kinds = new Mix();
            this.types = new Mix();
            if (trait == Magma.Trait.REQ) {
                // Variable
                if (kind == Magma.Kind.VAR) {
                    this.kinds.add(classes.get('expression').kinds);
                    this.types.add(new Mix([type]));
                    return true;
                }
                // If
                if (kind == Magma.Kind.IF) {
                    this.kinds.add(classes.get('expression').kinds);
                    this.types.add(new Mix([types.get('switch')]));
                    return true;
                }
                // Function
                if (kind == Magma.Kind.FUN) {
                    this.infinite = true;
                    this.kinds.add(new Mix([Magma.Kind.VAR]));
                    this.types.add(new Mix([types.get('any')]));
                    return true;
                }
                // Assign
                if (kind == Magma.Kind.ASSIGN) {
                    this.kinds.add(new Mix([Magma.Kind.REFER]));
                    this.types.add(new Mix([types.get('any')]));
                    this.kinds.add(classes.get('expression').kinds);
                    this.types.add(new Mix([this.getData(0).type]));
                    return true;
                }
                // Refer
                if (kind == Magma.Kind.REFER) {
                    return true;
                }
                // Call
                if (kind == Magma.Kind.CALL) {
                    // Native Functionality
                    // This functionality will be
                    // Exposed to user in the end
                    return true;
                }
                // Sum, Eq, Neq
                if ([Magma.Kind.SUM, Magma.Kind.EQ, Magma.Kind.NEQ].includes(kind)) {
                    this.infinite = true;
                    this.kinds.add(classes.get('expression').kinds);
                    this.types.add(new Mix(types.getMore('number', 'text', 'switch')));
                    return true;
                }
                // Sub, Mul, Div, Rem 
                if ([Magma.Kind.SUB, Magma.Kind.MUL, Magma.Kind.DIV, Magma.Kind.REM].includes(kind)) {
                    this.infinite = true;
                    this.kinds.add(classes.get('expression').kinds);
                    this.types.add(new Mix(types.getMore('number', 'switch')));
                    return true;
                }
                // Gt, Gteq, Lt, Lteq
                if ([Magma.Kind.GT, Magma.Kind.GTEQ, Magma.Kind.LT, Magma.Kind.LTEQ].includes(kind)) {
                    this.kinds.add(classes.get('expression').kinds);
                    this.types.add(new Mix(types.getMore('number', 'switch')));
                    return true;
                }
                // And, Or
                if ([Magma.Kind.GT, Magma.Kind.GTEQ, Magma.Kind.LT, Magma.Kind.LTEQ].includes(kind)) {
                    this.infinite = true;
                    this.kinds.add(classes.get('expression').kinds);
                    this.types.add(new Mix(types.getMore('switch')));
                    return true;
                }
                // Value
                if (kind == Magma.Kind.VALUE) {
                    return true;
                }
            }
            if (trait == Magma.Trait.CODE) {
                if (classes.get('yes-code').has(kind)) {
                    this.infinite = true;
                    this.kinds.add(classes.get('in-code').kinds);
                    this.types.add(new Mix([types.get('any')]));
                    return true;
                }
            }
        }
    }
    Magma.Rules = Rules;
})(Magma || (Magma = {}));
var Magma;
(function (Magma) {
    let Trait;
    (function (Trait) {
        Trait[Trait["REQ"] = 0] = "REQ";
        Trait[Trait["CODE"] = 1] = "CODE";
    })(Trait = Magma.Trait || (Magma.Trait = {}));
    class Node {
        constructor(kind) {
            this.kind = kind;
            this.name = '';
            this.type = new Magma.Type({
                name: 'void'
            });
            // Avoid recursion - Start Nodes are primitives
            if (kind != Magma.Kind.START) {
                this.reqNodes = new Mix([new Node(Magma.Kind.START)]);
                this.codeNodes = new Mix([new Node(Magma.Kind.START)]);
            }
            this.reqRules = new Magma.Rules(this.kind, this.type, Trait.REQ, this.reqNodes);
            this.codeRules = new Magma.Rules(this.kind, this.type, Trait.CODE, this.codeNodes);
        }
        updateRules() {
            // If requirements are dependant
            // on the type of this node (var)
            // this function will update em
        }
        setType(value) {
            this.type = value;
            this.updateRules();
        }
    }
    Magma.Node = Node;
})(Magma || (Magma = {}));
var Magma;
(function (Magma) {
    /**
     * A pointer to a node which holds
     * it's index and whether it exists
     * in "req" or "code" context
     */
    class NodePointer {
        /**
         * Materialize Node Pointer
         * with specified parameters
         * @param index Position in block
         * @param req Does it exist in "req" context?
         * @param code Does it exist in "code" context?
         */
        constructor(index, req, code) {
            this.index = index;
            this.req = req;
            this.code = code;
        }
    }
    Magma.NodePointer = NodePointer;
    /**
     * A path to a node that consists
     * of a NodePointers list and gives
     * the ability to traversally operate
     * on the path
     */
    class NodePath {
        /**
         * Materialize Node Path with an optional path
         * @param path Optionally assign a new path
         */
        constructor(path = new Mix([])) {
            this.path = path;
        }
        /**
         * Get Node pointed by path
         * (returns null if failed)
         */
        getNode() {
            let cur = Magma.Core.memory.rootNode;
            this.path.foreach(value => {
                if (value == null)
                    return null;
                if (value.code) {
                    cur = cur.codeNodes.array[value.index];
                }
                else if (value.req) {
                    cur = cur.reqNodes.array[value.index];
                }
            });
            return cur;
        }
        /**
         * Change index child selection
         * of parent node
         * @param index Numerical index
         */
        changeIndex(index) {
            if (!this.path.size)
                return false;
            const save = this.path.last.data.index;
            this.path.last.data.index = index;
            // Dangling pointer case
            if (this.getNode() == null) {
                this.path.last.data.index = save;
                return false;
            }
            // Successfull case
            else {
                return true;
            }
        }
        /**
         * Enter traits ("req" or "code")
         * @param trait Which trair do you want to enter?
         */
        enter(trait) {
            const node = this.getNode();
            if (trait == Magma.Trait.REQ) {
                if (node.reqNodes == null)
                    return false;
                this.path.add(new NodePointer(0, true, false));
                return true;
            }
            if (trait == Magma.Trait.CODE) {
                if (node.codeNodes == null)
                    return false;
                this.path.add(new NodePointer(0, false, true));
                return true;
            }
            return false;
        }
        /**
         * Leave current node
         * back to parent
         */
        leave() {
            const node = this.getNode();
            if (node.kind == Magma.Kind.ROOT)
                return false;
            this.path.delete(this.path.last.id);
            return true;
        }
        /**
         * Return parent of current path
         * (if not possible returns null)
         */
        parent() {
            const size = this.path.size;
            if (!size)
                return null;
            return new NodePath(new Mix(this.path.array.slice(0, size - 1)));
        }
        /**
         * Shows in which trait you currently are
         * (Returns null if root node is selected)
         */
        whichTrait() {
            if (this.path.last.data.req)
                return Magma.Trait.REQ;
            if (this.path.last.data.code)
                return Magma.Trait.CODE;
            return null;
        }
        /**
         * Returns index of
         * the last NodePointer
         */
        getIndex() {
            if (this.path.size) {
                return this.path.last.data.index;
            }
            return 0;
        }
    }
    Magma.NodePath = NodePath;
})(Magma || (Magma = {}));
var Magma;
(function (Magma) {
    /**
     * Main class which consists of
     * main node flow kept in "rootNode"
     * as well as of available node types
     * and node kind classes (check KindClass
     * for more information)
     */
    class Memory {
        /**
         * Initialize all Node classes
         * and primitive types
         */
        constructor() {
            //  ---NODE KINDS ---
            this.nodeKindClasses = new Arr();
            // Primitive
            this.nodeKindClasses.set('primitive', new Magma.KindClass(new Mix([
                Magma.Kind.START, Magma.Kind.VALUE
            ])));
            // Math
            this.nodeKindClasses.set('math', new Magma.KindClass(new Mix([
                Magma.Kind.SUM, Magma.Kind.SUB,
                Magma.Kind.MUL, Magma.Kind.DIV,
                Magma.Kind.REM
            ])));
            // Comparison
            this.nodeKindClasses.set('comparison', new Magma.KindClass(new Mix([
                Magma.Kind.EQ, Magma.Kind.NEQ,
                Magma.Kind.GT, Magma.Kind.GTEQ,
                Magma.Kind.LT, Magma.Kind.LTEQ
            ])));
            // Combo
            this.nodeKindClasses.set('combo', new Magma.KindClass(new Mix([
                Magma.Kind.AND, Magma.Kind.OR
            ])));
            // Expression
            this.nodeKindClasses.set('expression', new Magma.KindClass(new Mix([
                Magma.Kind.REFER, Magma.Kind.CALL, Magma.Kind.VALUE
            ]), new Mix([
                this.nodeKindClasses.get('math'),
                this.nodeKindClasses.get('comparison'),
                this.nodeKindClasses.get('combo')
            ])));
            // Declaration
            this.nodeKindClasses.set('declaration', new Magma.KindClass(new Mix([
                Magma.Kind.VAR, Magma.Kind.IF,
                Magma.Kind.FUN, Magma.Kind.ASSIGN
            ])));
            // The ones that do not accept Requirement
            this.nodeKindClasses.set('no-req', new Magma.KindClass(new Mix([
                Magma.Kind.REFER, Magma.Kind.VALUE,
                Magma.Kind.ROOT, Magma.Kind.START
            ])));
            // The ones that do not accept Code
            this.nodeKindClasses.set('no-code', new Magma.KindClass(new Mix([
                Magma.Kind.VAR, Magma.Kind.ASSIGN,
                Magma.Kind.REFER, Magma.Kind.CALL,
                Magma.Kind.VALUE, Magma.Kind.START
            ]), new Mix([
                this.nodeKindClasses.get('math'),
                this.nodeKindClasses.get('comparison'),
                this.nodeKindClasses.get('combo')
            ])));
            // The ones that accept Code
            this.nodeKindClasses.set('yes-code', new Magma.KindClass(new Mix([
                Magma.Kind.ROOT,
                Magma.Kind.IF,
                Magma.Kind.FUN
            ])));
            // The ones you can use in code block
            this.nodeKindClasses.set('in-code', new Magma.KindClass(new Mix(), new Mix([
                this.nodeKindClasses.get('declaration'),
                this.nodeKindClasses.get('expression')
            ])));
            //  ---NODE TYPES ---
            this.nodeTypes = new Arr();
            this.nodeTypes.set('any', new Magma.Type({
                name: 'any',
                all: true
            }));
            this.nodeTypes.set('none', new Magma.Type({
                name: 'none'
            }));
            this.nodeTypes.set('switch', new Magma.Type({
                name: 'switch'
            }));
            this.nodeTypes.set('number', new Magma.Type({
                name: 'number'
            }));
            this.nodeTypes.set('text', new Magma.Type({
                name: 'text'
            }));
        }
        /**
         * Method crutial for node system
         * to be working. If it possible
         * materialize it using Core
         * class'es constructor
         */
        initNodeSystem() {
            this.rootNode = new Magma.Node(Magma.Kind.ROOT);
        }
    }
    Magma.Memory = Memory;
})(Magma || (Magma = {}));
var Magma;
(function (Magma) {
    /**
     * Class responsible for
     * intellisense list context
     * which is part of an
     * action controller
     */
    class Intellisense {
        /**
         * Materialize a new Intellisense List
         * @param list ready to use list of items
         */
        constructor(list) {
            this.list = list;
            this.index = 0;
        }
        /**
         * Returns true if the
         * intellisense list
         * context exists
         */
        exists() {
            if (this.list && this.list.size)
                return true;
            return false;
        }
        /**
         * Returns true if object passed is
         * ready to be set to the Intellisense
         * list... false otherwise
         */
        meaningful(list) {
            if (list && list.size)
                return true;
            return false;
        }
        /**
         * Set a new item set to Intellisense List
         * @param list ready to use list of items
         */
        set(list) {
            this.list = list;
        }
        /**
         * Get current selection
         */
        get() {
            return this.list.array[this.index];
        }
        /**
         * Go up the intellisense list
         */
        up() {
            if (this.index < 1)
                return false;
            this.index--;
            return true;
        }
        /**
         * Go up the intellisense list
         */
        down() {
            if (this.index >= this.list.size - 1)
                return false;
            this.index++;
            return true;
        }
        /**
         *                   Dummy Method
         * This method is supposed to react with selected item
         * but it appears that it's going to be removed due to
         * the fact that "selection" operation will be maintained
         * in Controller class
         */
        select() {
            console.log('Selected');
        }
    }
    Magma.Intellisense = Intellisense;
})(Magma || (Magma = {}));
var Magma;
(function (Magma) {
    /**
     *           ATTENTION!
     * The following class is not used yet
     * but aims to be the only one
     * used by user or preferebly AI
     */
    /**
     * The ultimate class
     * which controls nodes
     * and intellisense list
     * based on Action class
     */
    class Controller {
        /**
         * Materialize new controller
         * with given context
         */
        constructor() { }
    }
    Magma.Controller = Controller;
})(Magma || (Magma = {}));
var Magma;
(function (Magma) {
    /**
     * Action class responsible for
     * [traversion, operation, inspection]
     * in node flow defined in Memory as "rootNode"
     */
    class Action {
        /**
         * Materailize new Controller
         * with default path defined
         */
        constructor() {
            this.path = new Magma.NodePath();
        }
        /**
         * Get information
         * about current Node
         */
        get() {
            return this.path.getNode();
        }
        /**
         * Go up the node list
         */
        up() {
            if (this.path.path.size) {
                let index = this.path.path.last.data.index;
                return this.path.changeIndex(index - 1);
            }
            return false;
        }
        /**
         * Go down the node list
         */
        down() {
            if (this.path.path.size) {
                let index = this.path.path.last.data.index;
                return this.path.changeIndex(index + 1);
            }
            return false;
        }
        /**
         * Enter requirements
         * trait of current node
         */
        enterReq() {
            return this.path.enter(Magma.Trait.REQ);
        }
        /**
         * Enter code trait
         * of current node
         */
        enterCode() {
            return this.path.enter(Magma.Trait.CODE);
        }
        /**
         * Leave current node scope
         */
        leave() {
            return this.path.leave();
        }
        /**
         * Get a list full of available
         * node kinds to create and
         * set new intellisense list
         */
        createGet() {
            if (!this.path.path.size)
                return;
            // Local variables
            const parent = this.path.parent().getNode();
            const index = this.path.getIndex();
            const trait = this.path.whichTrait();
            const intelliList = new Mix();
            let results = new Mix();
            console.log('createGet', trait);
            // Assign req trait kinds
            if (trait == Magma.Trait.REQ) {
                let rules = parent.reqRules.getRule(index);
                if (rules) {
                    results = rules.kind;
                }
            }
            // Assign code trait kinds
            if (trait == Magma.Trait.CODE) {
                let rules = parent.codeRules.getRule(index);
                console.log(rules);
                if (rules) {
                    results = rules.kind;
                }
            }
            // Create intelli list
            results.foreach(value => {
                intelliList.add({
                    name: Magma.KindNames.array[value],
                    value
                });
            });
            // Upload intelli list if it makes sense
            if (Magma.Core.intelli.meaningful(intelliList))
                Magma.Core.intelli.set(intelliList);
            return results;
        }
        /**
         * Creates Node and returnes
         * boolean if succeeded
         * @param value - Node Kind
         */
        createSet(value) {
            if (!this.path.path.size)
                return;
            // Local variables
            const parent = this.path.parent().getNode();
            const index = this.path.getIndex();
            const trait = this.path.whichTrait();
            const results = this.createGet();
            // Check whether the kind exists in requirements
            if (!results.array.includes(value)) {
                return false;
            }
            // Add nodes in traits
            if (trait == Magma.Trait.REQ) {
                const id = parent.reqNodes.readId(index + 1);
                const newId = parent.reqNodes.add(new Magma.Node(value));
                if (id != null) {
                    parent.reqNodes.sort(newId, id);
                }
                return true;
            }
            // Add nodes in traits
            if (trait == Magma.Trait.CODE) {
                const id = parent.codeNodes.readId(index + 1);
                const newId = parent.codeNodes.add(new Magma.Node(value));
                if (id != null) {
                    parent.codeNodes.sort(newId, id);
                }
                return true;
            }
        }
    }
    Magma.Action = Action;
})(Magma || (Magma = {}));
var Magma;
(function (Magma) {
    /**
     * Basis class of the entire
     * Magma Ecosystem. Must be
     * materialized in order to
     * keep node flow working
     * properly
     */
    class Core {
        /**
         * Initialize Magma Node Environment
         */
        constructor() {
            Core.memory.initNodeSystem();
        }
    }
    Core.memory = new Magma.Memory();
    Core.intelli = new Magma.Intellisense();
    Magma.Core = Core;
})(Magma || (Magma = {}));
class Container extends UI {
    display() {
        return this.div({ className: 'cont' }, [
            new Intellisense(),
            new Jumbotron()
        ]);
    }
}
class Intellisense extends UI {
    display() {
        return this.div({
            className: 'intelli'
        }, [
            this.div({
                className: 'title',
                text: 'Intellisense'
            }),
            this.div({
                className: 'cont'
            })
        ]);
    }
}
class Inspector extends UI {
    display() {
        return this.div({
            className: 'inspector',
            html: 'You can see what\'s<br>happening here'
        });
    }
}
class Keys extends UI {
    display() {
        return this.div({
            className: 'key-bindings',
        }, [
            this.div({ text: 'Help', className: 'title' }),
            this.div({ text: 'Inspect Selected - [F]' }),
            this.div({ text: 'Up / Down - [W, S]' }),
            this.div({ text: 'Enter Req - [R]' }),
            this.div({ text: 'Enter Code - [C]' }),
            this.div({ text: 'Leave - [Q]' }),
            this.div({ text: 'Create Node - [Space]' }),
        ]);
    }
}
class Jumbotron extends UI {
    beforeMount() {
        this.path = new Pointer('main', 'icons', 'magma');
    }
    display() {
        return this.div({ className: 'jumbotron' }, [
            new Inspector(),
            new Keys()
        ]);
    }
}
// Config File
// Set Initial Data
const DATA = {
    main: {}
};
// Set Root Node
const ROOT = new Container();
class App {
    constructor() {
        this.lists = new Arr();
    }
    mountData(data) {
        for (const list of Object.keys(data)) {
            Data.create(list);
            this.lists.set(list, new ListPointer(`${list}ID`, list));
            for (const item of Object.keys(data[list])) {
                this.lists.get(list).add(data[list][item], item);
            }
        }
    }
    mountUI() {
        ROOT.initialise('.app');
    }
    unmountData() {
        this.lists.foreach(list => list.unmount());
        Data.remove('main');
    }
    unmountUI() {
        this.base.unmount();
    }
}
const app = new App();
app.mountData(DATA);
app.mountUI();
const core = new Magma.Core();
const controller = new Magma.Action();
const kindNames = [
    'Root', 'Start', 'Variable', 'If',
    'Function', 'Assignment', 'Reference',
    'Function Call', 'Addition', 'Substraction', 'Multiplication',
    'Division', 'Reminder', 'Equasion', 'Negative Equasion',
    'Greater', 'Greater or Equal', 'Less', 'Less or Equal', 'And',
    'Or', 'Value'
];
// Easy Element creation
function createElement(tagName, className, target) {
    const element = document.createElement(tagName);
    element.className = className;
    if (target == null)
        return element;
    target.appendChild(element);
    return element;
}
// Main contexts
const inspector = document.querySelector('.inspector');
const intelli = document.querySelector('.intelli .cont');
// Variable to store last 
// Evaluated values
let lastObject = null;
window.addEventListener('keydown', e => {
    // Inspect current selection
    if (e.key == 'f') {
        const object = controller.get();
        inspector.innerHTML = `
            <div class="keyword">${kindNames[object.kind]}</div>
            <div class="name">${object.name}</div>
            <div class="value">${object.type.name}</div>
        `;
        console.log(object);
        lastObject = object;
    }
    // Move Up
    if (e.key == 'w') {
        const object = controller.up();
        inspector.innerHTML = `
            <div class="function">Move Up</div>
            <div class="value">${(object) ? 'Done' : 'Failed'}</div>
        `;
        console.log('Move Up', object);
        lastObject = object;
    }
    // Move Down
    if (e.key == 's') {
        const object = controller.down();
        inspector.innerHTML = `
            <div class="function">Move Down</div>
            <div class="value">${(object) ? 'Done' : 'Failed'}</div>
        `;
        console.log('Move Down', object);
        lastObject = object;
    }
    // Enter Requirements
    if (e.key == 'r') {
        const object = controller.enterReq();
        inspector.innerHTML = `
            <div class="function">Enter Requirements</div>
            <div class="value">${(object) ? 'Done' : 'Failed'}</div>
        `;
        console.log('Enter Req', object);
        lastObject = object;
    }
    // Enter Code
    if (e.key == 'c') {
        const object = controller.enterCode();
        inspector.innerHTML = `
            <div class="function">Enter Code</div>
            <div class="value">${(object) ? 'Done' : 'Failed'}</div>
        `;
        console.log('Enter Code', object);
        lastObject = object;
    }
    // Enter Code
    if (e.key == 'q') {
        const object = controller.leave();
        inspector.innerHTML = `
            <div class="function">Leave</div>
            <div class="value">${(object) ? 'Done' : 'Failed'}</div>
        `;
        console.log('Leave', object);
        lastObject = object;
    }
    // Create Node
    if (e.key == ' ') {
        const object = controller.createGet();
        if (object != null) {
            intelli.innerHTML = '';
            object.foreach(value => {
                const el = createElement('div', 'item keyword', intelli);
                el.innerHTML = kindNames[value];
                el.addEventListener('click', e => {
                    const object = controller.createSet(value);
                    inspector.innerHTML = `
                        <div class="function">Create Node Set</div>
                        <div class="value">${(object) ? 'Done' : 'Failed'}</div>
                    `;
                    intelli.innerHTML = '';
                });
            });
        }
        inspector.innerHTML = `
            <div class="function">Create Node Get</div>
            <div class="value">${(object && object.size) ? 'Done' : 'Failed'}</div>
        `;
        console.log(object);
        lastObject = object;
    }
});
//# sourceMappingURL=build.js.map