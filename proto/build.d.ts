/// <reference types="node" />
declare class Ground<O = any> {
    properties: O;
    random(min: number, max: number): number;
}
declare let _alphabetIndex: number;
declare const AlphabedIndexes: {
    a: number;
    b: number;
    c: number;
    d: number;
    e: number;
    f: number;
    g: number;
    h: number;
    i: number;
    j: number;
    k: number;
    l: number;
    m: number;
    n: number;
    o: number;
    p: number;
    q: number;
    r: number;
    s: number;
    t: number;
    u: number;
    v: number;
    w: number;
    x: number;
    y: number;
    z: number;
};
declare class Unit extends Ground {
    timeouts: Map<string, NodeJS.Timeout>;
    intervals: Map<string, NodeJS.Timeout>;
    setTimeout(func: VoidFunction, delay: number, name?: string): NodeJS.Timeout;
    clearTimeout(name?: string): void;
    setInterval(func: VoidFunction, delay: number, name?: string): NodeJS.Timeout;
    clearInterval(name?: string): void;
    numberify(name: string): number;
    uniqueNumber(): number;
    uniqueID(): string;
    construct(key: string, val: any): void;
}
interface AssocArrayType<T, I> {
    item: T;
    id: I;
}
declare class AssocArray<I = string, T = any> {
    protected items: AssocArrayType<T, I>[];
    protected ids: Map<I, () => AssocArrayType<T, I>>;
    size: number;
    get(id: I): T;
    foreach(callback: (item: T, id?: I) => boolean | unknown): void;
    forEach(callback: (item: T, id?: I) => boolean | unknown): void;
    has(id: I): boolean;
    sort(callback: (a: T, b: T, aID?: I, bID?: I) => number): void;
    set(id: I, item: T): void;
    clean(callback?: (item: T, id?: I) => boolean | unknown): void;
    delete(id: I): void;
}
interface AssocNode<T, I> {
    data: T;
    id: I;
    prev: AssocNode<T, I> | null;
    next: AssocNode<T, I> | null;
}
declare class AssocList<I, T> extends Unit {
    protected ids: Map<I, () => AssocNode<T, I>>;
    protected indexes: Map<number, () => AssocNode<T, I>>;
    firstNode: AssocNode<T, I> | null;
    lastNode: AssocNode<T, I> | null;
    size: number;
    constructor(items?: [I, T][] | T[]);
    /**
     *          --- Pawel ---
     * This method returns id of certain index
     * @param id - index in the array
     */
    readId(id: number): I;
    /**
     *          --- Pawel ---
     * This method returns all the ids
     */
    readIds(): Array<I>;
    export(): [I, T][];
    get first(): AssocNode<T, I> | null;
    get last(): AssocNode<T, I> | null;
    add(item: T): string;
    set(id: I, item: T): void;
    index(n: number): T;
    iteratedSort(callback: (a: T, b: T) => number): void;
    sort(beforeID: I, afterID: I): void;
    has(id: I): boolean;
    /**
     *          --- Pawel ---
     * Get multiple data based on multiple ids
     * @param ids - IDs of an element
     */
    getMore(...ids: I[]): T[];
    get(id: I): T;
    getNode(id: I): AssocNode<T, I>;
    clean(callback?: (item: T, id?: I) => boolean | unknown): void;
    delete(id: I): void;
    foreach(callback: (item: T, id?: I) => boolean | unknown, reversed?: boolean): void;
    forEach(callback: (item: T, id?: I) => boolean | unknown): void;
    protected doNext(node: AssocNode<T, I> | null, callback: (item: T, id?: I) => boolean | unknown): AssocNode<T, I> | null;
    protected doPrev(node: AssocNode<T, I> | null, callback: (item: T, id?: I) => boolean | unknown): AssocNode<T, I> | null;
    copy(): Arr<I, T>;
    get array(): T[];
    get map(): Map<I, T>;
    get obj(): any;
}
declare class Arr<I = any, T = any> extends AssocList<I, T> {
}
declare class Mix<T = any> extends Arr<string, T> {
}
declare class Vec3D {
    x: number;
    y: number;
    z: number;
    percentX: boolean;
    percentY: boolean;
    percentZ: boolean;
    offsetX: number;
    offsetY: number;
    offsetZ: number;
    constructor(data?: Partial<Vec3D>);
}
declare class Pos extends Vec3D {
}
declare class Size extends Vec3D {
}
declare class List<DataType = any> extends Ground {
    id: string;
    items: AssocList<string, Item<DataType>>;
    pointers: AssocList<string, ListPointer<DataType>>;
    appliedEventIDs: Map<string, boolean>;
    constructor(id: string);
    protected catchWaitingList(): void;
    mount(): void;
    clean(): void;
    add(structure: DataType, id?: string): void;
    remove(id?: string): void;
    sort(elementID1?: string, elementID2?: string, type?: number): void;
    unmount(): void;
    broadcastAdd(id: string): void;
    broadcastRemove(id: string): void;
    broadcastSort(elementID1?: string, elementID2?: string, type?: number): void;
    broadcastUnmount(): void;
}
declare class Item<DataType> extends Ground {
    id: string;
    data: DataType;
    pointers: AssocArray<string, Pointer<any>[]>;
    constructor(id: string, data: DataType);
    update(prop: string, newValue: any): void;
    broadcast(prop: string, newValue: any, oldValue: any): void;
}
declare class ListPointer<DataType = any> extends Ground {
    eventID: string;
    listID: string;
    onAdd: null | ((id: string) => void);
    onRemove: null | ((id: string) => void);
    onSort: null | ((elementID1: string, elementID2: string, type: number) => void);
    onUnmount: null | VoidFunction;
    mounted: boolean;
    list: List<DataType> | null;
    onWaitedMount: () => {};
    constructor(eventID: string, listID: string, onAdd?: null | ((id: string) => void), onRemove?: null | ((id: string) => void), onSort?: null | VoidFunction, onUnmount?: null | VoidFunction, autoMount?: boolean);
    size(): number;
    clean(): void;
    item(id?: string): Item<DataType>;
    add(structure: DataType, id?: string): void;
    remove(id?: string): void;
    sort(elementID1?: string, elementID2?: string, type?: number): void;
    protected registerWaitingList(): void;
    notifyMountUI(): void;
    mount(): void;
    unmount(): void;
}
declare type PointerUpdateFunction = (newValue: any, oldValue: any) => void;
declare class Pointer<DataType = any> extends Ground {
    listID: string;
    itemID: string;
    prop: string;
    onUnmount: null | VoidFunction;
    onUpdate: null | PointerUpdateFunction;
    mounted: boolean;
    uniqueID: string;
    constructor(listID?: string, itemID?: string, prop?: string, onUpdate?: null | PointerUpdateFunction, onUnmount?: null | VoidFunction, autoMount?: boolean);
    get item(): Item<any>;
    get value(): DataType;
    set value(newValue: DataType);
    update(): void;
    mount(): void;
    unmount(): void;
}
declare class Struct {
}
declare const Data: {
    lists: AssocList<string, List<any>>;
    create: (id?: string) => void;
    remove: (id?: string) => void;
    data: <T = any>(listID: string, itemID: string) => T;
    listPointersWaiting: Map<string, ListPointer<any>[]>;
};
declare type UIChildren = Array<NodeUI | UI | NodeComment | null>;
declare abstract class ShortcutsUI extends Unit {
    constructor();
    /** @abstract Method is abstract, implemented in UI class */
    abstract create(type: string, options: Partial<OptionsUI>, children: UIChildren): NodeUI;
    div(options?: Partial<OptionsUI>, children?: UIChildren): NodeUI;
    span(options?: Partial<OptionsUI>, children?: UIChildren): NodeUI;
    a(options?: Partial<OptionsUI>, children?: UIChildren): NodeUI;
    li(options?: Partial<OptionsUI>, children?: UIChildren): NodeUI;
    ul(options?: Partial<OptionsUI>, children?: UIChildren): NodeUI;
    p(options?: Partial<OptionsUI>, children?: UIChildren): NodeUI;
    form(options?: Partial<OptionsUI>, children?: UIChildren): NodeUI;
    input(options?: Partial<OptionsUI>, children?: UIChildren): NodeUI;
    textarea(options?: Partial<OptionsUI>, children?: UIChildren): NodeUI;
    button(options?: Partial<OptionsUI>, children?: UIChildren): NodeUI;
    video(options?: Partial<OptionsUI>, children?: UIChildren): NodeUI;
    audio(options?: Partial<OptionsUI>, children?: UIChildren): NodeUI;
    canvas(options?: Partial<OptionsUI>, children?: UIChildren): NodeUI;
}
declare type MouseEventFunction = (ev?: MouseEvent) => void;
declare class OptionsUI {
    name?: string;
    text?: string;
    html?: string;
    style?: string;
    width?: string;
    height?: string;
    className?: string;
    stateText?: NodeState | null;
    stateStyle?: NodeState | null;
    stateClass?: NodeState | null;
    type?: string;
    value?: string;
    placeholder?: string;
    stateValue?: NodeState;
    onClick?: MouseEventFunction | null;
    onChange?: MouseEventFunction | null;
    onFocus?: MouseEventFunction | null;
    onBlur?: MouseEventFunction | null;
    onDoubleClick?: MouseEventFunction | null;
    onMouseDown?: MouseEventFunction | null;
    onMouseUp?: MouseEventFunction | null;
    onMouseMove?: MouseEventFunction | null;
    onMouseEnter?: MouseEventFunction | null;
    onMouseLeave?: MouseEventFunction | null;
    onMouseOver?: MouseEventFunction | null;
    onMouseOut?: MouseEventFunction | null;
    onRightClick?: MouseEventFunction | null;
    onKeyUp?: MouseEventFunction | null;
    onKeyDown?: MouseEventFunction | null;
    onScroll?: MouseEventFunction | null;
    onMouseWheel?: MouseEventFunction | null;
    onSubmit?: MouseEventFunction | null;
    onResize?: MouseEventFunction | null;
    _onSubmit?: MouseEventFunction | null;
    attributes?: ({
        name: string;
        value: string;
    })[];
    constructor(data: Partial<OptionsUI>);
}
declare type MaybeUI = UI | null;
declare type StateUpdateFunction = (update: (data: NodeValue[] | string | boolean | number) => NodeValue[] | void, newVal?: any, oldVal?: any) => void;
interface UIData {
    cursor: Pos;
    windowResponderActive: boolean;
    windowKeyDownEvents: any[];
    windowKeyUpEvents: any[];
    dragging: boolean;
    dragElement: UI<any> | null;
    dragPlaceholder: HTMLDivElement | null;
    dragPos: Pos;
    dragLock: boolean;
}
declare const uiListPointer: ListPointer<UIData>;
declare const View: {
    cursor: Pointer<Pos>;
    windowResponderActive: Pointer<boolean>;
    windowKeyDownEvents: Pointer<any[]>;
    windowKeyUpEvents: Pointer<any[]>;
    dragging: Pointer<boolean>;
    dragElement: Pointer<UI<any>>;
    dragPlaceholder: Pointer<HTMLDivElement>;
    dragPos: Pointer<Pos>;
    dragLock: Pointer<boolean>;
};
declare class UI<OptionsType = {}> extends ShortcutsUI {
    parent: UI | null;
    elements: Map<string, UI<{}>>;
    nodes: Map<string, NodeUI>;
    node: NodeUI;
    name: string;
    options: OptionsType;
    propPointers: Map<string, Pointer<any>[]>;
    interactive: InteractiveUI;
    isMounted: boolean;
    private _windowMouseUp;
    private _windowMouseDown;
    private _windowMouseMove;
    setup(parent: UI | null, options: OptionsType): void;
    initialise(query?: string): void;
    _windowMouseUpEvent(ev: any): void;
    _windowMouseMoveEvent(ev: any): void;
    _windowMouseDownEvent(ev: any): void;
    prepareWindow(): void;
    releaseWindow(): void;
    mount(external?: boolean): void;
    unmount(done?: VoidFunction): void;
    protected onUnmountSync(): void;
    doUnmount(): void;
    element<O = {}>(classReference: new () => UI, options?: O, name?: string): UI<{}>;
    addElement<O = {}>(nodeName: string, classReference: new () => UI, options: O, name?: string): void;
    create(type: string, options: Partial<OptionsUI>, children: UIChildren): NodeUI;
    mountNode(node: NodeUI): void;
    applyChildren(node: NodeUI, children: Array<NodeUI | NodeComment | NodePosition | NodeElement | UI | null>): void;
    applyNode(node: NodeUI, child: NodeUI): void;
    applyStateMount(node: NodeUI, child: NodeComment): void;
    applyStateSwitch(node: NodeUI, child: NodeSwitch): void;
    applyStatePosition(node: NodeUI, child: NodePosition): void;
    applyElement(node: NodeUI, child: UI): void;
    /** @todo Implement statefullness to attributes */
    setupNodeAttributes(node: NodeUI): void;
    setupFormProbability(node: NodeUI, type: string, children: UIChildren): void;
    setupNodeType(node: NodeUI): void;
    setupNodeText(node: NodeUI): void;
    setupNodeValue(node: NodeUI): void;
    setupNodeStyle(node: any): void;
    setupNodeClass(node: NodeUI): void;
    setupNodeSize(node: NodeUI): void;
    setupNodeEvents(node: NodeUI): void;
    cleanNodeEvents(node: NodeUI): void;
    state(pointers: Pointer<any>[] | Pointer<any>, onUpdate: StateUpdateFunction, rewritable?: boolean): NodeState;
    value(key: string, value: any): NodeValue;
    stateValue(pointer: Pointer<any> | Pointer[]): NodeState;
    stateMount<DataType = any>(pointers: Pointer[] | Pointer, onUpdate: () => boolean, elementReference: () => (new () => UI), data: VoidFunction, name?: string): NodeComment;
    stateSwitch<DataType>(pointers: Pointer<DataType>[] | Pointer, onUpdate: () => any, data?: VoidFunction, name?: string, refreshable?: boolean): NodeSwitch;
    statePosition<DataType>(pointers: Pointer<DataType>[], elementName: string, condition: () => boolean, name?: string): NodePosition;
    list(name: string, listNode: NodeUI, listID: string, itemClassReference: new () => UI<any>): UI;
    removeItem(listID: string, itemID: string): void;
    keyDownUpdate(update?: () => void): void;
    getNode<T extends HTMLElement>(name: string): T;
    getInput(name: string): HTMLInputElement;
    getDisplay(): NodeUI;
    externalMounted(): void;
    externalBeforeMount(): void;
    externalUnmount(unmount: VoidFunction): void;
    externalUnmountSync(): void;
    /**
     * @abstract Method is abstract, implemented in extensions of UI class
     * @returns {NodeUI}
     * */
    protected display(): NodeUI;
    /**
     * @abstract Method is abstract, implemented in extensions of UI class
     * */
    protected mounted(): void;
    /**
     * @abstract Method is abstract, implemented in extensions of UI class
     * */
    protected beforeMount(): void;
    /**
     * @abstract Method is abstract, implemented in extensions of UI class
     * */
    protected onUnmount(unmount?: () => void): void;
}
declare class Transform extends Ground {
    node: NodeUI;
    matrix: number[][];
    constructor(node: NodeUI);
    translateX(x?: number, apply?: boolean): void;
    translateY(y?: number, apply?: boolean): void;
    translateZ(z?: number, apply?: boolean): void;
    translateXY(x?: number, y?: number, apply?: boolean): void;
    translate(x?: number, y?: number, z?: number, apply?: boolean): void;
    rotateX(x?: number, apply?: boolean): void;
    rotateY(y?: number, apply?: boolean): void;
    rotateZ(z?: number, apply?: boolean): void;
    rotateXY(x?: number, y?: number, apply?: boolean): void;
    rotate(x?: number, y?: number, z?: number, apply?: boolean): void;
    scaleX(x?: number, apply?: boolean): void;
    scaleY(y?: number, apply?: boolean): void;
    scale(x?: number, y?: number, apply?: boolean): void;
    skewX(x?: number, apply?: boolean): void;
    skewY(y?: number, apply?: boolean): void;
    skew(x?: number, y?: number, apply?: boolean): void;
    updateMatrix(trX?: number, trY?: number, trZ?: number, roX?: number, roY?: number, roZ?: number, scX?: number, scY?: number, skX?: number, skY?: number, apply?: boolean): void;
    sizeX(x?: number, px?: number, ox?: number, apply?: boolean): void;
    sizeY(y?: number, py?: number, oy?: number, apply?: boolean): void;
    sizeZ(z?: number, pz?: number, oz?: number, apply?: boolean): void;
    sizeXY(x?: number, y?: number, px?: number, py?: number, ox?: number, oy?: number, apply?: boolean): void;
    size(x?: number, y?: number, z?: number, px?: number, py?: number, pz?: number, ox?: number, oy?: number, oz?: number, apply?: boolean): void;
    apply(): void;
    applySize(): void;
}
declare type OnStateUpdateFunction = (newValue?: any, oldValue?: any) => void;
declare class NodeUI extends Ground {
    node: HTMLElement | HTMLInputElement;
    options: OptionsUI;
    transform: Transform;
    rect: DOMRect | ClientRect | null;
    constructor(node: HTMLElement, options: OptionsUI);
}
declare class NodeState extends Ground {
    pointers: Pointer<any>[];
    onUpdate: StateUpdateFunction;
    rewritable: Boolean;
    constructor(pointers: Pointer[], onUpdate: StateUpdateFunction, rewritable?: Boolean);
}
declare class NodeValue extends Ground {
    key: string;
    value: any;
    constructor(key: string, value: any);
}
declare class NodeComment extends Ground {
    comment: Comment;
    pointers: Pointer<any>[];
    onUpdate: () => boolean;
    elementReference: () => (new () => UI);
    data: () => any;
    name: string;
    constructor(comment: Comment, pointers: Pointer<any>[], onUpdate: () => boolean, elementReference: () => (new () => UI), data: () => any, name: string);
}
declare class NodePosition extends Ground {
    comment: Comment;
    pointers: Pointer<any>[];
    elementName: string;
    condition: () => boolean;
    name: string;
    constructor(comment: Comment, pointers: Pointer<any>[], elementName: string, condition: () => boolean, name: string);
}
declare class NodeSwitch extends Ground {
    comment: Comment;
    pointers: Pointer<any>[];
    onUpdate: () => any;
    elementReference: () => (new () => UI);
    data: () => any;
    name: string;
    refreshable: boolean;
    constructor(comment: Comment, pointers: Pointer<any>[], onUpdate: () => boolean, elementReference: () => (new () => UI), data: () => any, name: string, refreshable: boolean);
}
declare class NodeElement extends Ground {
    ref: new () => UI;
    data: any;
    name: string;
    constructor(ref?: typeof UI, data?: {}, name?: string);
}
interface ListUIData {
    listNode: NodeUI;
    listID: string;
    itemClassReference: new () => UI;
}
declare class ListUI<DataType> extends UI<ListUIData> {
    listPointer: ListPointer<DataType> | null;
    sorting: boolean;
    protected itemClassReference: new () => UI;
    mounted(): void;
    protected mountList(): void;
    onUnmount(unmount?: () => void): void;
    display(): NodeUI;
}
declare function node(options?: any): void;
declare function style(options?: any): void;
declare function background(options?: any): void;
declare class InteractiveLibrary extends Ground {
    interactive: InteractiveUI;
    enabled: boolean;
    constructor(interactive: InteractiveUI);
}
declare class InteractiveUI extends Ground {
    ui: UI;
    move: Move;
    drag: Drag;
    drop: Drop;
    resize: Resize;
    rotate: Rotate;
    skew: Skew;
    sort: Sort;
    padding: Padding;
    constructor(ui: UI);
}
declare type InteractiveMoveEventFunction = (node: NodeUI, pointer: Pointer<Pos>, name: string) => void;
declare class MoveMount {
    id: string;
    node: NodeUI;
    pointer: Pointer<Pos>;
    down: (ev?: MouseEvent) => void;
    move: (ev?: MouseEvent) => void;
    up: (ev?: MouseEvent) => void;
    constructor(id: string, node: NodeUI, pointer: Pointer<Pos>, down?: (ev?: MouseEvent) => void, move?: (ev?: MouseEvent) => void, up?: (ev?: MouseEvent) => void);
    mount(): void;
    unmount(): void;
}
declare type MoveCustomEvent = (node?: NodeUI, pointer?: Pointer<Pos>, id?: string, ev?: MouseEvent) => void;
declare let moving: boolean;
declare class Move extends InteractiveLibrary {
    nodeName: string;
    mount: MoveMount;
    startPos: Pos;
    startTransform: Pos;
    muted: boolean;
    paused: boolean;
    onStart: MoveCustomEvent;
    onMove: MoveCustomEvent;
    onEnd: MoveCustomEvent;
    _down(node: NodeUI, pointer: Pointer<Pos>, id: string, ev: MouseEvent): void;
    _move(node: NodeUI, pointer: Pointer<Pos>, id: string, ev: MouseEvent): void;
    _up(node: NodeUI, pointer: Pointer<Pos>, id: string, ev: MouseEvent): void;
    _pointerUpdate(pointer: Pointer<Pos>, node: NodeUI): void;
    enable(pointer: Pointer<Pos>, nodeName?: string): void;
    disable(): void;
    unmount(): void;
}
declare class Drag extends InteractiveLibrary {
    placeholder: any;
    placeholderColor: string;
    floating: string;
    onBeforeStart: VoidFunction;
    onStart: VoidFunction;
    onEnd: VoidFunction;
    delay: number;
    startAttempted: boolean;
    startPos: Pos;
    nodeName: string;
    private _onDragStartBinded;
    windowDelayMove: ((ev2: MouseEvent) => void) | null;
    windowDelayUp: () => void;
    _onDragStart(nodeName: string, ev: MouseEvent): void;
    _onDragMove(): void;
    _onStart(): void;
    protected mountPlaceholder(): void;
    _onEnd(): void;
    enable(nodeName?: string, placeholderColor?: string, floating?: string): void;
    disable(): void;
    unmount(): void;
}
/**
 * Features to be applied
 * ~1. Can drop item's data
 * 2. Can be combined with sort to automatically adjust where to appear the new item
 * 3. When combined with sort to have oninsert method which handles the data insertion and it's processing
 *
*/
declare class Drop extends InteractiveLibrary {
    _accept: Array<typeof Struct>;
    _onDropBinded: VoidFunction;
    _onDragEnterBinded: VoidFunction;
    _onDragLeaveBinded: VoidFunction;
    sortElementName: string;
    combineWithSort: boolean;
    sortedInsert: boolean;
    node: NodeUI;
    onDrop: (data?: any, ev?: MouseEvent) => void;
    onDragEnter: (data?: any) => void;
    onDragLeave: (data?: any) => void;
    onInsert: (data?: any) => [Struct, string?] | boolean;
    accept(/** @type {Array<<Struct>>} */ structSet?: any[]): void;
    _onDragEnter(): void;
    _onDragLeave(): void;
    _onDrop(ev: MouseEvent): void;
    validDrag(): boolean;
    enable(nodeName: string, structSet: Array<typeof Struct>, onDrop: (data?: any) => void, sortElementName?: string, sortedInsert?: boolean): void;
    disable(): void;
    unmount(): void;
}
declare class Resize extends InteractiveLibrary {
    left: boolean;
    top: boolean;
    right: boolean;
    bottom: boolean;
    topLeft: boolean;
    topRight: boolean;
    bottomLeft: boolean;
    bottomRight: boolean;
    movement: boolean;
    noMovement: boolean;
    percentOffsetCorrection: boolean;
    onResizeStart: VoidFunction;
    onResizeMove: VoidFunction;
    onResizeEnd: VoidFunction;
    _pointerUpdate(sizePointer: Pointer<Size>, positionPointer: Pointer<Pos>, node: NodeUI, manual?: boolean): void;
    enable(sizePointer: Pointer<Size>, posPointer: Pointer<Pos>): void;
    disable(): void;
    unmount(): void;
}
interface ResizerOptions {
    sizePointer: Pointer<Size>;
    posPointer: Pointer<Pos>;
    active: boolean;
    movement: boolean;
    start: VoidFunction;
    move: VoidFunction;
    end: VoidFunction;
}
declare class Resizer extends UI<ResizerOptions> {
    sizePointer: Pointer<Size>;
    posPointer: Pointer<Pos>;
    startPos: Pos;
    startStatePos: Pos;
    startSize: Size;
    startStateSize: Size;
    beforeMount(): void;
    mounted(): void;
    display(): NodeUI;
}
declare class ResizerLeft extends Resizer {
    mounted(): void;
    onStart(node: NodeUI, pointer: Pointer<Pos>, id: string, ev: MouseEvent, applyResizer?: boolean): void;
    onResize(node: NodeUI, pointer: Pointer<Pos>, id: string, ev: MouseEvent): void;
    onEnd(node: NodeUI, pointer: Pointer<Pos>, id: string, ev: MouseEvent): void;
}
declare class ResizerTop extends Resizer {
    mounted(): void;
    onStart(node: NodeUI, pointer: Pointer<Pos>, id: string, ev: MouseEvent, applyResizer?: boolean): void;
    onResize(node: NodeUI, pointer: Pointer<Pos>, id: string, ev: MouseEvent): void;
    onEnd(node: NodeUI, pointer: Pointer<Pos>, id: string, ev: MouseEvent): void;
}
declare class ResizerRight extends Resizer {
    mounted(): void;
    onStart(node: NodeUI, pointer: Pointer<Pos>, id: string, ev: MouseEvent, applyResizer?: boolean): void;
    onResize(node: NodeUI, pointer: Pointer<Pos>, id: string, ev: MouseEvent): void;
    onEnd(node: NodeUI, pointer: Pointer<Pos>, id: string, ev: MouseEvent): void;
}
declare class ResizerBottom extends Resizer {
    mounted(): void;
    onStart(node: NodeUI, pointer: Pointer<Pos>, id: string, ev: MouseEvent, applyResizer?: boolean): void;
    onResize(node: NodeUI, pointer: Pointer<Pos>, id: string, ev: MouseEvent): void;
    onEnd(node: NodeUI, pointer: Pointer<Pos>, id: string, ev: MouseEvent): void;
}
declare class ResizerTopLeft extends Resizer {
    mounted(): void;
    onStart(node: NodeUI, pointer: Pointer<Pos>, id: string, ev: MouseEvent): void;
    onResize(node: NodeUI, pointer: Pointer<Pos>, id: string, ev: MouseEvent): void;
    onEnd(node: NodeUI, pointer: Pointer<Pos>, id: string, ev: MouseEvent): void;
}
declare class ResizerTopRight extends Resizer {
    mounted(): void;
    onStart(node: NodeUI, pointer: Pointer<Pos>, id: string, ev: MouseEvent): void;
    onResize(node: NodeUI, pointer: Pointer<Pos>, id: string, ev: MouseEvent): void;
    onEnd(node: NodeUI, pointer: Pointer<Pos>, id: string, ev: MouseEvent): void;
}
declare class ResizerBottomRight extends Resizer {
    mounted(): void;
    onStart(node: NodeUI, pointer: Pointer<Pos>, id: string, ev: MouseEvent): void;
    onResize(node: NodeUI, pointer: Pointer<Pos>, id: string, ev: MouseEvent): void;
    onEnd(node: NodeUI, pointer: Pointer<Pos>, id: string, ev: MouseEvent): void;
}
declare class ResizerBottomLeft extends Resizer {
    mounted(): void;
    onStart(node: NodeUI, pointer: Pointer<Pos>, id: string, ev: MouseEvent): void;
    onResize(node: NodeUI, pointer: Pointer<Pos>, id: string, ev: MouseEvent): void;
    onEnd(node: NodeUI, pointer: Pointer<Pos>, id: string, ev: MouseEvent): void;
}
declare class Rotate extends InteractiveLibrary {
}
declare class Scale extends InteractiveLibrary {
}
declare class Skew extends InteractiveLibrary {
}
declare class Sort extends InteractiveLibrary {
    onBeforeStart: () => void;
    onStart: () => void;
    onEnd: () => void;
    onSort: () => void;
    ui: ListUI<any>;
    listPointer: ListPointer<any>;
    onMouseEnterBinded: VoidFunction;
    constructor(interactive: InteractiveUI);
    _onBeforeStart(): void;
    _onStart(): void;
    _onEnd(): void;
    onEnter(): void;
    enable(listPointer: ListPointer<any>, nodeName?: string, placeholderColor?: string, floating?: string): void;
    disable(): void;
    unmount(): void;
}
/** @todo Has to be reinvented from previous version */
declare class Padding extends InteractiveLibrary {
    enable(): void;
    disable(): void;
    unmount(): void;
}
declare const remote: any;
declare namespace Magma {
    /**
     * Kind values
     */
    enum Kind {
        ROOT = 0,
        START = 1,
        VAR = 2,
        IF = 3,
        FUN = 4,
        ASSIGN = 5,
        REFER = 6,
        CALL = 7,
        SUM = 8,
        SUB = 9,
        MUL = 10,
        DIV = 11,
        REM = 12,
        EQ = 13,
        NEQ = 14,
        GT = 15,
        GTEQ = 16,
        LT = 17,
        LTEQ = 18,
        AND = 19,
        OR = 20,
        VALUE = 21
    }
    /**
     * Kind name representations
     */
    const KindNames: Mix<string>;
    /**
     * Class that defines certain kinds
     * under one namespace which makes
     * it easier to identify
     */
    class KindClass {
        kinds: Mix<Kind>;
        /**
         * Merge classes and primitive kinds to
         * create a new kind class
         * @param kindList list of primitive kinds
         * @param classList list of other classes
         */
        constructor(kindList?: Mix<Kind>, classList?: Mix<KindClass>);
        /**
         * Checks if kind class
         * contains given kind
         * @param kind given sample kind
         */
        has(kind: Kind): boolean;
    }
}
declare namespace Magma {
    interface TypeConfig {
        name: string;
        structure?: Object;
        all?: boolean;
    }
    class Type {
        name: string;
        structure: Object;
        all: boolean;
        constructor(config: TypeConfig);
    }
}
declare namespace Magma {
    /**
     * An object that is being used
     * to pass rule to the getRule
     * endpoint
     */
    interface RuleObject {
        kind: Mix<Kind>;
        type: Mix<Type>;
    }
    class RulesData {
        constructor(newData: Mix<Node>);
    }
    class Rules {
        infinite: boolean;
        kinds: Mix<Mix<Kind>>;
        types: Mix<Mix<Type>>;
        trait: Trait;
        data: Mix<Node>;
        /**
         * Materialize Rule set for
         * node materialization
         * @param kind Kind of the node
         * @param type Type of the node
         */
        constructor(kind: Kind, type: Type, trait: Trait, data: Mix<Node>);
        /**
         * Get rule regarding a node on certain index
         * @param index index of the element to be inserted
         */
        getRule(index: number): RuleObject;
        /**
         * Update rules based on node data
         * < This method should be invoked before updating rules! >
         * @param types Nodes of trait that must be updated
         */
        updateData(newData: Mix<Node>): void;
        /**
         * Gets node data on certain index
         * @param index Index of data you want to get access
         */
        protected getData(index: number): Node;
        /**
         * Update Rules for
         * "req" and "code"
         */
        updateRules(kind: Kind, type: Type, trait: Trait): boolean;
    }
}
declare namespace Magma {
    enum Trait {
        REQ = 0,
        CODE = 1
    }
    class Node {
        kind: Kind;
        name: string;
        type: Type;
        reqRules: Rules;
        codeRules: Rules;
        reqNodes: Mix<Node>;
        codeNodes: Mix<Node>;
        constructor(kind: Kind);
        updateRules(): void;
        setType(value: Type): void;
    }
}
declare namespace Magma {
    /**
     * A pointer to a node which holds
     * it's index and whether it exists
     * in "req" or "code" context
     */
    class NodePointer {
        index: number;
        req: boolean;
        code: boolean;
        /**
         * Materialize Node Pointer
         * with specified parameters
         * @param index Position in block
         * @param req Does it exist in "req" context?
         * @param code Does it exist in "code" context?
         */
        constructor(index: number, req: boolean, code: boolean);
    }
    /**
     * A path to a node that consists
     * of a NodePointers list and gives
     * the ability to traversally operate
     * on the path
     */
    class NodePath {
        path: Mix<NodePointer>;
        /**
         * Materialize Node Path with an optional path
         * @param path Optionally assign a new path
         */
        constructor(path?: Mix<NodePointer>);
        /**
         * Get Node pointed by path
         * (returns null if failed)
         */
        getNode(): Node;
        /**
         * Change index child selection
         * of parent node
         * @param index Numerical index
         */
        changeIndex(index: any): boolean;
        /**
         * Enter traits ("req" or "code")
         * @param trait Which trair do you want to enter?
         */
        enter(trait: Trait): boolean;
        /**
         * Leave current node
         * back to parent
         */
        leave(): boolean;
        /**
         * Return parent of current path
         * (if not possible returns null)
         */
        parent(): NodePath;
        /**
         * Shows in which trait you currently are
         * (Returns null if root node is selected)
         */
        whichTrait(): Trait;
        /**
         * Returns index of
         * the last NodePointer
         */
        getIndex(): number;
    }
}
declare namespace Magma {
    /**
     * Main class which consists of
     * main node flow kept in "rootNode"
     * as well as of available node types
     * and node kind classes (check KindClass
     * for more information)
     */
    class Memory {
        rootNode: Node;
        nodeTypes: Arr<string, Type>;
        nodeKindClasses: Arr<string, KindClass>;
        /**
         * Initialize all Node classes
         * and primitive types
         */
        constructor();
        /**
         * Method crutial for node system
         * to be working. If it possible
         * materialize it using Core
         * class'es constructor
         */
        initNodeSystem(): void;
    }
}
declare namespace Magma {
    /**
     * Element representing a
     * choice object (selection item)
     */
    interface IntelliChoice {
        name: string;
        value: any;
    }
    /**
     * Class responsible for
     * intellisense list context
     * which is part of an
     * action controller
     */
    class Intellisense {
        list: Mix<IntelliChoice>;
        index: number;
        /**
         * Materialize a new Intellisense List
         * @param list ready to use list of items
         */
        constructor(list?: Mix<IntelliChoice>);
        /**
         * Returns true if the
         * intellisense list
         * context exists
         */
        exists(): boolean;
        /**
         * Returns true if object passed is
         * ready to be set to the Intellisense
         * list... false otherwise
         */
        meaningful(list: Mix<IntelliChoice>): boolean;
        /**
         * Set a new item set to Intellisense List
         * @param list ready to use list of items
         */
        set(list: Mix<IntelliChoice>): void;
        /**
         * Get current selection
         */
        get(): IntelliChoice;
        /**
         * Go up the intellisense list
         */
        up(): boolean;
        /**
         * Go up the intellisense list
         */
        down(): boolean;
        /**
         *                   Dummy Method
         * This method is supposed to react with selected item
         * but it appears that it's going to be removed due to
         * the fact that "selection" operation will be maintained
         * in Controller class
         */
        select(): void;
    }
}
declare namespace Magma {
    /**
     *           ATTENTION!
     * The following class is not used yet
     * but aims to be the only one
     * used by user or preferebly AI
     */
    /**
     * The ultimate class
     * which controls nodes
     * and intellisense list
     * based on Action class
     */
    class Controller {
        /**
         * Materialize new controller
         * with given context
         */
        constructor();
    }
}
declare namespace Magma {
    /**
     * Action class responsible for
     * [traversion, operation, inspection]
     * in node flow defined in Memory as "rootNode"
     */
    class Action {
        path: NodePath;
        /**
         * Materailize new Controller
         * with default path defined
         */
        constructor();
        /**
         * Get information
         * about current Node
         */
        get(): Node;
        /**
         * Go up the node list
         */
        up(): boolean;
        /**
         * Go down the node list
         */
        down(): boolean;
        /**
         * Enter requirements
         * trait of current node
         */
        enterReq(): boolean;
        /**
         * Enter code trait
         * of current node
         */
        enterCode(): boolean;
        /**
         * Leave current node scope
         */
        leave(): boolean;
        /**
         * Get a list full of available
         * node kinds to create and
         * set new intellisense list
         */
        createGet(): Mix<number>;
        /**
         * Creates Node and returnes
         * boolean if succeeded
         * @param value - Node Kind
         */
        createSet(value: number): boolean;
    }
}
declare namespace Magma {
    /**
     * Basis class of the entire
     * Magma Ecosystem. Must be
     * materialized in order to
     * keep node flow working
     * properly
     */
    class Core {
        static memory: Memory;
        static intelli: Intellisense;
        /**
         * Initialize Magma Node Environment
         */
        constructor();
    }
}
declare class Container extends UI {
    protected display(): NodeUI;
}
declare class Intellisense extends UI {
    protected display(): NodeUI;
}
declare class Inspector extends UI {
    protected display(): NodeUI;
}
declare class Keys extends UI {
    protected display(): NodeUI;
}
declare class Jumbotron extends UI {
    protected path: Pointer<string>;
    protected beforeMount(): void;
    protected display(): NodeUI;
}
declare const DATA: {
    main: {};
};
declare const ROOT: Container;
declare class App {
    lists: Arr<string, ListPointer>;
    base: UI;
    mountData(data: Object): void;
    mountUI(): void;
    unmountData(): void;
    unmountUI(): void;
}
declare const app: App;
declare const core: Magma.Core;
declare const controller: Magma.Action;
declare const kindNames: string[];
declare function createElement(tagName: string, className: string, target?: Element): Element;
declare const inspector: Element;
declare const intelli: Element;
declare let lastObject: any;
