class Container extends UI {
    protected display(): NodeUI {
        return this.div({ className: 'cont' }, [
            new Intellisense(),
            new Jumbotron()
        ])
    }
}

class Intellisense extends UI {
    protected display(): NodeUI {
        return this.div({
            className: 'intelli'
        }, [
            this.div({
                className: 'title',
                text: 'Intellisense'
            }),
            this.div({
                className: 'cont'
            })
        ])
    }
}

class Inspector extends UI {
    protected display(): NodeUI {
        return this.div({
            className: 'inspector',
            html: 'You can see what\'s<br>happening here'
        })
    }
}

class Keys extends UI {
    protected display(): NodeUI {
        return this.div({
            className: 'key-bindings',
        }, [
            this.div({ text: 'Help', className: 'title'}),
            this.div({ text: 'Inspect Selected - [F]' }),
            this.div({ text: 'Up / Down - [W, S]' }),
            this.div({ text: 'Enter Req - [R]' }),
            this.div({ text: 'Enter Code - [C]' }),
            this.div({ text: 'Leave - [Q]' }),
            this.div({ text: 'Create Node - [Space]' }),
        ])
    }
}

class Jumbotron extends UI {
    protected path: Pointer<string>
    
    protected beforeMount() {
        this.path = new Pointer<string>('main', 'icons', 'magma')
    }
    
    protected display(): NodeUI {
        return this.div({ className: 'jumbotron' }, [
            new Inspector(),
            new Keys()
        ])
    }
}