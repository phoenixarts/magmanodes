// Config File

// Set Initial Data
const DATA = {
    main: {
        
    }
}

// Set Root Node
const ROOT = new Container()

class App {
    public lists: Arr<string, ListPointer> = new Arr()
    public base: UI
    
    public mountData(data: Object) {
        for (const list of Object.keys(data)) {
            Data.create(list)
            this.lists.set(list, new ListPointer(`${list}ID`, list))
            for (const item of Object.keys(data[list])) {
                this.lists.get(list).add(data[list][item], item)
            }
        }        
    }
    
    public mountUI() {
        ROOT.initialise('.app')
    }
    
    public unmountData() {
        this.lists.foreach(list => list.unmount())
        Data.remove('main')
    }
    
    public unmountUI() {
        this.base.unmount()
    }
}

const app = new App()
app.mountData(DATA)
app.mountUI()