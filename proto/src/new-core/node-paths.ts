namespace Magma {
    
    /**
     * A pointer to a node which holds
     * it's index and whether it exists
     * in "req" or "code" context
     */
    export class NodePointer {
        public index: number
        public req: boolean
        public code: boolean
        
        /**
         * Materialize Node Pointer 
         * with specified parameters
         * @param index Position in block
         * @param req Does it exist in "req" context?
         * @param code Does it exist in "code" context?
         */
        constructor(index: number, req: boolean, code: boolean) {
            this.index = index
            this.req = req
            this.code = code
        }
    }
    
    /**
     * A path to a node that consists
     * of a NodePointers list and gives
     * the ability to traversally operate  
     * on the path
     */
    export class NodePath {
        public path: Mix<NodePointer>
        
        /**
         * Materialize Node Path with an optional path
         * @param path Optionally assign a new path
         */
        constructor(path: Mix<NodePointer> = new Mix([])) {
            this.path = path
        }
        
        /**
         * Get Node pointed by path 
         * (returns null if failed)
         */
        public getNode(): Node {
            let cur = Core.memory.rootNode
            this.path.foreach(value => {
                if (value == null) return null
                
                if (value.code) {
                    cur = cur.codeNodes.array[value.index]
                }
                else if (value.req) {                    
                    cur = cur.reqNodes.array[value.index]
                }
            })
            return cur
        }

        /**
         * Change index child selection 
         * of parent node
         * @param index Numerical index
         */
        public changeIndex(index): boolean {
            if (!this.path.size) return false
            const save = this.path.last.data.index
            this.path.last.data.index = index
            
            // Dangling pointer case
            if (this.getNode() == null) {
                this.path.last.data.index = save
                return false
            }
            
            // Successfull case
            else {
                return true
            }
        }
        
        /**
         * Enter traits ("req" or "code")
         * @param trait Which trair do you want to enter?
         */
        public enter(trait: Trait): boolean {
            const node = this.getNode()
            
            if (trait == Trait.REQ) {
                if (node.reqNodes == null) return false
                this.path.add(new NodePointer(0, true, false))
                return true
            }
            
            if (trait == Trait.CODE) {
                if (node.codeNodes == null) return false
                this.path.add(new NodePointer(0, false, true))
                return true
            }
            return false
        }
        
        /**
         * Leave current node 
         * back to parent
         */
        public leave(): boolean {
            const node = this.getNode()
            if (node.kind == Kind.ROOT) return false
            this.path.delete(this.path.last.id)
            return true
        }
        
        /**
         * Return parent of current path
         * (if not possible returns null)
         */
        public parent(): NodePath {
            const size = this.path.size
            if (!size) return null
            return new NodePath(
                new Mix(
                    this.path.array.slice(0, size - 1)
                )
            )
        }
        
        /**
         * Shows in which trait you currently are
         * (Returns null if root node is selected)
         */
        public whichTrait(): Trait {
            if (this.path.last.data.req)
                return Trait.REQ
            if (this.path.last.data.code)
                return Trait.CODE
            return null
        }
        
        /**
         * Returns index of 
         * the last NodePointer
         */
        public getIndex(): number {
            if (this.path.size) {
                return this.path.last.data.index
            }
            return 0
        }
    }
    
    
}