namespace Magma {
    
    /**
     * An object that is being used
     * to pass rule to the getRule
     * endpoint
     */
    export interface RuleObject {
        kind: Mix<Kind>
        type: Mix<Type>
    }

    export class RulesData {
        constructor(newData: Mix<Node>) {

        }
    }

    export class Rules {
        public infinite: boolean
        public kinds: Mix<Mix<Kind>>
        public types: Mix<Mix<Type>>
        public trait: Trait
        public data: Mix<Node>
        
        /**
         * Materialize Rule set for 
         * node materialization
         * @param kind Kind of the node
         * @param type Type of the node
         */
        constructor(kind: Kind, type: Type, trait: Trait, data: Mix<Node>) {
            this.trait = trait
            this.updateData((data) ? data : new Mix())
            this.updateRules(kind, type, trait)
        }

        /**
         * Get rule regarding a node on certain index
         * @param index index of the element to be inserted
         */
        public getRule(index: number) {
            // If it's an infinite requirement
            if (this.infinite) {
                index = 0
            }

            // Check whether index is correct
            if (index >= this.kinds.size) return null
            if (index < 0) return null

            return <RuleObject> {
                kind: this.kinds.array[index],
                type: this.types.array[index]
            }
        }

        /**
         * Update rules based on node data
         * < This method should be invoked before updating rules! >
         * @param types Nodes of trait that must be updated
         */
        public updateData(newData: Mix<Node>) {
            this.data = newData
        }

        /**
         * Gets node data on certain index
         * @param index Index of data you want to get access
         */
        protected getData(index: number): Node {
            return this.data.array[index]
        }

        /**
         * Update Rules for 
         * "req" and "code"
         */
        public updateRules(kind: Kind, type: Type, trait: Trait): boolean {
            const classes = Core.memory.nodeKindClasses
            const types = Core.memory.nodeTypes
            const isEmpty = (trait == Trait.REQ) 
                ? classes.get('no-req').has(kind)
                : classes.get('no-code').has(kind)
            
            
            if (isEmpty) {
                this.kinds = new Mix()
                this.types = new Mix()
            }
            
            this.infinite = false
            this.kinds = new Mix()
            this.types = new Mix()
            
            if(trait == Trait.REQ) {
                // Variable
                if (kind == Kind.VAR) {
                    this.kinds.add(classes.get('expression').kinds)
                    this.types.add(new Mix([type]))
                    return true
                }
                // If
                if (kind == Kind.IF) {
                    this.kinds.add(classes.get('expression').kinds)
                    this.types.add(new Mix([types.get('switch')]))
                    return true
                }
                // Function
                if (kind == Kind.FUN) {
                    this.infinite = true
                    this.kinds.add(new Mix([Kind.VAR]))
                    this.types.add(new Mix([types.get('any')]))
                    return true
                }
                // Assign
                if (kind == Kind.ASSIGN) {
                    this.kinds.add(new Mix([Kind.REFER]))
                    this.types.add(new Mix([types.get('any')]))

                    this.kinds.add(classes.get('expression').kinds)
                    this.types.add(new Mix([this.getData(0).type]))
                    return true
                }
                // Refer
                if (kind == Kind.REFER) {
                    return true
                }
                // Call
                if (kind == Kind.CALL) {
                    // Native Functionality
                    // This functionality will be
                    // Exposed to user in the end
                    return true
                }
                // Sum, Eq, Neq
                if ([Kind.SUM, Kind.EQ, Kind.NEQ].includes(kind)) {
                    this.infinite = true
                    this.kinds.add(classes.get('expression').kinds)
                    this.types.add(new Mix(types.getMore('number', 'text', 'switch')))
                    return true
                }
                // Sub, Mul, Div, Rem 
                if ([Kind.SUB, Kind.MUL, Kind.DIV, Kind.REM].includes(kind)) {
                    this.infinite = true
                    this.kinds.add(classes.get('expression').kinds)
                    this.types.add(new Mix(types.getMore('number', 'switch')))
                    return true
                }
                // Gt, Gteq, Lt, Lteq
                if ([Kind.GT, Kind.GTEQ, Kind.LT, Kind.LTEQ].includes(kind)) {
                    this.kinds.add(classes.get('expression').kinds)
                    this.types.add(new Mix(types.getMore('number', 'switch')))
                    return true
                }
                // And, Or
                if ([Kind.GT, Kind.GTEQ, Kind.LT, Kind.LTEQ].includes(kind)) {
                    this.infinite = true
                    this.kinds.add(classes.get('expression').kinds)
                    this.types.add(new Mix(types.getMore('switch')))
                    return true
                }
                // Value
                if (kind == Kind.VALUE) {
                    return true
                }
            }
            
            if (trait == Trait.CODE) {
                if(classes.get('yes-code').has(kind)) {
                    this.infinite = true
                    this.kinds.add(classes.get('in-code').kinds)
                    this.types.add(new Mix([types.get('any')]))
                    return true
                }
            }            
           
        }
        
    }
}