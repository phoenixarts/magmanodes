namespace Magma {
    
    export interface TypeConfig {
        name: string
        structure?: Object
        all?: boolean
    }
    
    export class Type {
        public name: string
        public structure: Object
        public all: boolean
        

        constructor(config: TypeConfig) {
            this.name = config.name
            this.structure = {}
            this.all = false
            
            if (config.structure) {
                this.structure = config.structure
            }
            
            if (config.all) {
                this.all = config.all
            }  
        }
        
    }
}