namespace Magma {

    /**
     * Main class which consists of
     * main node flow kept in "rootNode"
     * as well as of available node types
     * and node kind classes (check KindClass
     * for more information)
     */
    export class Memory {
        public rootNode: Node
        public nodeTypes: Arr<string, Type>
        public nodeKindClasses: Arr<string, KindClass>

        /**
         * Initialize all Node classes
         * and primitive types
         */
        constructor() {
            
            //  ---NODE KINDS ---
            
            this.nodeKindClasses = new Arr()

            // Primitive
            this.nodeKindClasses.set('primitive', new KindClass(
                new Mix([
                    Kind.START, Kind.VALUE
                ])
            ))
                
            // Math
            this.nodeKindClasses.set('math', new KindClass(
                new Mix([
                    Kind.SUM, Kind.SUB, 
                    Kind.MUL, Kind.DIV, 
                    Kind.REM
                ])
            ))
            
            // Comparison
            this.nodeKindClasses.set('comparison', new KindClass(
                new Mix([
                    Kind.EQ, Kind.NEQ,
                    Kind.GT, Kind.GTEQ,
                    Kind.LT, Kind.LTEQ
                ])
            ))
            
            // Combo
            this.nodeKindClasses.set('combo', new KindClass(
                new Mix([
                    Kind.AND, Kind.OR
                ])
            ))
            
            // Expression
            this.nodeKindClasses.set('expression', new KindClass(
                new Mix([
                    Kind.REFER, Kind.CALL, Kind.VALUE
                ]),
                new Mix([
                    this.nodeKindClasses.get('math'),
                    this.nodeKindClasses.get('comparison'),
                    this.nodeKindClasses.get('combo')
                ])
            ))
                
            // Declaration
            this.nodeKindClasses.set('declaration', new KindClass(
                new Mix([
                    Kind.VAR, Kind.IF,
                    Kind.FUN, Kind.ASSIGN
                ])
            ))
            
            // The ones that do not accept Requirement
            this.nodeKindClasses.set('no-req', new KindClass(
                new Mix([
                    Kind.REFER, Kind.VALUE,
                    Kind.ROOT, Kind.START
                ])
            ))
                
            // The ones that do not accept Code
            this.nodeKindClasses.set('no-code', new KindClass(
                new Mix([
                    Kind.VAR, Kind.ASSIGN,
                    Kind.REFER, Kind.CALL,
                    Kind.VALUE, Kind.START
                ]),
                new Mix([
                    this.nodeKindClasses.get('math'),
                    this.nodeKindClasses.get('comparison'),
                    this.nodeKindClasses.get('combo')
                ])
            ))
            
            // The ones that accept Code
            this.nodeKindClasses.set('yes-code', new KindClass(
                new Mix([
                    Kind.ROOT,
                    Kind.IF,
                    Kind.FUN
                ])
            ))
            
            // The ones you can use in code block
            this.nodeKindClasses.set('in-code', new KindClass(
                new Mix(),
                new Mix([
                    this.nodeKindClasses.get('declaration'),
                    this.nodeKindClasses.get('expression')
                ])
            ))
            
            
            //  ---NODE TYPES ---
            
            this.nodeTypes = new Arr()
            
            this.nodeTypes.set('any', new Type({
                name: 'any',
                all: true
            }))
            
            this.nodeTypes.set('none', new Type({
                name: 'none'
            }))
            
            this.nodeTypes.set('switch', new Type({
                name: 'switch'
            }))
            
            this.nodeTypes.set('number', new Type({
                name: 'number'
            }))

            this.nodeTypes.set('text', new Type({
                name: 'text'
            }))
        }
        
        /**
         * Method crutial for node system
         * to be working. If it possible
         * materialize it using Core 
         * class'es constructor
         */
        public initNodeSystem(): void {
            this.rootNode = new Node(Kind.ROOT)
        }

    }
}

