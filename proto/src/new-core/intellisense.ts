namespace Magma {

    /**
     * Element representing a
     * choice object (selection item)
     */
    export interface IntelliChoice {
        name: string
        value: any
    }

    /**
     * Class responsible for 
     * intellisense list context
     * which is part of an 
     * action controller
     */
    export class Intellisense {
        public list: Mix<IntelliChoice>
        public index: number

        /**
         * Materialize a new Intellisense List
         * @param list ready to use list of items
         */
        constructor(list?: Mix<IntelliChoice>) {
            this.list = list
            this.index = 0
        }

        /**
         * Returns true if the
         * intellisense list
         * context exists
         */
        public exists(): boolean {
            if(this.list && this.list.size) return true
            return false
        }

        /**
         * Returns true if object passed is
         * ready to be set to the Intellisense
         * list... false otherwise
         */
        public meaningful(list: Mix<IntelliChoice>): boolean {
            if (list && list.size) return true
            return false
        }

        /**
         * Set a new item set to Intellisense List
         * @param list ready to use list of items
         */
        public set(list: Mix<IntelliChoice>) {
            this.list = list
        }

        /**
         * Get current selection
         */
        public get(): IntelliChoice {
            return this.list.array[this.index]
        }

        /**
         * Go up the intellisense list
         */
        public up(): boolean {
            if (this.index < 1) return false
            this.index--
            return true
        }

        /**
         * Go up the intellisense list
         */
        public down(): boolean {
            if (this.index >= this.list.size - 1) return false
            this.index++
            return true
        }

        /**
         *                   Dummy Method
         * This method is supposed to react with selected item
         * but it appears that it's going to be removed due to 
         * the fact that "selection" operation will be maintained
         * in Controller class
         */
        public select() {
            console.log('Selected')
            
        }

    }
}