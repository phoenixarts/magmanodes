namespace Magma {

    /**
     * Action class responsible for
     * [traversion, operation, inspection]
     * in node flow defined in Memory as "rootNode"
     */
    export class Action {
        public path: NodePath

        /**
         * Materailize new Controller
         * with default path defined
         */
        constructor() {
            this.path = new NodePath()
        }

        /**
         * Get information
         * about current Node
         */
        public get(): Node {
            return this.path.getNode()
        }
        
        /**
         * Go up the node list
         */
        public up(): boolean {
            if (this.path.path.size) {
                let index = this.path.path.last.data.index
                return this.path.changeIndex(index - 1)
            }
            return false
        }
        
        /**
         * Go down the node list
         */
        public down(): boolean {
            if (this.path.path.size) {
                let index = this.path.path.last.data.index
                return this.path.changeIndex(index + 1)
            }
            return false
        }
        
        /**
         * Enter requirements
         * trait of current node
         */
        public enterReq(): boolean {
            return this.path.enter(Trait.REQ)
        }
        
        /**
         * Enter code trait
         * of current node
         */
        public enterCode(): boolean {
            return this.path.enter(Trait.CODE)
        }
        
        /**
         * Leave current node scope
         */
        public leave(): boolean {
            return this.path.leave()
        }
        
        /**
         * Get a list full of available
         * node kinds to create and 
         * set new intellisense list
         */
        public createGet(): Mix<number> {
            if (!this.path.path.size) return

            // Local variables
            const parent = this.path.parent().getNode()
            const index = this.path.getIndex()
            const trait = this.path.whichTrait()
            const intelliList = new Mix()
            let results = new Mix<number>()

            console.log('createGet', trait)
            
            
            // Assign req trait kinds
            if (trait == Trait.REQ) {
                let rules = parent.reqRules.getRule(index)
                if (rules) {
                    results = rules.kind
                }
            }
            
            // Assign code trait kinds
            if (trait == Trait.CODE) {
                let rules = parent.codeRules.getRule(index)
                console.log(rules)
                if (rules) {
                    results = rules.kind
                }
            }
            
            // Create intelli list
            results.foreach(value => {
                intelliList.add( <IntelliChoice> {
                    name: KindNames.array[value],
                    value
                })
            })

            // Upload intelli list if it makes sense
            if (Core.intelli.meaningful(intelliList))
                Core.intelli.set(intelliList)

            return results
        }

        /**
         * Creates Node and returnes 
         * boolean if succeeded
         * @param value - Node Kind
         */
        public createSet(value: number): boolean {
            if (!this.path.path.size) return

            // Local variables
            const parent = this.path.parent().getNode()
            const index = this.path.getIndex()
            const trait = this.path.whichTrait()
            const results = this.createGet()
            
            // Check whether the kind exists in requirements
            if (!results.array.includes(value)) {
                return false
            }

            // Add nodes in traits
            if (trait == Trait.REQ) {
                const id = parent.reqNodes.readId(index + 1)
                const newId = parent.reqNodes.add(new Node(value))
                if (id != null) {
                    parent.reqNodes.sort(newId, id)
                }
                return true
            }
            
            // Add nodes in traits
            if (trait == Trait.CODE) {
                const id = parent.codeNodes.readId(index + 1)
                const newId = parent.codeNodes.add(new Node(value))
                if (id != null) {
                    parent.codeNodes.sort(newId, id)
                }
                return true
            }
        }

    }
}