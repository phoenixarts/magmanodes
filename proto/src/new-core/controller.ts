namespace Magma {

    /**
     *           ATTENTION!
     * The following class is not used yet
     * but aims to be the only one
     * used by user or preferebly AI
     */
    
    /**
     * The ultimate class
     * which controls nodes
     * and intellisense list
     * based on Action class
     */
    export class Controller {
        
        /**
         * Materialize new controller
         * with given context
         */
        constructor() {}

        
    }

}