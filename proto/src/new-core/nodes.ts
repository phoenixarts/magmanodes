namespace Magma {
    
    export enum Trait {
        REQ, CODE
    }
    
    export class Node {
        public kind: Kind
        public name: string
        public type: Type
        public reqRules: Rules
        public codeRules: Rules
        public reqNodes: Mix<Node>
        public codeNodes: Mix<Node>

        constructor(kind: Kind) {
            this.kind = kind
            this.name = ''
            this.type = new Type({
                name: 'void'
            })

            // Avoid recursion - Start Nodes are primitives
            if (kind != Kind.START) {
                this.reqNodes = new Mix([ new Node(Kind.START) ])
                this.codeNodes = new Mix([ new Node(Kind.START) ])
            }
            
            this.reqRules = new Rules(
                this.kind, 
                this.type,
                Trait.REQ,
                this.reqNodes
            )
            this.codeRules = new Rules(
                this.kind,
                this.type, 
                Trait.CODE,
                this.codeNodes
            )

        }

        public updateRules() {
            // If requirements are dependant
            // on the type of this node (var)
            // this function will update em
        }

        public setType(value: Type) {
            this.type = value
            this.updateRules()
        }
    }
    
}