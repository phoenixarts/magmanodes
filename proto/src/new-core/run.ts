const core = new Magma.Core()
const controller = new Magma.Action()

const kindNames = [
    'Root', 'Start', 'Variable', 'If',
    'Function', 'Assignment', 'Reference',
    'Function Call', 'Addition', 'Substraction', 'Multiplication',
    'Division', 'Reminder', 'Equasion', 'Negative Equasion',
    'Greater', 'Greater or Equal', 'Less', 'Less or Equal', 'And',
    'Or', 'Value'
]

// Easy Element creation
function createElement(tagName: string, className: string, target?: Element): Element {
    const element = document.createElement(tagName)
    element.className = className
    if (target == null) return element
    target.appendChild(element)
    return element
}

// Main contexts
const inspector = document.querySelector('.inspector')
const intelli = document.querySelector('.intelli .cont')

// Variable to store last 
// Evaluated values
let lastObject = null

window.addEventListener('keydown', e => {
    // Inspect current selection
    if (e.key == 'f') {
        const object = controller.get()
        inspector.innerHTML = `
            <div class="keyword">${kindNames[object.kind]}</div>
            <div class="name">${object.name}</div>
            <div class="value">${object.type.name}</div>
        `
        console.log(object)
        lastObject = object
    }
    
    // Move Up
    if (e.key == 'w') {
        const object = controller.up()
        inspector.innerHTML = `
            <div class="function">Move Up</div>
            <div class="value">${(object) ? 'Done' : 'Failed'}</div>
        `
        console.log('Move Up', object)
        lastObject = object
    }
    
    // Move Down
    if (e.key == 's') {
        const object = controller.down()
        inspector.innerHTML = `
            <div class="function">Move Down</div>
            <div class="value">${(object) ? 'Done' : 'Failed'}</div>
        `
        console.log('Move Down', object)
        lastObject = object
    }
    
    // Enter Requirements
    if (e.key == 'r') {
        const object = controller.enterReq()
        inspector.innerHTML = `
            <div class="function">Enter Requirements</div>
            <div class="value">${(object) ? 'Done' : 'Failed'}</div>
        `
        console.log('Enter Req', object)
        lastObject = object
    }
    
    // Enter Code
    if (e.key == 'c') {
        const object = controller.enterCode()
        inspector.innerHTML = `
            <div class="function">Enter Code</div>
            <div class="value">${(object) ? 'Done' : 'Failed'}</div>
        `
        console.log('Enter Code', object)
        lastObject = object
    }
    
    // Enter Code
    if (e.key == 'q') {
        const object = controller.leave()
        inspector.innerHTML = `
            <div class="function">Leave</div>
            <div class="value">${(object) ? 'Done' : 'Failed'}</div>
        `
        console.log('Leave', object)
        lastObject = object
    }
    
    // Create Node
    if (e.key == ' ') {
        const object = controller.createGet()
        if (object != null) {
            intelli.innerHTML = ''
            object.foreach(value => {

                const el = createElement('div', 'item keyword', intelli)
                el.innerHTML = kindNames[value]
                el.addEventListener('click', e => {
                    const object = controller.createSet(value)
                    inspector.innerHTML = `
                        <div class="function">Create Node Set</div>
                        <div class="value">${(object) ? 'Done' : 'Failed'}</div>
                    `
                    intelli.innerHTML = ''
                })

            })
        }
        
        inspector.innerHTML = `
            <div class="function">Create Node Get</div>
            <div class="value">${(object && object.size) ? 'Done' : 'Failed'}</div>
        `
        console.log(object)
        lastObject = object
    }
    
})