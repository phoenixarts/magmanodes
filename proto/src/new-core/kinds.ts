namespace Magma {

    /**
     * Kind values
     */
    export enum Kind {
        ROOT, START, VAR, IF, FUN, ASSIGN, REFER, CALL,
        SUM, SUB, MUL, DIV, REM, EQ, NEQ,
        GT, GTEQ, LT, LTEQ, AND, OR, VALUE,
    }

    /**
     * Kind name representations
     */
    export const KindNames: Mix<string> = new Mix([
        'Root', 'Start', 'Variable', 'If',
        'Function', 'Assignment', 'Reference',
        'Function Call', 'Addition', 'Substraction', 'Multiplication',
        'Division', 'Reminder', 'Equasion', 'Negative Equasion',
        'Greater', 'Greater or Equal', 'Less', 'Less or Equal', 'And',
        'Or', 'Value'
    ])

    /**
     * Class that defines certain kinds
     * under one namespace which makes
     * it easier to identify
     */
    export class KindClass {
        public kinds: Mix<Kind>

        /**
         * Merge classes and primitive kinds to 
         * create a new kind class
         * @param kindList list of primitive kinds
         * @param classList list of other classes
         */
        constructor(kindList = new Mix<Kind>(), classList = new Mix<KindClass>()) {
            this.kinds = kindList
            
            // Incorporate class list into kind list
            classList.foreach(val => {
                const kinds = val.kinds
                kinds.foreach(val => {
                    this.kinds.add(val)
                })
            })
        }

        /**
         * Checks if kind class 
         * contains given kind
         * @param kind given sample kind
         */
        has(kind: Kind): boolean {
            let status = false
            this.kinds.foreach(v => {
                if (v == kind) status = true 
            })
            return status
        }

    }

}