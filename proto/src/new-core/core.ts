namespace Magma {

    /**
     * Basis class of the entire
     * Magma Ecosystem. Must be
     * materialized in order to
     * keep node flow working
     * properly
     */
    export class Core {
        public static memory: Memory = new Memory()
        public static intelli: Intellisense = new Intellisense()
        
        /**
         * Initialize Magma Node Environment
         */
        constructor() {
            Core.memory.initNodeSystem()
        }
    }
}
