![](magma-nodes-logo-readme.png)


![](https://badgen.net/badge/state/archived/?color=red)
# Magma Node Environment Project
This repository contains Magma Nodes environment written in *TypeScript* and runs with it's GUI on *Electron* although it's not necessary to use any UI. It's mainly supposed to be some sort of an engine for AI to code with.
